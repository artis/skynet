#pragma once
#include "SkyNet.h"
#include "ace/Dirent.h"
#include "ace/Dirent_Selector.h"

enum CacheOpcodes{
	OP_SEND_FILE_LIST	= 1,
	OP_REQUEST_FILE_LIST,
	OP_SEND_FILE,

};
struct CacheFile
{
	string dir;
	string name;
	CacheFile(string new_dir, string new_name){ dir = new_dir; name = new_name;}
};
class Downloader: public BasePlayer
{
	SimpleTimer m_timer;
	
public:

	vector<CacheFile> files;
	Downloader(GameSession* session):BasePlayer(session){
		m_timer.SetTime(200);
	};

	void Update(uint32 diff)
	{
		m_timer.Update(diff);

		//sLog->outDebug("Player update %d files",files.size());
		if( !files.empty())
		{
			if(IsSendQueueEmpty())
			{
				string base_dir = "cache/";
				string del = "/";
				CacheFile cache = files.back();
				files.pop_back();
				SkyNet_File file(base_dir + cache.dir + del + cache.name, cache.name, cache.dir);
				sLog->outDebug("Sending %s",(base_dir + cache.dir + del + cache.name).c_str());
				SendFile(file);
				//m_timer.AddTime(200);
			}
		}
	}
};
class CacheFileManager
{
	vector<CacheFile> m_cacheFile;
	
public:
	bool CheckIfFileIsAlreadyRead(CacheFile file)
	{
		for(int i = 0; i < m_cacheFile.size(); i++)
			if(m_cacheFile[i].dir == file.dir && m_cacheFile[i].name == file.name)
				return true;

		return false;
	}
	vector<CacheFile> GetFiles()
	{
		return m_cacheFile;
	}
	void AddFile(CacheFile file)
	{
		m_cacheFile.push_back(file);
	}
	vector<CacheFile> ReadFiles()
	{
		vector<CacheFile> new_files;
		ACE_Dirent dir;
		string TestDir = "cache";
		int entrycount = 0;
		if (dir.open (TestDir.c_str ()) == -1)
			return new_files;

		for (ACE_DIRENT *directory = 0;	(directory = dir.read ()) != 0;	entrycount++)
		{
			

			int sub_entry = 0;
			ACE_Dirent sub_dir;
			string dir_str = "cache";
			string del = "/";
			string sub_dir_str = directory->d_name;
			string open_dir = (dir_str + del + sub_dir_str).c_str();

			if(sub_dir_str == "." || sub_dir_str == "..")
				continue;
			if (sub_dir.open (open_dir.c_str()) == -1)
				return new_files;
			for (ACE_DIRENT *sub_directory = 0;	(sub_directory = sub_dir.read ()) != 0;	sub_entry++)
			{
				
				string entry = sub_directory->d_name;
				if(entry == "." || entry == "..")
				continue;
				//sLog->outDebug("Adding file %s/%s", sub_dir_str.c_str(), entry.c_str());
				CacheFile file(sub_dir_str,entry);
				if(!CheckIfFileIsAlreadyRead(file)){
					m_cacheFile.push_back(file);
					new_files.push_back(file);
				}
			}
		}
		return new_files;
	}
};

class DownloadRoom :
	public Room
{
	ACE_Recursive_Thread_Mutex* m_lock;
	CacheFileManager m_fileMgr;
	SimpleTimer m_timer;
public:
	DownloadRoom(bool init = true);
	virtual ~DownloadRoom(void);
	DownloadRoom* Clone()
	{
		return new DownloadRoom();
	}
	bool Update(uint32 diff)
	{
		m_timer.Update(diff);

		if(m_timer.isPassed())
			RecheckCache();
		return true;
	}

	void RecheckCache()
	{
		m_lock->acquire();
		vector<CacheFile> files = m_fileMgr.ReadFiles();
		m_timer.AddTime(5000);
		
		if(files.empty())
		{
			m_lock->release();
			return;
		}
		string base_dir = "cache/";
		string del = "/";

		vector<BasePlayer*> players = GetPlayers();
		sLog->outDebug("Sending to %d players", players.size());
		while(!players.empty()){
			BasePlayer* plr = players.back();
			players.pop_back();

			for(int i = 0; i < files.size(); i++)
			{
				SkyNet_File cache(base_dir + files[i].dir + del + files[i].name,  files[i].name, files[i].dir);
				plr->SendFile(cache);
			}

			
		}

		m_lock->release();
	}
	void HandleFileList( Yieldable* data, YieldSubject* subject)
	{
		try{
			int b;
		sLog->outDebug("HandleFileList()");
		m_lock->acquire();
		ComPacket* packet = data->ConvertTo<ComPacket>();
		Downloader* plr = subject->ConvertTo<Downloader>();
		if(!plr)
			sLog->outDebug("Failed to convert YieldSubject* to BasePlayer*");
		else
			sLog->outDebug("HandleFileList(): Subject* %p Downloader* %p",subject,plr);

		sLog->outDebug("HandleFileList(1): Player id is %d", plr->GetGUID());
		
		int num;
		(*packet) >> num;
		sLog->outDebug("HandleFileList(): Entering loop - %d packet size %d", num, (*packet).size());
		ComPacket request(OP_REQUEST_FILE_LIST, 200);
		int requests = 0;
		
		//cin >> b;
		for(int i = 0; i < num; i++)
		{
			
			string dir;
			string name;
			
			(*packet) >> dir;
			(*packet) >> name;

			//sLog->outDebug("HandleFileList(): Loop counter: %d %s", i, name.c_str());
			CacheFile file(dir,name);
			if(!m_fileMgr.CheckIfFileIsAlreadyRead(file))
			{
				//sLog->outDebug("Will request %s",name.c_str());
				requests++;
			}
		}
		sLog->outDebug("HandleFileList(): Leaving loop with %d requests", requests);
		request << requests;
		
		(*packet).rpos(0);
		(*packet) >> num;
		if(requests > 0)
		{
			requests = 0;
			sLog->outDebug("HandleFileList():Entering another loop num=%d",num);
		
			for(int i = 0; i < num; i++)
			{
				//sLog->outDebug("OOOOPS");
				string dir;
				string name;
			
				(*packet) >> dir;
				(*packet) >> name;

				CacheFile file(dir,name);
				if(!m_fileMgr.CheckIfFileIsAlreadyRead(file))
				{
					sLog->outDebug("Request %s/%s",dir.c_str(),name.c_str());
					request << dir;
					request << name;
					requests++;
				}
			}
		}
		sLog->outDebug("HandleFileList(): Leaving another loop with %d requests", requests);
		
		//if(requests > 0)
		//	request.hexlike();

		sLog->outDebug("HandleFileList(2): Player id is %d", plr->GetGUID());
		plr->SendPacket(request);
		
		m_lock->release();
		}catch(std::exception e){
			sLog->outDebug("Exception %s",e.what());
		}
	}
	void HandleRequestFiles( Yieldable* data, YieldSubject* subject)
	{
		m_lock->acquire();
		try{
		
		
		ComPacket* packet = data->ConvertTo<ComPacket>();
		Downloader* plr = subject->ConvertTo<Downloader>();

		int num;
		(*packet) >> num;
		sLog->outDebug("HandleRequestFiles(%d)",num);

		for(int i = 0; i < num; i++)
		{
			string dir;
			string file;
			(*packet) >> dir;
			(*packet) >> file;
			CacheFile cache(dir,file);
			plr->files.push_back(cache);
			sLog->outDebug("Scheduling file upload.");
		}
		
		}catch(SkyNetException& ex){
			sLog->outDebug("Exception caught");
		}
		catch(std::exception& ex){
			sLog->outDebug("Another exception caught");
		}
		sLog->outDebug("Life goes on...");
		m_lock->release();
	}
	void HandleReceiveFile( Yieldable* data, YieldSubject* subject)
	{
		//return;
		//sLog->outDebug("HandleReceiveFile()");
		m_lock->acquire();
		SkyNet_File* file = data->ConvertTo<SkyNet_File>();
		BasePlayer* uploader = subject->ConvertTo<BasePlayer>();

		sLog->outDebug("Downloading %s", file->GetFilename().c_str());
		string del = "/";
		string base_dir = "cache/";
		file->Save(base_dir + file->GetDestinationPath(), file->GetFilename());

		m_fileMgr.ReadFiles();
		
		vector<BasePlayer*> players = GetPlayers();
		//sLog->outDebug("Sending to %d players", players.size());
		while(!players.empty()){
			BasePlayer* plr = players.back();
			players.pop_back();

			if(plr != uploader)
				plr->SendFile(*file);


		}
		m_lock->release();
	}
	void OnPlayerJoinRoom(BasePlayer* plr)
	{
		sLog->outDebug("Downloader %d joined %p", plr->GetGUID(), plr);
		m_lock->acquire();
		vector<CacheFile> files = m_fileMgr.GetFiles();
		
		int counter = 0;
		ComPacket* data = new ComPacket(1,1000);
		ComPacket* ptr = data;

		int num = files.size();
		if(num > 10)
			num = 10;
		(*ptr) << num;
		while(!files.empty())
		{
			CacheFile file = files.back();
			files.pop_back();
			string dir = file.dir;
			string name = file.name;
			(*ptr) << dir;
			(*ptr) << name;

			counter++;;
			if(counter == 10 || files.empty())
			{
				plr->SendPacket((*ptr));
				delete ptr;
				counter = 0;
				ComPacket* newdata = new ComPacket(1,1000);
				ptr = newdata;
				num = files.size();
				if(num > 10)
					num = 10;
				(*ptr) << num;
				//SkyNet::Threading::Thread::Sleep(180);
			}
			
		}

		m_lock->release();
	}
	void OnCreate()
	{
		sLog->outDebug("DownloadRoom::OnCreate() ID %d", this->getId());
		m_lock->acquire();
		sLog->outDebug("DownloadRoom::OnCreate() There are %d new files.",m_fileMgr.ReadFiles().size());
		sLog->outDebug("DownloadRoom::OnCreate() There are %d new files.",m_fileMgr.ReadFiles().size());
		m_timer.SetTime(5000);
		m_lock->release();
	}
};

