#include "SkyNet.h"
#include "Coordinator/SkyNetCoordinator.h"
#include "DownloadRoom.h"

using namespace SkyNet;


class AuthModule: public Module{
public:
	AuthModule(){
		user_id = 0;
		sModuleMgr->Subscribe(0,YIELD_TYPE_SESSION , AUTHENTICATE, this, CREATE_CALLBACK(&AuthModule::HandleAuthentication));
	}

	
	void HandleAuthentication( Yieldable* data, YieldSubject* subject)
	{
		ComPacket& packet = *data->ConvertTo<ComPacket>();
		GameSession* session;
		sLog->outDebug("AuthModule::HandleAuthentication Got GameSession* %p",session);
		session = subject->ConvertTo<GameSession>();
		if(!session){
			sLog->outDebug("AuthenticateModule::HandleAuthentication: Bad pointer conversion");
			return;
		}
		Downloader* plr = new Downloader(session);

		session->SetPlayer(plr);
		sLog->outDebug("Authentication successful %p",plr);
		YieldSubject* subj = plr;
		sLog->outDebug("Downloader's subject ptr %p",subj);
		sLog->outDebug("Downloader's subject ptr back to Downloader %p",static_cast<Downloader*>(subj));
		plr->SetID(user_id);
		user_id++;
        sGameCore->AddPlayer(plr);
		ComPacket response(5, 1, 1);
        response << (uint8)AUTH_RESULT_SUCCESS;
		plr->SendPacket(response);
		sLog->outDebug("Authentication successful");
		sPlayerRegistry->RegisterPlayer(plr);
		
	}
	AuthModule* Clone(){ return new AuthModule(); }
private:
	int user_id;
};

int main(int argc, char *argv[]){
	SkyNetCoordinator brain;
	brain.Initialize();
	new AuthModule();
    brain.AddAcceptor("0.0.0.0", 1234);
    brain.RegisterRoomGroup(1, 1);
    brain.RegisterRoomScript(1, 1, new DownloadRoom(false));
	brain.CreateRoom(1);
	brain.Prime();
	brain.Wait();
	return 0;
}