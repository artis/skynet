#include "DownloadRoom.h"


DownloadRoom::DownloadRoom(bool init)
{
	
	m_lock = new ACE_Recursive_Thread_Mutex();
	if(!init)
		return;
	sModuleMgr->Subscribe(0,NETWORKING , RECIEVE_FILE, this, CREATE_CALLBACK(&DownloadRoom::HandleReceiveFile));
    sModuleMgr->Subscribe(0,YIELD_TYPE_DATA_PACKET, OP_REQUEST_FILE_LIST, this, CREATE_CALLBACK(&DownloadRoom::HandleRequestFiles));
    sModuleMgr->Subscribe(0,YIELD_TYPE_DATA_PACKET, OP_SEND_FILE_LIST, this, CREATE_CALLBACK(&DownloadRoom::HandleFileList));

}


DownloadRoom::~DownloadRoom(void)
{
}
