file(GLOB_RECURSE sources_game_src src/*.cpp src/*.h)

set(CLIENT_SRC
  ${CLIENT_SRC}
  ${sources_game_src}
)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(
  ${CMAKE_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/src
  ${CMAKE_SOURCE_DIR}/SkyNet/src
  ${ACE_INCLUDE_DIR}
  ${MYSQL_INCLUDE_DIR}
  ${BOOST_INCLUDE_DIR}  
  ${OPENSSL_INCLUDE_DIR}  
)
add_executable(Client ${CLIENT_SRC})
target_link_libraries(Client
${ACE_LIBRARY}
${MYSQL_LIBRARY}
${ACE_SSL_LIBRARY}
SkyNet
)
install(TARGETS Client DESTINATION ${CMAKE_SOURCE_DIR}/bin)
 
if ( UNIX )
	add_definitions("-pg -g -rdynamic")
endif ( UNIX )