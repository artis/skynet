#ifndef __BGRUNNABLE_H
#define __BGRUNNABLE_H
#include <ace/Basic_Types.h>
#include <ace/Thread_Mutex.h>
#include <ace/Singleton.h>
#include "static.h"
#include "Threading/Threading.h"

class ClientRunnable : public ACE_Based::Runnable
{
    public:
        void run();
        void HandleRoomListResponse(ComPacket& response){
        	uint32 rooms = 0;
        	response >> rooms;
        	for( uint i = 0; i < rooms; i++){
        		uint64 guid = 0;
        		uint8 type = 0;
        		response >> guid;
        		response >> type;
        		sLog->outDebug("Room GUID: %lu Type: %d", guid, type);

        	}
        }
};
#endif
