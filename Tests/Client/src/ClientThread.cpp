#include "ClientThread.h"
#include "static.h"
#include "defines.h"
#include "ComPacket.h"
#define BG_SLEEP_CONST 50


void ClientRunnable::run()
{

	while(true){
		ComPacket recv_packet;
		sServer->ReceivePacket(recv_packet);
		switch (recv_packet.GetOpcode()){
		case 0x000B:
			recv_packet.hexlike();
			this->HandleRoomListResponse(recv_packet);

			break;
		}
	}

}
