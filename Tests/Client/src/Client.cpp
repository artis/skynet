// page01.html,v 1.12 2000/11/27 17:56:42 othman Exp

/* To establish a socket connection to a server, we'll need an
  ACE_SOCK_Connector.  */

#include "ace/SOCK_Connector.h"
#include <ace/SSL/SSL_SOCK_Connector.h>
//#include "ace/Log_Msg.h"
#include "ComPacket.h"
#include <iostream>
#include <string>

/* Unlike the previous two tutorials, we're going to allow the user to
  provide command line options this time.  Still, we need defaults in
  case that isn't done.  */

static const int MAX_ITERATIONS = 4;
using namespace std;
struct Account
{
	string username;
	string password;
};
Account account[] = { 
	{ "artis", "123456" }, 
    { "test", "test" }, 
	{ "blah", "blah" }, 
	{ "ouch", "ouch" }, 
	{ "hm", "hm" }, 
} ;
struct ClientPktHeader
{
    uint16 size;
    uint16 cmd;
};
enum ServiceOpcodes
{
	 S_OP_NULL_ACTION					=	0x0000,
	 SCOP_PING							=	0x0001,
	 SSOP_PONG							=	0x0002,
	 SSOP_AUTH_REQUEST					=	0x0003,
	 SCOP_AUTH_SESSION					=	0x0004,
	 SSOP_AUTH_RESPONSE					=	0x0005,
};

#include "static.h"
#include "ClientThread.h"
int
main (int argc, char *argv[])
{

    ACE_SSL_Context *context = ACE_SSL_Context::instance ();
    //context->set_mode(SSLv3_server);
    context->set_mode(10);
    context->certificate ("./dummy.pem", SSL_FILETYPE_PEM);
    context->private_key ("./key.pem", SSL_FILETYPE_PEM);

    string cipher_list = "ALL";
    int status = ::SSL_CTX_set_cipher_list(context->context(), cipher_list.c_str());
    if(status == 1)
        ACE_OS::printf("+ OK.\n");
    else
        ACE_OS::printf("- SSL_CTX_set_cipher_list: %d\n", status);


    string server_host;
    int server_port;
    cout << "Enter server IP: ";
    cin >> server_host;
    cout << "Enter server port: ";
    cin >> server_port;

    string username,password;

    /*cout << "Enter authentication username: ";
    cin >> username;
    cout << "Enter authentication password: ";
    cin >> password;
    */
    username = "artis";
    password = "123456";
    cout << "Connecting..." << endl;

    ACE_INET_Addr addr (server_port, server_host.c_str());

TROJAN_CLIENT_SOCKET sockets[1000];

  /* So, we feed the Addr object and the Stream object to the
    connector's connect() member function. Given this information, it
    will establish the network connection to the server and attach
    that connection to the server object.  */
for(int i = 0; i < 1; i++){
  ComPacket auth(0x0008,50,1);


	  ACE_SSL_SOCK_Connector* connector = new ACE_SSL_SOCK_Connector();
	  if (connector->connect (sockets[i], addr) == -1){
		cout << "Could not connect to remote host." << endl;

		return -1;
	  }else{
		  //cout << "Connected!" << endl;
	  }
	  ComPacket auth1(0x0008,50,1);
	  auth1.clear();
	  auth1 << (uint8)1;
	  auth1 << username;
	  auth1 << password;

	  sockets[i].Send(auth1);

	  ComPacket recv_packet1;
	  sockets[i].ReceivePacket(recv_packet1);
	  sLog->outDebug("Sending Authentication");
	  sServer->Send(auth1);
	  ComPacket recv_packet;
	  sLog->outDebug("Receiving authentication...");
	  sockets[i].ReceivePacket(recv_packet);

//cout << recv_packet.GetOpcode() << "!!!!!" << endl;
	  if(recv_packet.GetOpcode() == SSOP_AUTH_RESPONSE ){
		  uint8 response;
		  recv_packet >> response;
		  switch(response){
		  case 0x01:
			  sLog->outDebug("Authentication Successful.");
			  break;
		  case 0x02:
			  cout << "Authentication failed. Wrong username and/or password." << endl;
			  break;

		  case 0x03:
			  cout << "Authentication failed. You are banned." << endl;
			  break;

		  }
	  }else{
		  cout << "Received unusual response from server. Expected SSOP_AUTH_RESPONSE opcode." << endl;
	  }
        cout <<"Initiated connection " << i << endl;

        ComPacket create(0x000C,10,1);
        create << (uint8)1;
		sLog->outDebug("Creating room...");
        sockets[i].Send(create);

        ComPacket recv_packet_create;
        
        sockets[i].ReceivePacket(recv_packet_create);
        uint8 result_create;
        uint64 room;
        recv_packet_create >> result_create;
        recv_packet_create >> room;
		sLog->outDebug("Created room %lu", room);
        //sLog->outDebug("Room created");
        
        ComPacket join(0x000E,10,1);
        join << (uint64)room;
		sLog->outDebug("Joining room %lu", room);
        sockets[i].Send(join);
        //sLog->outDebug("Room Joined");
		  ACE_OS::sleep(1000);
}

//sleep(1000000000);
  ACE_Based::Thread client_thread(new ClientRunnable);
  	client_thread.setPriority(ACE_Based::Highest);

  system ("export TERM=xterm");
  int action = -1;
  int sub_sub_action = -1;
  while(action != 2){
	  cout << string( 100, '\n' );
	  cout << "Choose your next action:" << endl;
	  cout << "[1] Send a packet to the server." << endl;
	  cout << "[2] Exit." << endl;
	  cout << "Action: ";


	cin >> action;
	cout << "You chose action: " << action << endl;
	ComPacket old_packet(0,100,0);
	switch (action){
	  case 1:{
		  ByteBuffer send_buffer;
		  int sub_action;
		  cout << "Subaction: " << sub_action << endl;
		  while(sub_action != 2){

			  cout << string( 100, '\n' );
			  cout << "Choose your next action:" << endl;
			  cout << "[1] Add variable to the packet." << endl;
			  cout << "[2] Send Packet." << endl;
			  cout << "[3] Re-send Packet." << endl;
			  cout << "Action: ";
			  cin >> sub_action;
			  switch(sub_action){
				  case 1:

					while(sub_sub_action != 0){
						cout << string( 100, '\n' );
						cout << "Choose your next action:" << endl;
						cout << "[1]  Add int8." << endl;
						cout << "[2]  Add int16." << endl;
						cout << "[3]  Add int32." << endl;
						cout << "[4]  Add int64." << endl;
						cout << "[5]  Add uint8." << endl;
						cout << "[6]  Add uint16." << endl;
						cout << "[7]  Add uint32." << endl;
						cout << "[8]  Add uint64." << endl;
						cout << "[9]  Add char." << endl;
						cout << "[10] Add string." << endl;

						cout << "[0]  Done." << endl;
						cout << "Action: ";

						cin >> sub_sub_action;
						switch(sub_sub_action){
							case 1:{
								getchar();
								int var;
								cout << "Value: ";
								cin >> var;

								send_buffer << (int8)var;
								break;
							}
							case 2:{
								int16 var;
								cout << "Value: ";
								cin >> var;
								send_buffer << var;
								break;
							}
							case 3:{
								int32 var;
								cout << "Value: ";
								cin >> var;
								send_buffer << var;
								break;
							}


							case 4:{
								int64 var;
								cout << "Value: ";
								cin >> var;
								send_buffer << var;
								break;
							}
							case 5:{
								getchar();
								uint32 var;
								cout << "Value: ";
								cin >> var;
								send_buffer << (uint8)var;
								break;
							}
							case 6:
							{
								uint16 var;
								cout << "Value: ";
								cin >> var;
								send_buffer << var;
								break;
							}
							case 7:{
								uint32 var;
								cout << "Value: ";
								cin >> var;
								send_buffer << var;
								break;
							}
							case 8:{
								uint64 var;
								cout << "Value: ";
								cin >> var;
								send_buffer << var;
								break;
							}
							case 9:{
								char var;
								cout << "Value: ";
								cin >> var;
								send_buffer << (char)var;
								cout << "Your value '" << var << "'" << endl;
								break;
							}
							case 10:{
								string var;
								cout << "Value: ";
								cin >> var;
								send_buffer << var;
								break;
							}
							case 0:
								sub_sub_action = 0;
								break;
						}
					}
					sub_sub_action = -1;
					break;
				  case 3:
					  sServer->Send(old_packet);
					  cout << "Sending packet" << endl;
					  break;

				  case 2:
					  uint16 opcode, flags;
					  cout << "Enter packet Opcode: ";
					  cin >> opcode;
					  cout << "Enter packet Flags: ";
					  cin >> flags;

					  ComPacket send_packet(opcode, send_buffer.size(), flags);
					  send_packet.append(send_buffer.contents(), send_buffer.size());

					  old_packet.SetOpcode(opcode);
					  old_packet.SetFlag(flags);
					  old_packet.append(send_packet.contents(), send_buffer.size());
					 // if(opcode == 14){
						//  for(int i =1; i<=4; i++)
							//  sServer->Send(send_packet);
					  //}
					  sServer->Send(send_packet);
					  break;

			  }
		  }
		  sub_action = 0;
	  }
		  break;

	  case 2:
		  return 0;
		  break;
	}
  }


  /* Just for grins, we'll send the server several messages.  */

     /* char buf[BUFSIZ];


      ComPacket packet;
      packet << (uint16)18;

      packet << (uint16)4;
      packe12 << (uint16)1;
      string username = "acc1";
      string password = "password";
      packet << username;
      packet << password;

      server.send_n ((char const *)packet.contents(),
                               (size_t)packet.size());
      packet.clear();

      packet << (uint16)8;

      packet << (uint16)1;
      packet << (uint16)0;
      packet << (uint32)0;
      uint32 a;
      uint16 b;

     // packet.PrepareForSending();
      packet.hexlike();

      if (server.send_n ((char const *)packet.contents(),
                         (size_t)packet.size()) == -1)
        ACE_ERROR_RETURN ((LM_ERROR,
                           "%p\n",
                           "send"),
                          -1);
      else
         Pause for a second.  */
        //ACE_OS::sleep (1);


  /* Close the connection to the server.  The servers we've created so
    far all are based on the ACE_Reactor.  When we close(), the
    server's reactor will see activity for the registered event
    handler and invoke handle_input().  That, in turn, will try to
    read from the socket but get back zero bytes.  At that point, the
    server will know that we've closed from our side.  */
  if (sServer->close () == -1)
    ACE_ERROR_RETURN ((LM_ERROR,
                       "%p\n",
                       "close"),
                      -1);
  return 0;
}

