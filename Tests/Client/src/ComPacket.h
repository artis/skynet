#ifndef COMPACKET_H
#define COMPACKET_H

#include "ByteBuffer.h"

enum PacketFlags{
	SERVICE_LAYER_PACKET	=	1,
	ENCRYPTED_PACKET		=	2, // TODO: Implement Encrypted Packtes
	COMPRESSED_PACKET		=	4, // TODO: Implement Packet Compression
	PRIORITY_PACKET			=	8, // TODO: Implement Priority Packets
};
class ComPacket : public ByteBuffer
{
    public:
                                                            // just container for later use
	ComPacket()                                       : ByteBuffer(0), m_opcode(0)
        {
        }
        explicit ComPacket(uint16 opcode, size_t res=200, uint16 flags=0) : ByteBuffer(res), m_opcode(opcode), m_flags(flags) { }
                                                            // copy constructor
        ComPacket(const ComPacket &packet)              : ByteBuffer(packet), m_opcode(packet.m_opcode)
        {
        }
        ~ComPacket(){
        }
        void Initialize(uint16 opcode, size_t newres=200)
        {
            clear();
            _storage.reserve(newres);
            m_opcode = opcode;
        }

        uint16 GetOpcode() const { return m_opcode; }
		void SetOpcode(uint16 opcode) { m_opcode = opcode; }

		uint16 GetFlags() const { return m_flags; }
		void SetFlags(uint16 flags) { m_flags = flags; }
		void SetFlag(uint16 flag){
			m_flags = m_flags | flag;

		}
		bool HasFlag(uint16 flag){
			return (m_flags & flag) != 0;
		}

    protected:
        uint16 m_opcode;
        uint16 m_flags;
};
#endif
