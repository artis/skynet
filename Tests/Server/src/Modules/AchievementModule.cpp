#include "AchievementModule.h"
#include "PlayerImplementation/Player.h"
#include "GeneralOpcodes.h"

AchievementModule::AchievementModule(Player* player)
{
    SetPlayerMember(player);
    sModuleMgr->Subscribe(0,YIELD_TYPE_DATA_PACKET, ACHIEVEMENT_EARNED, this, CREATE_CALLBACK(&AchievementModule::AchievementEarned));
}

AchievementModule::~AchievementModule()
{
    //dtor
}

void AchievementModule::AchievementEarned(Yieldable* data, YieldSubject* subject){
    sLog->outDebug("Player %d earned achievement.", GetPlayer()->GetID());
}
