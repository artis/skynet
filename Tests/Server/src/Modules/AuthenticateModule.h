/*
 * AuthenticateModule.h
 *
 *  Created on: Jun 19, 2012
 *      Author: dimitar
 */

#ifndef AUTHENTICATEMODULE_H_
#define AUTHENTICATEMODULE_H_
#include "SkyNet.h"

enum AuthenticationProtocol{
	AUTH_REGULAR	=	1,
	AUTH_WEB		=	2,
};

enum KongregateAuthSteps{
	FIRST	= 1,
	SECOND	= 2,
};
class AuthenticateModule: public Module {
public:
	AuthenticateModule();
	virtual ~AuthenticateModule();
	void HandleAuthentication(Yieldable* data, YieldSubject* subject);
	void HandleRegularAuthentication(ComPacket& data, GameSession* session);
	void HandleKongregateAuthentication(ComPacket& data, GameSession* session);
	void HandleRegistration(Yieldable* data, YieldSubject* subject);
	AuthenticateModule* Clone(){ return new AuthenticateModule();}
	void TestPayment(Yieldable* data, YieldSubject* subject){
		ExternalProcedure* payment = data->ConvertTo<ExternalProcedure>();
		sLog->outDebug("Player %d paid for %d credits", payment->GetArg1(), payment->GetArg2());
	};

	void TestPaymentCancel(Yieldable* data, YieldSubject* subject){
		ExternalProcedure* payment = data->ConvertTo<ExternalProcedure>();
		sLog->outDebug("Player %d canceled payment for his %d credits", payment->GetArg1(), payment->GetArg2());
	};
	void Test2(Yieldable* data, YieldSubject* subject){sLog->outDebug("HURAY!");};

int player_id;
};

#endif /* AUTHENTICATEMODULE_H_ */
