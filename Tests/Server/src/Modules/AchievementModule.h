#ifndef ACHIEVEMENTMODULE_H
#define ACHIEVEMENTMODULE_H

#include "SkyNet.h"

class Player;
class AchievementModule : public Module
{
    class Achievement{
        public:
            Achievement(int id):m_id(id){};
        private:
        int m_id;
    };
    public:
        AchievementModule(Player* player = NULL);
        virtual ~AchievementModule();

        void GrantAchievement(int id){
            m_achievements.push_back(Achievement(id));
        }

        /* Module Callbacks Begin */
        void AchievementEarned(Yieldable* data, YieldSubject* subject);
        /* Module Callbacks End */

        Player* GetPlayer(){ return reinterpret_cast<Player*>(Module::GetPlayer());}

        AchievementModule* Clone(){ return new AchievementModule(*this);}

    protected:
    private:
    vector<Achievement> m_achievements;

};

#endif // ACHIEVEMENTMODULE_H
