/*
 * AuthValidator.h
 *
 *  Created on: Oct 25, 2012
 *      Author: dimitar
 */

#ifndef AUTHVALIDATOR_H_
#define AUTHVALIDATOR_H_
#include "SkyNet.h"

class AuthValidator:public ModuleValidator {
public:
	AuthValidator();
	virtual ~AuthValidator();
	bool HandleAuthentication(Yieldable* data, YieldSubject* subject){
		//sLog->outDebug("Validator called.");
		return true;
	}
};

#endif /* AUTHVALIDATOR_H_ */
