/*
 * AuthenticateModule.cpp
 *
 *  Created on: Jun 19, 2012
 *      Author: dimitar
 */

#include "AuthenticateModule.h"
#include <string>
#include "Connection/ServiceOpcodes.h"
#include "PlayerImplementation/Player.h"
#include "Implementation/Database/Database.h"

AuthenticateModule::AuthenticateModule() {
    player_id = 0;
	sModuleMgr->Subscribe(0,YIELD_TYPE_SESSION , AUTHENTICATE, this, CREATE_CALLBACK(&AuthenticateModule::HandleAuthentication));
	sModuleMgr->Subscribe(0,YIELD_TYPE_SESSION , REGISTER, this, CREATE_CALLBACK(&AuthenticateModule::HandleRegistration));

}

AuthenticateModule::~AuthenticateModule() {
}

void AuthenticateModule::HandleAuthentication( Yieldable* data, YieldSubject* subject)
{
	//sLog->outDebug("AuthenticateModule::HandleAuthentication");
	ComPacket& packet = *data->ConvertTo<ComPacket>();
	GameSession* session;
	session = subject->ConvertTo<GameSession>();
	if(!session){
        sLog->outDebug("AuthenticateModule::HandleAuthentication: Bad pointer conversion");
        return;
    }

	uint8 auth_type;
	packet >> auth_type;

	switch (auth_type)
	{
	case AUTH_REGULAR:
		HandleRegularAuthentication(packet, session);
		break;
	case AUTH_WEB:
		HandleKongregateAuthentication(packet, session);
		break;
	}

	
}


void AuthenticateModule::HandleRegularAuthentication(ComPacket& data, GameSession* session)
{
	sLog->outDebug("Received regular authentication request.");
	std::string username;
	std::string password;

	data >> username;
	data >> password;


    PreparedStatement* stmt = MainDatabase.GetPreparedStatement(MAIN_GET_ACCOUNT_VALIDATION);

    stmt->setString(0, username);
    stmt->setString(1, password);


	Player* plr = new Player(session);
    session->SetPlayer(plr);

	plr->RegisterAuthCallback(MainDatabase.AsyncQuery(stmt));
}
void AuthenticateModule::HandleKongregateAuthentication(ComPacket& data, GameSession* session)
{
	sLog->outDebug("Received Web authentication request.");
	string accID;
	string sessionToken;

	data >> accID;
	data >> sessionToken;

	sLog->outDebug("User: %s Session Token: %s", accID.c_str(), sessionToken.c_str());


	// Check if the provided kongregateID is actually registered.
	PreparedStatement* stmt = MainDatabase.GetPreparedStatement(MAIN_GET_ACC_BY_ID);

	stmt->setString(0, accID);

	PreparedQueryResult result = MainDatabase.Query(stmt);
	if(result)
	{
		string dbSessionToken;
		Field* fields = result->Fetch();
		dbSessionToken = fields[1].GetString();
		uint32 userID = fields[0].GetUInt32();
		if(dbSessionToken == sessionToken)
		{

			Player* plr = new Player(session);
			session->SetPlayer(plr);
			plr->SetID(userID);
			sGameCore->AddPlayer(plr);

			ComPacket response(5, 1, 1);
			response << (uint8)AUTH_RESULT_SUCCESS;
			plr->SendPacket(response);
			sLog->outDebug("Authentication successful!");
		}
		else
			sLog->outDebug("Authentication failed!");

		
	}
	else
	{
		sLog->outDebug("Authentication failed!");
	}
		

}

void AuthenticateModule::HandleRegistration( Yieldable* data, YieldSubject* subject)
{
}