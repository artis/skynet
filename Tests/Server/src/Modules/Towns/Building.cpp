#include "Building.h"

Building::Building()
{
    //ctor
}

Building::~Building()
{
    //dtor
}

void Building::_Update(uint32 diff){
    switch(GetPhase())
    {
        case PHASE_BUILDING:
            _UpdateBuild(diff);
            break;

        case PHASE_UPGRADING:
            _UpdateUpgrade(diff);
            break;

        default:
            Update(diff);
            break;
    }
}

void Building::_LoadFromDB(){
    // TODO: Load generic building state from DB (level, upgrade timers, etc.).

    LoadFromDB();
}
void Building::_UpdateBuild(uint32 diff)
{
    if(GetPhase() != PHASE_BUILDING)
        return;

    if(m_buildTimer <= diff)
    {
        SetPhase(PHASE_READY);
    }else{
        m_buildTimer -= diff;
    }

};
