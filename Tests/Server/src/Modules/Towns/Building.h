#ifndef BUILDING_H
#define BUILDING_H
#include "SkyNet.h"

enum Phase{
    PHASE_READY  = 1,
    PHASE_BUILDING,
    PHASE_UPGRADING,
};
enum BuildingType{
    BUILDING_TYPE_BARRACK   = 1,
    BUILDING_TYPE_FARM      = 2,
    BUILDING_TYPE_HOUSE     = 3,
};
class Town;
class Building
{
    public:
        Building();
        virtual ~Building();

        Town* GetTown() { return m_town; }
        virtual void Update(uint32 diff) { };
        uint8 GetType() const { return m_type; }
        Phase GetPhase() const { return m_phase; }

        virtual Building* Clone() = 0;
        virtual void LoadFromDB() { };

        void _SetTown(Town* town){ m_town = town; }
        void _LoadFromDB();
        void _Update(uint32 diff);
        void _UpdateBuild(uint32 diff);
        void _UpdateUpgrade(uint32 diff) { };
    protected:
    private:
        void SetPhase(Phase phase) { m_phase = phase; };
        Town* m_town;
        uint8 m_type;
        Phase m_phase;
        uint8 m_buildTimer;
};

#define sBuildingFactory ACE_Singleton<TemplateHolder<Building>, ACE_Null_Mutex>::instance()


#endif // BUILDING_H
