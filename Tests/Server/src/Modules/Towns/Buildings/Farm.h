#ifndef FARM_H
#define FARM_H

#include "../Building.h"


class Farm : public Building
{
    public:
        Farm() {
            sBuildingFactory->RegisterTemplate(1,this);
        }
        virtual ~Farm() {}

        Farm* Clone(){ return new Farm(*this); }
    protected:
    private:
};

#endif // FARM_H
