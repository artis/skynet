#ifndef TOWNMGR_H
#define TOWNMGR_H
#include "SkyNet.h"


class Building;
class Player;
class Town: public Module
{
    public:
        Town(Player* owner);
        virtual ~Town();
        Player* GetPlayer() { return m_player; }
        bool Update(uint32 diff);
        void Build(uint8 buildingType);

        Town* Clone(){ return new Town(NULL);}
    protected:
    private:
        Player* m_player;
        vector<Building*> m_buildings;
};

#endif // TOWNMGR_H
