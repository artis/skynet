#include "TownMgr.h"
#include "Building.h"

Town::Town(Player* owner): m_player(owner)
{
    Build(1);
}

Town::~Town()
{
}

bool Town::Update(uint32 diff)
{
    for(size_t i = 0; m_buildings.size(); i++)
        m_buildings[i]->_Update(diff);

	return true;
}

void Town::Build(uint8 BuildingType){
    //sLog->outDebug("Town::Build()");
    Building* instance = sBuildingFactory->GenerateObject(BuildingType);
    if(!instance)
        return;

    instance->_SetTown(this);

    m_buildings.push_back(instance);
}
