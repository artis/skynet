#pragma once
#ifndef _PAYMENTMODULE_H_
#define _PAYMENTMODULE_H_

#include "SkyNet.h"

class PaymentModule: public Module
{
public:
	PaymentModule(void);
	virtual ~PaymentModule(void);

	void HandlePayment(Yieldable* data, YieldSubject* subject);
	void HandlePaymentCancel(Yieldable* data, YieldSubject* subject);

	PaymentModule* Clone() { return new PaymentModule(*this); };

};

#endif