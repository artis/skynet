#include "PaymentModule.h"
#include "PlayerImplementation/Player.h"

PaymentModule::PaymentModule(void)
{

	sModuleMgr->Subscribe(0, EXTERNAL_PROCEDURE, 1, this, CREATE_CALLBACK(&PaymentModule::HandlePayment));
	sModuleMgr->Subscribe(0, EXTERNAL_PROCEDURE, 2, this, CREATE_CALLBACK(&PaymentModule::HandlePaymentCancel));
}


PaymentModule::~PaymentModule(void)
{
}

void PaymentModule::HandlePayment(Yieldable* data, YieldSubject* subject)
{
	ExternalProcedure* payment = data->ConvertTo<ExternalProcedure>();
	sLog->outDebug("Player %d paid for %d credits", payment->GetArg1(), payment->GetArg2());
	uint32 playerID = (uint32)payment->GetArg1();
	uint32 currency = payment->GetArg2();
	Player* player = static_cast<Player*>(sPlayerRegistry->FindPlayer(playerID));
	
	if(player)
	{
		sLog->outDebug("Player is online and has %d currency.", player->GetCurrency());
		player->AddCurrency(currency);
		player->Save();
		sLog->outDebug("Player has %d currency now.", player->GetCurrency());
	}
	else
	{
		sLog->outDebug("Player is offline, adding currency in the database.");
		MainDatabase.PExecute("UPDATE `users` SET `currency` = `currency` + '%d' WHERE `id` = '%d'", currency, playerID);
	}

}
void PaymentModule::HandlePaymentCancel(Yieldable* data, YieldSubject* subject) 
{
};
