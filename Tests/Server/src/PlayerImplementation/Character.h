#pragma once
#ifndef _CHARACTER_H_
#define _CHARACTER_H_
#include "common.h"
#include "defines.h"
#include "GameCore/SkyNet_Object.h"

namespace SkyNet{
	namespace Core{
		class BasePlayer;
	}
}
using namespace SkyNet::Core;
class Character: public SkyNet_Object 
{
public:
	Character(BasePlayer* player);
	virtual ~Character(void);

	/*! Returns the owner BasePlayer object. */
	BasePlayer* GetPlayer() { return m_player; }

	uint8 GetLevel() { return m_level; };
	void Levelup() { m_level++; };
private:

	BasePlayer* m_player;
	uint8 m_level;
};

#endif