/*
 * Player.h
 *
 *  Created on: Oct 17, 2012
 *      Author: dimitar
 */

#ifndef PLAYER_H_
#define PLAYER_H_
#include "SkyNet.h"
#include "Implementation/Database/Database.h"

class AchievementModule;
class Town;
class Player: public BasePlayer {
public:
	Player(): BasePlayer(NULL){};
	Player(GameSession* session);
	virtual ~Player();

	void Save();
	Town* GetTown() { return m_town; }


	void LoadFromDB();
	uint32 GetCurrency() const{ return m_currency; }
	void SetCurrency(uint32 amount) { m_currency = amount; }
	void AddCurrency(uint32 amount) { m_currency += amount; }

	void HandleAuthentication(PreparedQueryResult& result);
	private:
	AchievementModule* m_achievementMgr;
	Town* m_town;
	uint32 m_currency;
};

#endif /* PLAYER_H_ */
