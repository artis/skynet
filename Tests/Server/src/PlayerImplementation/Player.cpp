/*
 * Player.cpp
 *
 *  Created on: Oct 17, 2012
 *      Author: dimitar
 */

#include "Player.h"
#include "Modules/AchievementModule.h"
#include "Modules/Towns/TownMgr.h"
#include "GameCore/PlayerRegistry.h"

Player::Player(GameSession* session):BasePlayer(session) {
    m_achievementMgr = new AchievementModule(this);
    m_town = new Town(this);
}

Player::~Player() {
    delete m_town;
}
void Player::Save()
{
	MainDatabase.PExecute("UPDATE `users` SET `currency` = '%d' WHERE `id` = '%d'", this->GetCurrency(), this->GetID());
}
void Player::LoadFromDB()
{
	QueryResult result = MainDatabase.PQuery("SELECT `currency` FROM `users` WHERE `id` = '%d'", GetID());
	if(result)
	{
		Field* fields = result->Fetch();
		SetCurrency(fields[0].GetUInt32());

	}
	return;
};
void Player::HandleAuthentication(PreparedQueryResult& result)
{
	ComPacket response(5, 1, 1);

	if (result) {
        Field* fields = result->Fetch();
        uint32 userId = fields[0].GetUInt32();

		SetID(sGameCore->GetPlayerID());
        sGameCore->AddPlayer(this);
        response << (uint8)AUTH_RESULT_SUCCESS;
		//sLog->outDebug("Authentication successful");
		LoadFromDB();
		sPlayerRegistry->RegisterPlayer(this);
    } else {
        response << (uint16)AUTH_RESULT_FAILED_WRONG_INFO;
        sLog->outDebug("Authentication failed:");
    }


	SendPacket(response);
}
