#include "SkyNet.h"
#include "MainDB.h"

using namespace SkyNet::Database;
MainDBDatabaseWorkerPool MainDatabase;

void MainDBDatabaseConnection::DoPrepareStatements()
{
    if (!m_reconnecting)
        m_stmts.resize(MAX_MAINDATABASE_STATEMENTS);


	PREPARE_STATEMENT(MAIN_GET_ACCOUNT_VALIDATION, "SELECT `id` FROM `users` WHERE `username` like ? AND `password`=?", CONNECTION_ASYNC);
	PREPARE_STATEMENT(MAIN_GET_ACC_BY_ID, "SELECT `id`, `sessionToken` FROM `users` WHERE `id` = ?", CONNECTION_SYNCH);
		


}

