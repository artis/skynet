#ifndef _MAINDATABASE_H
#define _MAINDATABASE_H

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"
using namespace SkyNet::Database;
class MainDBDatabaseConnection : public MySQLConnection
{
    public:
        //- Constructors for sync and async connections
	MainDBDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) {}
	MainDBDatabaseConnection(ACE_Activation_Queue* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) {}

    //- Loads database type specific prepared statements
    void DoPrepareStatements();
};


typedef DatabaseWorkerPool<MainDBDatabaseConnection> MainDBDatabaseWorkerPool;

enum MainDatabaseStatements
{
    /*  Naming standard for defines:
        {DB}_{SET/DEL/ADD/REP}_{Summary of data changed}
        When updating more than one field, consider looking at the calling function
        name for a suiting suffix.
    */
    MAIN_GET_ACCOUNT_VALIDATION,
	MAIN_GET_ACC_BY_ID,
    MAX_MAINDATABASE_STATEMENTS,
};

#endif
