#include "SkyNet.h"
#include "Database.h"



bool InitDB(){
	
    std::string dbstring;
    uint8 async_threads, synch_threads;
	sConfig->SetSource("./skynet.conf");
    ///- Get login database info from configuration file
    dbstring = sConfig->GetStringDefault("MainDatabaseInfo", "77.70.124.69;3306;root;Buub1es;MainDB");
    if (dbstring.empty())
    {
        sLog->outError("Main database not specified in configuration file");
        return false;
    }

    async_threads = sConfig->GetIntDefault("MainDatabase.WorkerThreads", 1);
    if (async_threads < 1 || async_threads > 32)
    {
        sLog->outError("Main database: invalid number of worker threads specified. "
            "Please pick a value between 1 and 32.");
        return false;
    }

    synch_threads = sConfig->GetIntDefault("MainDatabase.SynchThreads", 1);
    ///- Initialise the login database
    if (!MainDatabase.Open(dbstring, async_threads, synch_threads))
    {
        sLog->outError("Cannot connect to main database %s", dbstring.c_str());
        return false;
    }

	
	
    return true;
}
