#ifndef SHOPITEMENTRY_H
#define SHOPITEMENTRY_H
#include "common.h"
#include "Database/DatabaseEnv.h"

class ShopItemEntry
{
    public:
        ShopItemEntry(Field* fields){
            entry       = fields[0] .GetUInt32();
            name        = fields[1] .GetString();
            currency1   = fields[2] .GetUInt32();
            currency2   = fields[3] .GetUInt32();
        }
        virtual ~ShopItemEntry();
    protected:
    private:
        uint32 entry;
        string name;
        uint32 currency1;
        uint32 currency2;
};

#endif // SHOPITEMENTRY_H
