#ifndef DATASTORAGE_H_INCLUDED
#define DATASTORAGE_H_INCLUDED
#include "SkyNet.h"
#include "Implementation/ShopItemEntry.h"

typedef LSDStorage<ShopItemEntry> ShopItemStore;

void LoadLSDStores();
#define sShopItemStore ACE_Singleton<ShopItemStore, ACE_Null_Mutex>::instance()

#endif // DATASTORAGE_H_INCLUDED
