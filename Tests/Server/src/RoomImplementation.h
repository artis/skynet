/*
 * RoomImplementation.h
 *
 *  Created on: Oct 18, 2012
 *      Author: dimitar
 */

#ifndef ROOMIMPLEMENTATION_H_
#define ROOMIMPLEMENTATION_H_
#include "RoomImplementation/LobbyRoom.h"

enum RoomPools{
	ROOM_POOL_LOBBY	= 1,
	ROOM_POOL_GAME	= 2,

};

enum RoomTypes{
	ROOM_TYPE_LOBBY			= 1,
	ROOM_TYPE_TOWER_DEFENSE	= 2,
};


#endif /* ROOMIMPLEMENTATION_H_ */
