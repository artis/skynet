/*
 * LobbyRoom.h
 *
 *  Created on: Oct 19, 2012
 *      Author: dimitar
 */

#ifndef LOBBYROOM_H_
#define LOBBYROOM_H_
#include "SkyNet.h"

class LobbyRoom: public Room {
public:
	LobbyRoom();
	virtual ~LobbyRoom();
	bool Update(uint32 diff){
		return true;
	}
	virtual LobbyRoom* Clone(){
		return new LobbyRoom();
	}
};

#endif /* LOBBYROOM_H_ */
