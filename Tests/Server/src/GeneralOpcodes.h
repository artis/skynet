#ifndef GENERALOPCODES_H_INCLUDED
#define GENERALOPCODES_H_INCLUDED

//The first 1/2 of the opcodes are reserved for the games. Second half is for general usage.

enum GeneralOpcodes{
    GENERAL_OPCODES_OFFEST  =   0x7FFF,
    ACHIEVEMENT_EARNED,
    ACHIEVEMENT_LIST,
    QUERY_SHOP_ITEM,

};

#endif // GENERALOPCODES_H_INCLUDED
