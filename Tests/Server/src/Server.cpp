#include "SkyNet.h"
#include "Coordinator/SkyNetCoordinator.h"
#include "RoomImplementation.h"
#include "Modules/AuthenticateModule.h"
#include "Modules/AuthValidator.h"
#include "Implementation/Database/Database.h"
#include "DataStorage/DataStorage.h"
#include "Modules/Towns/Buildings/Farm.h"
#include "Modules/Shop/PaymentModule.h"
#include "PlayerImplementation/Player.h"

using namespace SkyNet;

#define sPlayerObjectPool ACE_Singleton<ObjectPool<Player>, ACE_Null_Mutex>::instance()
int main(int argc, char *argv[])
{

/*	std::vector<Player*> players;
	const int num = 1000000;
	players.reserve(num);
	sPlayerObjectPool->Initialize();
	uint32 start = getMSTime();
	for(int i = 0; i < num; i++)
	{
		Player* plr = sPlayerObjectPool->Build();
		players.push_back(plr);
	}
	uint32 stop = getMSTime();
	sLog->outDebug("Allocated %d instances of Player object for %d ms", num, stop-start);


	
	for(int i = 0; i < num; i++)
	{
		sPlayerObjectPool->Recycle(players[i]);

	}
	players.clear();
    players.reserve(num);
	start = getMSTime();
	for(int i = 0; i < num; i++)
	{
		Player* plr = sPlayerObjectPool->Build();
		players.push_back(plr);
	}
	stop = getMSTime();
	sLog->outDebug("Allocated %d instances of Player object for %d ms", num, stop-start);
	sLog->outDebug("Test Done");

	ACE_OS::sleep(1000000000);*/

   	new AuthenticateModule();
	new AuthValidator();
    new Farm();
	new PaymentModule;



	//sLog->AddSystemFilter(ExternalProcedures);
	getMSTime();
	SkyNetCoordinator brain;
	brain.Initialize();
	InitDB();
    LoadLSDStores();
    brain.AddAcceptor("0.0.0.0", 3803);
    //brain.AddAcceptor("127.0.0.2", 3803);

	brain.RegisterRoomGroup(ROOM_POOL_LOBBY, 1);
	brain.RegisterRoomGroup(ROOM_POOL_GAME, 2);


	brain.RegisterRoomScript(ROOM_TYPE_LOBBY, ROOM_POOL_LOBBY, new LobbyRoom());
	brain.CreateRoom(ROOM_TYPE_LOBBY);
	brain.Prime();
	brain.Wait();

	return 0;
}
