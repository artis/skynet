file(GLOB_RECURSE sources_clientcluster_src src/*.cpp src/*.h)

set(CLIENT_SRC
  ${CLIENT_SRC}
  ${sources_clientcluster_src}
)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(
  ${CMAKE_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/src
  ${CMAKE_SOURCE_DIR}/SkyNet/src
  ${CMAKE_SOURCE_DIR}/SkyNet/src/Logging
  ${CMAKE_SOURCE_DIR}/SkyNet/src/Database
  ${CMAKE_SOURCE_DIR}/SkyNet/src/Threading
  ${CMAKE_SOURCE_DIR}/SkyNet/src/Utilities
  ${ACE_INCLUDE_DIR}
  ${MYSQL_INCLUDE_DIR}
  ${BOOST_INCLUDE_DIR}  
  ${OPENSSL_INCLUDE_DIR}  
)
add_executable(ClusterClient ${CLIENT_SRC})
target_link_libraries(ClusterClient
${ACE_LIBRARY}
${MYSQL_LIBRARY}
${ACE_SSL_LIBRARY}
SkyNet
)
install(TARGETS ClusterClient DESTINATION ${CMAKE_SOURCE_DIR}/bin)
 
if ( UNIX )
	add_definitions("-pg -g -rdynamic")
endif ( UNIX )