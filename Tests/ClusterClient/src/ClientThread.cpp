#include "ClientThread.h"
#include "static.h"
#include "defines.h"
#include "ComPacket.h"
#define BG_SLEEP_CONST 50


void ClientRunnable::run()
{
	m_recvFile = NULL;
	sLog->outDebug("Running the client thread");
	while(true){
		ComPacket recv_packet;
		if(!sServer->ReceivePacket(recv_packet))
			return;

		string name;
		string path;
		if(recv_packet.HasFlag(1))
			switch (recv_packet.GetOpcode()){
			case 19:
				
				int size;
				recv_packet >> path;
				recv_packet >> name;
				recv_packet >> size;
				m_recvFile = new SkyNet_File(size);
				//sLog->outDebug("Received File %s header size %d Total packets expected %d", name.c_str(), m_recvFile->GetSize(), m_recvFile->GetNumPackets()+1);
				m_recvFile->SetFilename(name);
				//sLog->outDebug("Current rpos %d ", recvPacket.rpos());
				recv_packet.rpos(0);

				m_recvFileData.clear();
				m_recvFileData.push_back(recv_packet);
				break;
			case 20:
				
					m_recvFileData.push_back(recv_packet);
					//sLog->outDebug("Received File data size %d total %d expecting %d", recv_packet.size(), m_recvFileData.size(),m_recvFile->GetNumPackets()+1);
					if(m_recvFile->GetNumPackets()+1 == m_recvFileData.size())
					{	
						//sLog->outDebug("Received the %d packet. We are ready to build a file.",m_recvFileData.size());

						m_recvFile->LoadFromPackets(m_recvFileData);
						this->HandleFileSend(m_recvFile);
						m_recvFile = NULL;
					}
				break;
			}

		else
		switch (recv_packet.GetOpcode()){
		case 1:
		
			this->HandleFileList(recv_packet);

			break;
		
		case 2:
			try{
				this->HandleFileRequest(recv_packet);
			}catch(...){
				sLog->outDebug("Exception caught");
			}
			break;




		}
		
	
	}

}
