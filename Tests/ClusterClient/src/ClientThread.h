#ifndef __BGRUNNABLE_H
#define __BGRUNNABLE_H
#include <ace/Basic_Types.h>
#include <ace/Thread_Mutex.h>
#include <ace/Singleton.h>
#include "static.h"
#include "Threading/Threading.h"

class ClientRunnable : public SkyNet::Threading::Runnable
{
	SimpleTimer m_timer;
	vector<ComPacket> m_requests;
	vector<ComPacket> m_recvFileData;
	SkyNet_File* m_recvFile;
    public:
        void run();
		void HandleFileSend(SkyNet_File* file){
			//sLog->outDebug("Downloading %s", file->GetFilename().c_str());
			string del = "/";
			string base_dir = "cache/";
			//sLog->outDebug("Saving file %s", file->GetFilename().c_str());
			file->Save(base_dir + file->GetDestinationPath(), file->GetFilename());
			//sLog->outDebug("Saved file %s", file->GetFilename().c_str());
			sLog->outDebug("Downloading %s", file->GetDestinationPath().c_str());
			
			//sLog->outDebug("Re-reading file structure");
			//sendLock->acquire();
			sLog->outDebug("HandleFileSend: adding %s/%s", file->GetDestinationPath().c_str(), file->GetFilename().c_str());
			sCacheFileManager->AddFile(CacheFile(file->GetDestinationPath(),file->GetFilename()));
			delete file;
			//sCacheFileManager->ReadFiles();
			//sendLock->release();
			//sLog->outDebug("Done");
		}
		void HandleFileList(ComPacket& response){
			//sLog->outDebug("HandleFileList() %d",response.size());

			int num;
			response >> num;
			//sLog->outDebug("HandleFileList() %d",num);
			ComPacket request(2, 200);
			int requests = 0;
			for(int i = 0; i < num; i++)
			{
			
				string dir;
				string name;
			
				response >> dir;
				response >> name;

				//sLog->outDebug("%s",name.c_str());
				CacheFile file(dir,name);
				if(!sCacheFileManager->CheckIfFileIsAlreadyRead(file))
				{
					requests++;
				}
			}

			response.rpos(0);
			response >> num;

			request << requests;
			if(requests > 0)
			for(int i = 0; i < num; i++)
			{
				string dir;
				string name;
			
				response >> dir;
				response >> name;


				CacheFile file(dir,name);
				if(!sCacheFileManager->CheckIfFileIsAlreadyRead(file))
				{
					request << dir;
					request << name;
					sLog->outDebug("Requesting %s/%s", dir.c_str(), name.c_str());
					requests++;
				}
			}

			if(requests > 0)
				sServer->Send(request);



        }
		
		void HandleFileRequest(ComPacket& response)
		{
			sLog->outDebug("Received file request");
			string dir;
			string name;
			
			int num;
			response >> num;
			string base_dir = "cache/";
			string del = "/";
			for(int i = 0; i < num; i++)
			{
				string dir;
				string file;
				response >> dir;
				response >> file;
				sLog->outDebug("Uploading %s",(dir + del + file).c_str());
				SkyNet_File cache(base_dir + dir + del + file, file, dir);
				//sendLock->acquire();
				sServer->Send(cache);
				
				//sendLock->release();
			}
		}
        void HandleRoomListResponse(ComPacket& response){
        	uint32 rooms = 0;
        	response >> rooms;
        	for( uint i = 0; i < rooms; i++){
        		uint64 guid = 0;
        		uint8 type = 0;
        		response >> guid;
        		response >> type;
        		sLog->outDebug("Room GUID: %lu Type: %d", guid, type);

        	}
        }
};
#endif
