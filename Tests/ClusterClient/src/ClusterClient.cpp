#include <iostream>
#include <fstream>
#include "SkyNet.h"
#include "static.h"
#include "Coordinator/SkyNetCoordinator.h"
#include "ace/SOCK_Connector.h"
#include <ace/SSL/SSL_SOCK_Connector.h>
#include "ClientThread.h"

#include "ace/OS_NS_sys_stat.h"
#include "ace/OS_NS_unistd.h"
#include "ace/SString.h"
#include <exception>

using namespace std;
using namespace SkyNet::Networking;



int main(int argc, char *argv[])
{


	//source.Save("test.png");
	ACE_INET_Addr addr (1234, "127.0.0.1");
	while(true){
		ACE_SSL_SOCK_Connector* connector = new ACE_SSL_SOCK_Connector();
		sServer->close();
		if (connector->connect (*sServer, addr) == -1){
			cout << "Could not connect to remote host. Will try again in 5 seconds..." << endl;

			sServer->SetConnected(false);
			ACE_OS::sleep(5);
			continue;

		}else{
			cout << "Conected to server." << endl;
			sServer->SetConnected(true);
		}
		int b;
		ComPacket recv_packet1;
		sServer->ReceivePacket(recv_packet1);

		ComPacket auth(0x0008,50,1);
		auth.clear();
		auth << (uint8)1;

		sServer->Send(auth);
		ComPacket recv_packet2;
		sServer->ReceivePacket(recv_packet2);

		ComPacket join(0x000E,10,1);
		join << (uint64)1;
		//sLog->outDebug("Joining room %lu", 1);

		sServer->Send(join);

		SkyNet::Threading::Thread client_thread(new ClientRunnable);
  		client_thread.setPriority(SkyNet::Threading::Highest);

	
		sCacheFileManager->Clear();
		vector<CacheFile> files = sCacheFileManager->ReadFiles();

		sLog->outDebug("There are a total of %d files",files.size());

		/*while(!files.empty())
		{
			ComPacket* data = new ComPacket(1,20000);

			int num = files.size();

			if(num > 50)
				num = 50;

			//sLog->outDebug("Preparing packet with size %d",num);
			(*data) << num;

			for(int i = 0; i < num; i++)
			{
				CacheFile file = files.back();
				files.pop_back();
				string dir = file.dir;
				string name = file.name;
				//sLog->outDebug("File dir %s name %s size %d",dir.c_str(), name.c_str(), data->size());
				(*data) << dir;
				(*data) << name;
			}
			//sendLock->acquire();
			
			sServer->Send((*data));
			delete data;

			//sendLock->release();

		}*/
		while(true)
		{
			//sendLock->acquire();
			files = sCacheFileManager->ReadFiles();
			//sendLock->release();
			string base_dir = "cache/";
			string del = "/";
			int n = 0;
			//sLog->outDebug("Sending %d files",files.size());
			//cin >> n;
			while(files.size() > 0){

				try{
				CacheFile file = files.back();
				SkyNet_File cache(base_dir + file.dir + del + file.name, file.name, file.dir);
				sLog->outDebug("Uploading %s",(file.dir + del + file.name).c_str());
				sServer->Send(cache);
				files.pop_back();
				//sLog->outDebug("Sending file %d", i);
				n++;
				}
				catch(std::exception& ex){
					sLog->outDebug("EXCEPTION %s", ex.what());
				}
			
			}
			if(!sServer->IsConnected()){
				sLog->outError("Lost connection to synch server.");
				sLog->outNormal("Will attempt to reconnect in 5 seconds.");
				break;
			}
			ACE_OS::sleep(5);
		}
	}
	int size;
	cin >> size;
	return 0;
}
