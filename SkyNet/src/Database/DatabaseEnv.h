#ifndef DATABASEENV_H
#define DATABASEENV_H

#include "defines.h"
#include "Database/DatabaseDefines.h"

#define _LIKE_           "LIKE"
#define _TABLE_SIM_      "`"
#define _CONCAT3_(A, B, C) "CONCAT( " A " , " B " , " C " )"
#define _OFFSET_         "LIMIT %d, 1"

#include "Implementation/SkyNetDatabase.h"

extern SkyNetDatabaseWorkerPool SkyNetDatabase;
extern StorageDatabaseWorkerPool StorageDatabase;

#endif

