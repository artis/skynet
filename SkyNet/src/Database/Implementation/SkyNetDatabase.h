#ifndef _SKYNETDATABASE_H
#define _SKYNETDATABASE_H

#define SKYNET_API __declspec(dllexport)

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"

class SkyNetDatabaseConnection : public MySQLConnection
{
    public:
        //- Constructors for sync and async connections
	SkyNetDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) {}
	SkyNetDatabaseConnection(ACE_Activation_Queue* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) {}

        //- Loads database type specific prepared statements
    void DoPrepareStatements();
};

class StorageDatabaseConnection : public MySQLConnection
{
    public:
        //- Constructors for sync and async connections
	StorageDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) {}
	StorageDatabaseConnection(ACE_Activation_Queue* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) {}

        //- Loads database type specific prepared statements
    void DoPrepareStatements(){};
};

typedef SkyNet::Database::DatabaseWorkerPool<SkyNetDatabaseConnection> SkyNetDatabaseWorkerPool;
typedef SkyNet::Database::DatabaseWorkerPool<StorageDatabaseConnection> StorageDatabaseWorkerPool;


enum SkyNetDatabaseStatements
{
    /*  Naming standard for defines:
        {DB}_{SET/DEL/ADD/REP}_{Summary of data changed}
        When updating more than one field, consider looking at the calling function
        name for a suiting suffix.
    */
	GET_EXTERNAL_PROCEDURES,
    MAX_SKYNETDATABASE_STATEMENTS,
};

#endif
