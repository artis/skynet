
#include "SkyNetDatabase.h"


void SkyNetDatabaseConnection::DoPrepareStatements()
{
    if (!m_reconnecting)
        m_stmts.resize(MAX_SKYNETDATABASE_STATEMENTS);

    PREPARE_STATEMENT(GET_EXTERNAL_PROCEDURES, "SELECT * FROM `external_procedures` ORDER BY `id` ASC", CONNECTION_ASYNC);
	
}
SkyNetDatabaseWorkerPool SkyNetDatabase;
StorageDatabaseWorkerPool StorageDatabase;

