/*
 * RoomUpdateRunnable.cpp
 *
 *  Created on: Oct 8, 2012
 *      Author: dimitar
 */

#include "RoomUpdateRunnable.h"
#include "Threading/Threading.h"
#include "Connection/GameSocketMgr.h"
#include "common.h"
#include "defines.h"

using namespace SkyNet::Threading;

RoomUpdateRunnable::RoomUpdateRunnable() {
	setMaps(0);

}

RoomUpdateRunnable::~RoomUpdateRunnable() {
}

void RoomUpdateRunnable::run()
{

	//m_mapMgr = MapMgr();
	m_crashed = false;
	maps = 0;
    uint32 realCurrTime = 0;
    uint32 realPrevTime = getMSTime();

    uint32 prevSleepTime = 0;                               // used for balanced full tick time length near LOBBY_SLEEP_CONST
    m_LoopCounter = 0;
    while (!GameCore::IsStopped())
    {
        ++m_LoopCounter;
        realCurrTime = getMSTime();

        uint32 diff = getMSTimeDiff(realPrevTime,realCurrTime);

        m_mapMgr.Update( diff );
        realPrevTime = realCurrTime;


        if(diff > 55)
            sLog->outDebug("Updater diff: %d ms", diff);
        // diff (D0) include time of previous sleep (d0) + tick time (t0)
        // we want that next d1 + t1 == WORLD_SLEEP_CONST
        // we can't know next t1 and then can use (t0 + d1) == WORLD_SLEEP_CONST requirement
        // d1 = WORLD_SLEEP_CONST - t0 = WORLD_SLEEP_CONST - (D0 - d0) = WORLD_SLEEP_CONST + d0 - D0
        if (diff <= BG_SLEEP_CONST+prevSleepTime)
        {
            prevSleepTime = BG_SLEEP_CONST+prevSleepTime-diff;
            SkyNet::Threading::Thread::Sleep(prevSleepTime);
        }
        else
            prevSleepTime = 0;


    }

    sSocketMgr->StopNetwork();

}

void RoomUpdateRunnable::AddSession( GameSession* session){
	m_mapMgr.AddSession(session);
}

void RoomUpdateRunnable::AddRoom(Room* bg){
	sLog->outDebug("RoomUpdateRunnable::AddRoom pointer=%lu", (uint64)bg);
	m_mapMgr.AddMap(bg);
	bg->setUpdater(this);
	bg->OnCreate();
	maps++;
}

uint16 RoomUpdateRunnable::getMaps() const
{
    return maps;
}

void RoomUpdateRunnable::setMaps(uint16 maps)
{
    this->maps = maps;
}

uint16 RoomUpdateRunnable::getLoad() const
{
    return m_load;
}

void RoomUpdateRunnable::setLoad(uint16 load)
{
    m_load = load;
}

bool RoomUpdateRunnable::isCrashed() const
{
    return m_crashed;
}

void RoomUpdateRunnable::setCrashed(bool m_crashed)
{
    this->m_crashed = m_crashed;
}
