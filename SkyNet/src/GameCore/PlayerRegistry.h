#pragma once
#ifndef _PLAYERREGISTRY_H
#define _PLAYERREGISTRY_H

#include "GameCore/BasePlayer.h"

using namespace SkyNet::Core;
namespace SkyNet{
	namespace Core{
		class BasePlayer;
	}
}
using namespace SkyNet::Core;
class PlayerRegistry
{
public:
	PlayerRegistry(void);
	virtual ~PlayerRegistry(void);

	void RegisterPlayer(BasePlayer* plr);
	void RemovePlayer(BasePlayer* plr);

	BasePlayer* FindPlayer(uint32 id);

private:
	BasePlayerMap m_players;
};

#define sPlayerRegistry ACE_Singleton<PlayerRegistry, ACE_Thread_Mutex>::instance()

#endif
