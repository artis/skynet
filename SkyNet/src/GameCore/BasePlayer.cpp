/*
 * BasePlayer.cpp
 *
 *  Created on: Oct 12, 2012
 *      Author: dimitar
 */

#include "BasePlayer.h"
#include "Connection/GameSession.h"


SkyNet::Core::BasePlayer::BasePlayer(GameSession* session): m_session(session),SkyNet_Object(),YieldSubject(){
	sLog->outDebug("BasePlayer::BasePlayer %p",this);
	is_player = true;
	m_authed = false;
	m_roomJoin = NULL;
	if(session)
		m_session->AddReference();
}

SkyNet::Core::BasePlayer::~BasePlayer() {
	sLog->outDebug("BasePlayer::~BasePlayer");
	if(m_session)
		m_session->RemoveReference();
}

bool SkyNet::Core::BasePlayer::IsSendQueueEmpty(){ return m_session->IsSendQueueEmpty(); }
void SkyNet::Core::BasePlayer::SendPacket(ComPacket& data){
	GetSession()->SendPacket(data);
}

void SkyNet::Core::BasePlayer::SendFile(SkyNet_File& data)
{
	vector<ComPacket*> packets = data.SplitIntoPackets();
	if(packets.size() != data.GetNumPackets()+1){
		sLog->outDebug("We are sending wrong packets count!!! Sending %d instead of %d",packets.size(), data.GetNumPackets()+1);
		int b;
		cin >> b;
	}
	else
		sLog->outDebug("Sending right packet count");
	for(int i = 0; i < packets.size(); i++){
		this->SendPacket(*packets[i]);
		delete packets[i];
	}
}
bool SkyNet::Core::BasePlayer::_Update(uint32 diff){
	if(!GetSession()->Update(diff)){
		return false;
	}
	ModuleMgr::Update(diff);
	Update(diff);
	return true;
}

void SkyNet::Core::BasePlayer::_ProcessCallbacks()
{
	
	if(m_loginCallback.ready())
	{
		//sLog->outDebug("BasePlayer::ProcessCallbacks: Login query is ready");
		PreparedQueryResult result;
		m_loginCallback.get(result);
		HandleAuthentication(result);
		m_loginCallback.cancel();
	}

}