#include "SkyNet_Object.h"
#include "Exceptions/SkyNetException.h"

SkyNet_Object::SkyNet_Object(void)
{
	sLog->outDebug("SkyNet_Object::SkyNet_Object %p",this);
	m_initialized = false;
	m_guid = 0;
}


SkyNet_Object::~SkyNet_Object(void)
{
}

uint64 SkyNet_Object::GetGUID() const
{
	sLog->outDebug("SkyNet_Object::GetGUID %p",this);
	if(!m_initialized)
		throw SkyNetException("Attempted to get non-initialized Object GUID.", "Operation aborted.");
	return m_guid;
}
void SkyNet_Object::_SetGUID(uint64 guid)
{
	sLog->outDebug("SkyNet_Object::_SetGUID %p",this);
	if(m_initialized)
		throw SkyNetException("Attempted to set GUID after it was already set.", "Operation aborted.");

	m_initialized = true;
	m_guid = guid;
}