#pragma once
#ifndef SKYNET_OBJECT
#define SKYNET_OBJECT
#include "common.h"
#include "defines.h"

#define MAKE_NEW_GUID(S, T, ST,  L)   uint64( uint64(L) | ( uint64(ST) << 40 ) | ( uint64(T) << 48 ) | uint64(S) << 56)

#define GUID_SERVER_PART(x)             (uint32)((uint64(x) >> 56) & 0x000000FF)
#define GUID_TYPE_PART(x)               (uint32)((uint64(x) >> 48) & 0x000000FF)
#define GUID_STORAGE_PART(x)    (uint32)((uint64(x) >> 40) & 0x0000FFFF)
#define GUID_LOW_PART(x)                (uint32)(uint64(x) & UI64LIT(0x00000000FFFFFFFF))

enum ObjectType{
	OBJECT_TYPE_PLAYER	= 128,
	OBJECT_TYPE_ROOM	= 129,
};
class SkyNet_Object
{
public:
	SkyNet_Object(void);
	~SkyNet_Object(void);

	/*! Get the object's Globally Unique ID (GUID) */
	uint64 GetGUID() const;
	void _SetGUID(uint64 guid);
private:
	uint64 m_guid;
	bool m_initialized;
};

#endif