#include "RoomCreationCallback.h"
#include "Connection/GameSession.h"

RoomCreationCallback::RoomCreationCallback(GameSession* session): m_session(session)
{
	sLog->outDebug("RoomCreationCallback::RoomCreationCallback");
	if(session)
		m_session->AddReference();
};

RoomCreationCallback::~RoomCreationCallback()
{
	sLog->outDebug("RoomCreationCallback::~RoomCreationCallback");
	if(m_session)
		m_session->RemoveReference();
}

void RoomCreationCallback::update(const ACE_Future< uint64 >& future)
{
	if(!m_session)
		return;

	uint64 roomID;
	future.get(roomID);
	m_session->RoomCreateCallback(roomID);
	//sLog->outDebug("Room creation callback called.");

};
