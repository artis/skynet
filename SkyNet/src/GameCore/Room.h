/*
 * Map.h
 *
 *  Created on: Oct 28, 2011
 *      Author: dimitar
 */

#ifndef MAP_H_
#define MAP_H_

#include "defines.h"
#include "common.h"
#include "BasePlayer.h"
#include "Timer.h"
#include <set>
#include <vector>
using namespace SkyNet::Threading;
enum MapTypes{
	NORMAL			= 0x01,
	BATTLEGROUND	= 0x02,
};
namespace SkyNet{
	namespace Threading{
		class RoomUpdateRunnable;
	}
}
typedef SkyNet::Threading::LockedQueue<BasePlayer*, ACE_Thread_Mutex> BasePlayerQueue;
class Room: public Module {
public:
	Room();
	virtual ~Room();
	void Initialize_();
	virtual bool Update(uint32 diff);

	bool Update_(uint32 diff);
	RoomUpdateRunnable *getUpdater() const;
    void setUpdater(RoomUpdateRunnable *updater);
	virtual void OnCreate(){};
	virtual void OnClose(){};
	virtual void OnPlayerJoinRoom(BasePlayer* plr) {};
	uint64 GetGUID(){ return m_guid; }
	virtual Room* Clone() = 0;
	uint64 getId() const {
		return m_id;
	}

	void setId(uint64 id) {
		//sLog->outDebug("Room::setId(%d)",id);
		m_id = id;
	}
	void AddPlayer(BasePlayer* plr);
	void AddPlayer_(BasePlayer* plr);
	uint8 GetType(){ return m_type; }
	void SetType(uint8 type){ m_type = type;}
	uint8 GetGroup(){ return m_group; }
	void SetGroup(uint8 group){ m_group = group;}

	vector<BasePlayer*> GetPlayers()
	{
		vector<BasePlayer*> result;
		for(BasePlayerMap::iterator it = m_players.begin(); it != m_players.end(); it++)
		{
			result.push_back(it->second);
		}
		return result;
	}
private:
	uint64 m_guid;
	RoomUpdateRunnable* updater;
	uint64 m_id;
	uint8 m_type;
	uint8 m_group;
	BasePlayerMap m_players;
	BasePlayerQueue* addPlayerQueue;

};

class RoomTemplateHolder{
public:
	RoomTemplateHolder(): m_template(NULL), m_group(0){

	}
	RoomTemplateHolder(Room* instance, uint8 group): m_template(instance), m_group(group){

	}
	~RoomTemplateHolder(){
        delete m_template;
	}

	uint8 getGroup() const {
		return m_group;
	}

	void setGroup(uint8 group) {
		m_group = group;
	}

	Room* getTemplate() {
		return m_template;
	}



private:
	Room* m_template;
	uint8 m_group;

};
typedef UNORDERED_MAP<uint64, Room*> RoomMap;
typedef vector<Room*> RoomVector;
typedef vector<Room*> RoomArray;
#endif /* MAP_H_ */
