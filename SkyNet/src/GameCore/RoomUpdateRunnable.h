/*
 * RoomUpdateRunnable.h
 *
 *  Created on: Oct 8, 2012
 *      Author: dimitar
 */

#ifndef ROOMUPDATERUNNABLE_H_
#define ROOMUPDATERUNNABLE_H_
#include "RoomMgr.h"
#include "Threading/ThreadSystem.h"

#define BG_SLEEP_CONST 50
namespace SkyNet{

	namespace Threading{
		class RoomUpdateRunnable: public SkyNet::Threading::Runnable {
		public:
			RoomUpdateRunnable();
			virtual ~RoomUpdateRunnable();

		public:
			void run();
			void AddRoom(Room* bg);
			void AddSession(GameSession* session);
			uint16 getMaps() const;
			void setMaps(uint16 maps);
			uint16 getLoad() const;
			void setLoad(uint16 load);
			bool isCrashed() const;
			void setCrashed(bool m_crashed);
			uint32 getCounter(){ return m_LoopCounter;}
			SessionMap* getSessions(){ return m_mapMgr.getSessions();}
			RoomMap* getInstances(){ return m_mapMgr.getInstances();}
			private:
				bool m_crashed;
				volatile uint32 m_LoopCounter;
				RoomMgr m_mapMgr;
				uint16 m_load;
				uint16 maps;
				SkyNet::Threading::LockedQueue<Room*, ACE_Recursive_Thread_Mutex> addMapQueue;

		};
	}
}
#endif /* ROOMUPDATERUNNABLE_H_ */
