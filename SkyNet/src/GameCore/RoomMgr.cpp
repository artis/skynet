/*
 * RoomMgr.cpp
 *
 *  Created on: Nov 8, 2011
 *      Author: dimitar
 */
#include "Connection/GameSession.h"
#include "RoomMgr.h"
#include "GameCore/RoomRegistry.h"
RoomMgr::RoomMgr() {
	add_session_timer 	= 0;
	add_map_timer		= 0;
}

RoomMgr::~RoomMgr() {
}
void RoomMgr::AddMap(Room* m)
{
	//sLog->outDebug("RoomMgr::AddMap pointer=%lu",(uint64)m);
	instance_queue.add(m);
}

void RoomMgr::AddMap_(Room *m){
	//sLog->outDebug("RoomMgr::AddMap_ ID: %d pointer=%lu",m->getId(),(uint64)m);
	instances[m->getId()] = m;
	return;

}
void RoomMgr::AddSession(GameSession* s)
{
	//sLog->outDebug("RoomMgr::AddSession");
	//if(!session->GetPlayer()){
		//sLog->outDebug("RoomMgr::AddSession No player aborting");
	//	return;
	//}
	addSessQueue.add(s);
}

void RoomMgr::AddSession_(GameSession *s){
	//sLog->outDebug("RoomMgr::AddSession_");
	m_sessions[s->GetPlayer()->GetID()] = s;

	return;

}

void RoomMgr::Update(uint32 diff){
	///- Add new sessions

	AddNewMaps();

	AddNewSessions();

	//sLog->outDebug("Updating %d rooms",instances.size());
    for (RoomMap::iterator itr = instances.begin(), next; itr != instances.end(); itr = next)
    {
        next = itr;
        ++next;
        Room* map = itr->second;
		//sLog->outDebug("updating %lu", map);

        if ((!map->Update_(diff)))                      // As interval = 0
        {
            //sLog->outDebug("!!! Deleting map !!!");

            instances.erase(itr);
            delete itr->second;
        }
    }
};

bool RoomMgr::AddNewMaps(){
	Room* map;
	while (!instance_queue.empty()){
        instance_queue.next(map);
        AddMap_ (map);
	}


	return true;
}

bool RoomMgr::AddNewSessions(){
	return true;
}

