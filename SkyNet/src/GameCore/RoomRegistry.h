/*
 * RoomRegistry.h
 *
 *  Created on: Oct 20, 2012
 *      Author: dimitar
 */

#ifndef ROOMREGISTRY_H_
#define ROOMREGISTRY_H_
#include "common.h"
#include "defines.h"
#include <vector>
#include "Room.h"
#include"Connection/GameSession.h"
#include <list>
#include "GameCore/GameCore.h"

class AsyncRoomRequest;
class RoomRegistry {
public:
	RoomRegistry();
	virtual ~RoomRegistry();
	uint64 RegisterRoom(Room* instance, RoomCreationCallback* callback);

	void _RegisterRoom(Room* instance, uint64 id){ m_rooms[id] = instance; };

	void ProcessCallbacks();
	Room* GetRoom(uint64 id);
	RoomVector GetRooms();
	RoomMap m_rooms;
	QueryResultFuture m_callback;
	AsyncRoomRequest* m_request;
	std::list<AsyncRoomRequest*> m_requests;
};
#define sRoomRegistry ACE_Singleton<RoomRegistry, ACE_Null_Mutex>::instance()




#endif /* ROOMREGISTRY_H_ */
