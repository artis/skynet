/*
 * GameCore.h
 *
 *  Created on: Oct 12, 2011
 *      Author: dimitar
 */

#ifndef GAMECORE_H_
#define GAMECORE_H_

#include "common.h"
#include "defines.h"
#include "BasePlayer.h"
#include "ComPacket.h"
#include "Timer.h"
#include "Room.h"
#include <algorithm>

enum LobbyTimers
{
    LUPDATE_GAMESERVERS,
    LUPDATE_COUNT
};


namespace SkyNet{
	namespace Threading{
		class RoomUpdateRunnable;

	}
}
using namespace SkyNet::Threading;
class RoomCreationCallback;
class GameCore
{
    public:
        static volatile uint32 m_LoopCounter;

        GameCore();
        virtual ~GameCore();

        //GameSession* FindSession(uint32 id) const;
        void AddPlayer(BasePlayer *p);
        void RemovePlayer(BasePlayer * p);

        void SendGlobalMessage(ComPacket *packet);
        //bool RemoveSession(uint32 id);
        /// Get the number of current active sessions
        void UpdateMaxSessionCounters();
        /// Get number of players
        inline uint32 GetPlayerCount() const { return m_PlayerCount; }
        inline uint32 GetMaxPlayerCount() const { return m_MaxPlayerCount; }
        /// Increase/Decrease number of players
        inline void IncreasePlayerCount()
        {
            m_PlayerCount++;
			m_MaxPlayerCount = max(m_MaxPlayerCount, m_PlayerCount);
        }
        inline void DecreasePlayerCount() { m_PlayerCount--; }


        bool IsClosed() const;
        void SetClosed(bool val);
     /// Active session server limit
        void SetPlayerAmountLimit(uint32 limit) { m_playerLimit = limit; }
        uint32 GetPlayerAmountLimit() const { return m_playerLimit; }




        /// When server started?
        time_t const& GetStartTime() const { return m_startTime; }
        /// What time is it?
        time_t const& GetGameTime() const { return m_gameTime; }
        /// Uptime (in secs)
        uint32 GetUptime() const { return uint32(m_gameTime - m_startTime); }
        /// Update time
        uint32 GetUpdateTime() const { return m_updateTime; }

        void SetInitialWorldSettings();
        void LoadConfigSettings(bool reload = false);

        /// Are we in the middle of a shutdown?
        bool IsShutdowning() const { return m_ShutdownTimer > 0; }
        void ShutdownServ(uint32 time, uint32 options, uint8 exitcode);
        void ShutdownCancel();

        static uint8 GetExitCode() { return m_ExitCode; }
        static void StopNow(uint8 exitcode) { m_stopEvent = true; m_ExitCode = exitcode; }
        static bool IsStopped() { return m_stopEvent; }

        virtual void Update(uint32 diff);

        void UpdateSessions(uint32 diff);

        void KickAll();

        void ForceGameEventUpdate();

        void RecordTimeDiff(const char * text, ...);

        void UpdateAreaDependentAuras();

        bool GetEventKill() { return isEventKillStart; }

        void RegisterRoomGroup(uint8 id, uint8 workers);

        void RegisterRoomScript(const uint16 type, RoomTemplateHolder* roomTemplate);

        uint64 CreateRoom(const uint16 type, RoomCreationCallback* callback);
		uint64 CreateRoom(const uint16 type);
		void AssignRoomToWorker(Room* room);
        Room* FindRoom(uint64 id);
        RoomVector GetRooms();
		int GetPlayerID(){
			m_playerID++;
			return m_playerID;
		}
        bool isEventKillStart;
    protected:
        void _UpdateGameTime();

    private:
        static volatile bool m_stopEvent;

        static uint8 m_ExitCode;
        uint32 m_ShutdownTimer;
        uint32 m_ShutdownMask;

        bool m_isClosed;

        time_t m_startTime;
        time_t m_gameTime;

        time_t mail_timer;
        time_t mail_timer_expires;
        uint32 m_updateTime, m_updateTimeSum;
        uint32 m_updateTimeCount;
        uint32 m_currentTime;


        BasePlayerMap m_players;
        typedef UNORDERED_MAP<uint32, time_t> DisconnectMap;
        uint32 m_maxActiveSessionCount;
        uint32 m_maxQueuedSessionCount;
        uint32 m_PlayerCount;
        uint32 m_MaxPlayerCount;
        uint32 m_playerLimit;
        std::string m_newCharString;

        uint32 m_userLimit;

        IntervalTimer m_timers[1];
        //sessions that are added async
        void AddPlayer_(BasePlayer* p);
        SkyNet::Threading::LockedQueue<BasePlayer*, ACE_Thread_Mutex> addPlayerQueue;
    protected:
        SkyNet::Threading::LockedQueue<BasePlayer*, ACE_Thread_Mutex> remove_queue;
        std::vector<vector<RoomUpdateRunnable*> > m_workers;
        std::vector<RoomTemplateHolder*> m_roomTemplates;
        int m_roomIds;
		int m_playerID;
};

// Future support for multiple lobby servers
extern uint32 lobbyID;



#define sGameCore ACE_Singleton<GameCore, ACE_Null_Mutex>::instance()
#endif /* GAMECORE_H_ */
