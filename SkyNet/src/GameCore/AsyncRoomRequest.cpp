#include "AsyncRoomRequest.h"
#include "RoomMgr.h"
#include "RoomRegistry.h"
#include "GameCore/RoomCreationCallback.h"

AsyncRoomRequest::AsyncRoomRequest(AsyncRoomRequestType type, Room* room, int roomType, int roomCapacity, RoomCreationCallback* callback):
   m_room(room),m_type(type), m_roomType(roomType), m_roomCapacity(roomCapacity), m_callback(callback)
{
	m_queryResultCallback = SkyNetDatabase.AsyncPQuery("SELECT create_room('%d','%d')", room->GetType(), roomCapacity );
	m_returnCallback.attach(callback);
};

void AsyncRoomRequest::Process(){
	switch(m_type)
	{
		case ASYNC_ROOM_REQUEST_CREATE:
			ProcessRoomCreation();
			break;
	}
}

bool AsyncRoomRequest::Ready()
{ 
	return m_queryResultCallback.ready(); 
};

void AsyncRoomRequest::ProcessRoomCreation()
{
	QueryResult result;
	m_queryResultCallback.get(result);
	if(!result)
		return;
	Field* fields = result->Fetch();
	uint64 new_id = fields[0].GetInt32();
	m_room->setId(new_id);
	sRoomRegistry->_RegisterRoom(m_room, new_id);
	m_returnCallback.set(new_id);
	//sLog->outDebug("Room %d created successfully.", m_room->getId());
	
	m_queryResultCallback.cancel();
	sGameCore->AssignRoomToWorker(m_room);
	delete m_callback;
}
