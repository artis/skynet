#include "PlayerRegistry.h"
#include "GameCore/BasePlayer.h"

PlayerRegistry::PlayerRegistry(void)
{
}


PlayerRegistry::~PlayerRegistry(void)
{
}

void PlayerRegistry::RegisterPlayer(BasePlayer* plr)
{
	m_players[plr->GetID()] = plr;
}

void PlayerRegistry::RemovePlayer(BasePlayer* plr)
{
	BasePlayerMap::iterator it = m_players.find(plr->GetID());
	if(it != m_players.end())
		m_players.erase(it);
}

BasePlayer* PlayerRegistry::FindPlayer(uint32 id)
{
	BasePlayerMap::iterator it = m_players.find(id);
	if(it != m_players.end())
		return it->second;
	else
		return NULL;
}
