/*
 * RoomRegistry.cpp
 *
 *  Created on: Oct 20, 2012
 *      Author: dimitar
 */

#include "RoomRegistry.h"
#include "Room.h"
#include "Database/DatabaseEnv.h"
#include "Connection/GameSession.h"
#include "AsyncRoomRequest.h"

RoomRegistry::RoomRegistry() {

}

RoomRegistry::~RoomRegistry() {
}

uint64 RoomRegistry::RegisterRoom(Room* instance, RoomCreationCallback* callback){

	// TODO: Implement Read/Write mutexes/spinlock to protect this.
	m_requests.push_back(new AsyncRoomRequest(ASYNC_ROOM_REQUEST_CREATE, instance, instance->GetType(),15, callback));


	return 0;
}

void RoomRegistry::ProcessCallbacks()
{
	for(std::list<AsyncRoomRequest*>::iterator itr = m_requests.begin(), next; itr != m_requests.end(); itr = next)
	{
		next = itr;
        ++next;
		AsyncRoomRequest* request = *itr;
		if(request->Ready())
		{
			sLog->outDebug("Processing Callback...");
			request->Process();
			sLog->outDebug("Deleting Callback...");
			delete request;
			sLog->outDebug("Deleted Callback...");
			m_requests.erase(itr);
		}
	}
}
Room* RoomRegistry::GetRoom(uint64 id){
	//sLog->outDebug("RoomRegistry::GetRoom(%d)",id);
	RoomMap::iterator i = m_rooms.find(id);
	if(i == m_rooms.end()){
		sLog->outDebug("Room %d not found", id);
		return NULL;
	}
	//sLog->outDebug("Returning room pointer %lu", (uint64) i->second);
	return i->second;
}

RoomVector RoomRegistry::GetRooms(){
	RoomVector result;
	for (RoomMap::iterator i = m_rooms.begin(); i != m_rooms.end(); i++){
		result.push_back(i->second);
	}
	return result;
}
