#ifndef __SESSIONUPDATERRUNNABLE_H
#define __SESSIONUPDATERRUNNABLE_H

#include "Threading/Threading.h"

/// Main Game thread. Updates everything ( sessions, chat channels etc.)
namespace SkyNet{
	namespace Threading{
		class MasterRunnable : public SkyNet::Threading::Runnable
		{
			public:
				void run();
		};
	}
}
#endif
