/*
 * Map.cpp
 *
 *  Created on: Oct 28, 2011
 *      Author: dimitar
 */

#include "Room.h"
#include "Connection/GameSession.h"
Room::Room() {
	m_id = 0;
}

Room::~Room() {
	sLog->outDebug("Deleting room...");
}

bool Room::Update(uint32 diff){
	return true;
}

bool Room::Update_(uint32 diff){
	BasePlayer* plr;
	//sLog->outDebug("Updating1 this=%lu",(uint64)this);
	while (!addPlayerQueue->empty()){
		//sLog->outDebug("addSessQueue is not empty");
		addPlayerQueue->next(plr);
		AddPlayer_(plr);

	}
	//sLog->outDebug("Updating2 this=%lu",(uint64)this);
	BasePlayer* player;
	//sLog->outDebug("Updating %d players in room %d",m_players.size(), this->getId());
    for (BasePlayerMap::iterator itr = m_players.begin(), next; itr != m_players.end(); itr = next)
    {
        next = itr;
        ++next;
        player = itr->second;
        assert(player != NULL);

        ///- and remove not active sessions from the list.
        if (!player->_Update(diff))                      // As interval = 0
        {
        	//sLog->outDebug("Room::_Update: Deleting Player");
        	m_players.erase(itr);
			//player->GetSession()->RemoveReference("Room");
            //delete player->GetSession();
			delete player;

        }

    }
	return Update(diff);
}
RoomUpdateRunnable *Room::getUpdater() const
{
    return updater;
}

void Room::setUpdater(RoomUpdateRunnable *updater)
{
    this->updater = updater;
}

void Room::AddPlayer(BasePlayer* plr)
{
	addPlayerQueue->add(plr);



}
void Room::AddPlayer_(BasePlayer* plr)
{
	m_players[plr->GetID()] = plr;
	OnPlayerJoinRoom(plr);
	sLog->outDebug(RoomSystem, "Room::AddPlayer: Player %d added successfully to room ID %lu ptr = %lu", plr->GetID(), getId(), (uint64)this);
}

void Room::Initialize_()
{
	addPlayerQueue = new BasePlayerQueue();
	
}