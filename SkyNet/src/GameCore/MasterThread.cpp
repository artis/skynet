#include "MasterThread.h"
#include "Connection/GameSocketMgr.h"
#define MASTER_SLEEP_CONST 50



using namespace SkyNet::Threading;
void MasterRunnable::run()
{

    uint32 realCurrTime = 0;
    uint32 realPrevTime = getMSTime();

    uint32 prevSleepTime = 0;                               // used for balanced full tick time length near LOBBY_SLEEP_CONST

    while (!sGameCore->IsStopped())
    {
        ++GameCore::m_LoopCounter;
        realCurrTime = getMSTime();

        uint32 diff = getMSTimeDiff(realPrevTime,realCurrTime);

        sGameCore->Update( diff );
        if(diff > 55)
            sLog->outDebug("Master diff: %d ms", diff);
        realPrevTime = realCurrTime;

        if (diff <= MASTER_SLEEP_CONST+prevSleepTime)
        {
            prevSleepTime = MASTER_SLEEP_CONST+prevSleepTime-diff;
            SkyNet::Threading::Thread::Sleep(prevSleepTime);
        }
        else
            prevSleepTime = 0;


    }
    sLog->outNormal("Master Thread exiting...");
    sSocketMgr->StopNetwork();

}
