#ifndef _ROOMCREATIONCALLBACK_H_
#define _ROOMCREATIONCALLBACK_H_
#include "common.h"
#include "ace/Future.h"

class GameSession;
class RoomCreationCallback: public ACE_Future_Observer<uint64>
{
public:
	RoomCreationCallback(GameSession* session);
	virtual ~RoomCreationCallback();
	void update(const ACE_Future< uint64 >& future);
	GameSession* m_session;
};

#endif