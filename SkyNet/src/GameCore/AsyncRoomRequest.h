#ifndef _ASYNCROOMREQUEST_H
#define _ASYNCROOMREQUEST_H
#include "common.h"
#include "Database/DatabaseEnv.h"

enum AsyncRoomRequestType
{
	ASYNC_ROOM_REQUEST_CREATE	= 1,
};
class Room;
class RoomCreationCallback;
class AsyncRoomRequest
{
public:
	AsyncRoomRequest(AsyncRoomRequestType type, Room* room, int roomType, int roomCapacity, RoomCreationCallback* callback);
	
	  bool Ready();
	  void Process();

private:
	void ProcessRoomCreation();
	Room* m_room;
	AsyncRoomRequestType m_type;
	QueryResultFuture m_queryResultCallback;
	ACE_Future<uint64> m_returnCallback;
	int m_roomType;
	int m_roomCapacity;
	RoomCreationCallback* m_callback;
};
#endif
