/*
 * RoomMgr.h
 *
 *  Created on: Nov 8, 2011
 *      Author: dimitar
 */

#ifndef ROOMMGR_H_
#define ROOMMGR_H_
#include "Threading/LockedQueue.h"
#include <ace/Task.h>
#include "common.h"
#include "defines.h"
#include "Room.h"
#include "Connection/GameSession.h"

class GameSession;
class RoomMgr {
public:
	RoomMgr();
	virtual ~RoomMgr();
	void AddMap(Room* m);
	void AddSession(GameSession* m);
	void Update(uint32 diff);
	SessionMap* getSessions(){ return &m_sessions;}
	RoomMap* getInstances(){ return &instances;}
private:
	bool AddNewMaps();
	bool AddNewSessions();
	void AddMap_(Room* m);
	void AddSession_(GameSession* s);
	RoomMap instances;
	SessionMap m_sessions;

	SkyNet::Threading::LockedQueue<Room*, ACE_Thread_Mutex> instance_queue;
	SkyNet::Threading::LockedQueue<GameSession*, ACE_Thread_Mutex> addSessQueue;

    uint64 add_session_timer;
    uint64 add_map_timer;
    uint64 queue_sleep;
};

//#define sRoomMgr ACE_Singleton<RoomMgr, ACE_Null_Mutex>::instance()
#endif /* ROOMMGR_H_ */
