/*
 * GameCore.cpp
 *
 *  Created on: Oct 12, 2011
 *      Author: dimitar
 */

#include "GameCore.h"
#include "Connection/GameSession.h"
#include "RoomUpdateRunnable.h"
#include "Threading/ThreadSystem.h"
#include "Exceptions/SkyNetException.h"
#include "RoomRegistry.h"
#include "ModuleSystem/ModuleSystem.h"
#include "ExternalProcedures/ExternalProcedureMgr.h"
#include "GameCore/RoomCreationCallback.h"

volatile uint32 GameCore::m_LoopCounter = 0;
volatile bool GameCore::m_stopEvent = false;
GameCore::GameCore(){
	m_roomIds = 0;
	m_playerID = 0;
};
GameCore::~GameCore(){
    for(int i = 0; i < m_roomTemplates.size(); i++)
        delete m_roomTemplates[i];
};
void GameCore::UpdateSessions(uint32 diff)
{
    ///- Add new sessions
    BasePlayer* plr;
    //sLog->outDebug("Checking for new sessions");


    while (!addPlayerQueue.empty()){
    	//sLog->outDebug("addSessQueue is not empty");
    	addPlayerQueue.next(plr);
        AddPlayer_ (plr);

    }
   // sLog->outDebug("addSessQueue is empty");

    while (!remove_queue.empty()){
    	remove_queue.next(plr);
		m_players.erase(plr->GetID());
		if(plr->_isScheduledForMove()){
			Room* room = plr->_GetScheduledRoom();
			room->AddPlayer(plr);
		}

    	//plr->GetSession()->OnRemoveFromUpdater();
    }
    	//sLog->outDebug("No session to remove");

    //sLog->outDebug("GameCore::UpdateSessions");
    ///- Now update current sessions
    //sLog->outDebug("Updating %d players.", m_players.size());
    for (BasePlayerMap::iterator itr = m_players.begin(), next; itr != m_players.end(); itr = next)
    {
        next = itr;
        ++next;
        BasePlayer* player = itr->second;
        ///- and remove not active sessions from the list.
        if (!player->_Update(diff))                      // As interval = 0
        {
        	sLog->outDebug("GameCore::UpdateSessions: Deleting Player");
        	m_players.erase(itr);
			//player->GetSession()->RemoveReference("GameCore");
            delete player;

        }

    }
}
void GameCore::AddPlayer(BasePlayer* plr)
{
    //sModuleMgr->CopyModulesToPlayer(plr);
	//sLog->outDebug("GameCore::AddPlayer");

    addPlayerQueue.add(plr);
}
void GameCore::RemovePlayer(BasePlayer * plr){
    remove_queue.add(plr);
}

void GameCore::AddPlayer_(BasePlayer *plr){
	//sLog->outDebug("GameCore::AddPlayer_");
	m_players[plr->GetID()] = plr;
	//plr->GetSession()->AddReference();
	return;

}
void GameCore::Update(uint32 diff)
{
	sRoomRegistry->ProcessCallbacks();
	UpdateSessions(diff);
	sModuleMgr->Update(diff);
	sExternalProcedureMgr->Update(diff);
}

/*GameSession* GameCore::FindSession(uint32 id) const
{
    SessionMap::const_iterator itr = m_sessions.find(id);

    if (itr != m_sessions.end())
        return itr->second;                                 // also can return NULL for kicked session
    else
        return NULL;
}*/

void GameCore::SendGlobalMessage(ComPacket *packet)
{
    BasePlayerMap::const_iterator itr;
    for (itr = m_players.begin(); itr != m_players.end(); ++itr)
    {
    	itr->second->GetSession()->SendPacket(*packet);
    }
}
void GameCore::RegisterRoomGroup(uint8 id, uint8 workers){
	if(m_workers.size() <= id)
		m_workers.resize(id+1);
	if(m_workers[id].size() < workers)
		m_workers[id].resize(workers);

	for(int i = 0; i < workers; i++){
		RoomUpdateRunnable* runnable = new RoomUpdateRunnable();
		SkyNet::Threading::Thread thread(runnable);
		thread.setPriority(SkyNet::Threading::Highest);
		m_workers[id][i] = runnable;
	}
}

void GameCore::RegisterRoomScript(const uint16 type, RoomTemplateHolder* roomTemplate){
	uint8 group = roomTemplate->getGroup();
	//sLog->outDebug("GameCore::RegisterRoomScript: Registering Room Script type %d in group %d", type, group);
	if(m_workers.size() <= group || m_workers[group].size() == 0)
		throw SkyNetException("Trying to register Room type %d to non-existing group %d.", "Room Script registration skipped.",type, group);

	if(m_roomTemplates.size() <= type)
		m_roomTemplates.resize(type+1);


	m_roomTemplates[type] = roomTemplate;
	//sLog->outDebug("GameCore::RegisterRoomScript: m_roomTemplates[%d].getGroup() = %d",type,m_roomTemplates[type]->getGroup());
}
uint64 GameCore::CreateRoom(const uint16 type)
{
	RoomCreationCallback* dummy = new RoomCreationCallback(NULL);
	return CreateRoom(type,dummy);

}
void GameCore::AssignRoomToWorker(Room* room)
{
	uint8 group = room->GetGroup();
	int min = 0;
	for(uint i = 0; i < m_workers[group].size(); i++){
		if(m_workers[group][i]->getMaps() < m_workers[group][min]->getMaps())
			min = i;
	}
	m_workers[group][min]->AddRoom(room);
    //sLog->outDebug("Adding room to group %d thread %d", group, min);
}
uint64 GameCore::CreateRoom(const uint16 type, RoomCreationCallback* callback)
{

	//sLog->outDebug("GameCore::CreateRoom(%d)",type);
	if(m_roomTemplates.size() <= type)
		throw SkyNetException("Attempted to create room of non-existing (not registered) Room Type %d","Room creation skipped.",type);

	RoomTemplateHolder* holder = m_roomTemplates[type];
	Room* new_room = holder->getTemplate()->Clone();
	uint8 group = holder->getGroup();
	//sLog->outDebug("Created room. Pointer: %lu", (uint64)new_room);
	new_room->Initialize_();
	new_room->SetType(type);
	new_room->SetGroup(group);
	
	//sLog->outDebug("GameCore::CreateRoom: m_roomTemplates[%d].getGroup() = %d",type,m_roomTemplates[type]->getGroup());
	uint8 min = 0;
	//sLog->outDebug("Workers in group %d: %d",group,m_workers[group].size());
	uint64 new_room_id = sRoomRegistry->RegisterRoom(new_room, callback);
	
	return new_room_id;
}
Room* GameCore::FindRoom(uint64 id){
	return sRoomRegistry->GetRoom(id);
}

RoomVector GameCore::GetRooms(){
	return sRoomRegistry->GetRooms();
}
