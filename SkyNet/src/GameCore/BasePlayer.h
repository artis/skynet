/*
 * BasePlayer.h
 *
 *  Created on: Oct 12, 2012
 *      Author: dimitar
 */

#ifndef BASE_PLAYER_H_
#define BASE_PLAYER_H_

#include "common.h"
#include "defines.h"
#include <vector>
#include "ModuleSystem/ModuleSystem.h"
#include "Database/DatabaseEnv.h"
#include "SkyNet_Object.h"

class Room;
class GameSession;
//class SkyNet::Networking::ComPacket;
namespace SkyNet{
    namespace ModuleSystem{
        class Module;
    }

}
using namespace SkyNet::ModuleSystem;

namespace SkyNet{
	namespace Core{
		//! \brief Base Player object. Developer's Player implementation class should inherit this class.  
		//! It provides a set of member function needed for a Player implementation.
		class BasePlayer: public ModuleMgr, public SkyNet_Object, public YieldSubject {
		public:
			BasePlayer(GameSession* session);
			virtual ~BasePlayer();
	
			/* \brief Returns the current Player object ID. */
			uint64 GetID() const { return m_id; };
			//! \brief Returns the Room the Player is currently inside.
			/*! @note Will return NULL if the Player is not currently in a Room.*/
			Room* GetRoom(){ return m_map; }

			//! \brief Returns the underlying GameSession object instance.
			GameSession* GetSession(){ return m_session; }
			//! \brief Checks wether the Player has been authenticated.
			//! @return True if the Player object has already passed authentication, otherwise False.
			bool isAuthed() { return m_authed; };
			void RegisterAuthCallback(PreparedQueryResultFuture query) { m_loginCallback = query; };
			/*! 
			 *  \brief Send a ComPacket to the client through the network.
			 */

			bool IsSendQueueEmpty();
			void SendPacket(ComPacket& data);

			void SendFile(SkyNet_File& data);
			/*! 
			 *  Set the current Player object ID.
			 *  @note GUID will automatically be generated.
			 *  @note Do not call this function twice.
			 */
			void SetID(uint32 id) {
				sLog->outDebug("BasePlayer::SetID %p",this);
				m_id = id;
				m_authed = true;
				this->_SetGUID(MAKE_NEW_GUID(0,OBJECT_TYPE_PLAYER,0,id));
			}

			virtual void HandleAuthentication(PreparedQueryResult& result){};
			/*! \brief Virtual function for saving the Player's progress.
			 *
			 *  The developer should implement a function that will save the current player's progress.
			 */
			virtual void Save() {};
			/*! \brief Virtual function for Player updates.
			 *
			 *  Will be invoked on every update cycle (default: 50ms).
			 * \param diff indicates the real time elapsed since last update in ms.
			 * @note This function will be called on equal intervals set in the SkyNet.conf file.
			 */
			virtual void Update(uint32 diff) {	};

			Room* _GetScheduledRoom(){ return m_roomJoin; }
			bool _isScheduledForMove(){ return m_roomJoin != NULL;};
			void _ProcessCallbacks();
			void _ResetRoomMoveSchedule(){ m_roomJoin = NULL; };
			void _ScheduleForMove(Room* room){ m_roomJoin = room; };
			void _SetRoom(Room* map){ m_map = map; }
			bool _Update(uint32 diff);
	
		
			void _SetSession(GameSession* session){ m_session = session; };
		private:
			Room* m_map;
			GameSession* m_session;
			uint32 m_id;
			vector<Module*> m_modules;
			bool m_authed;
			vector<vector<vector<ModuleCallBack*>* >* > m_callback;
			PreparedQueryResultFuture m_loginCallback;
			Room* m_roomJoin;
		};
	}
}
typedef UNORDERED_MAP<uint32, BasePlayer*>  BasePlayerMap;

#endif /* BASE_PLAYER_H_ */
