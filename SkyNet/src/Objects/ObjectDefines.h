/*
 * ObjectDefines.h
 *
 *  Created on: Jul 6, 2011
 *      Author: dimitar
 */

#ifndef OBJECTDEFINES_H_
#define OBJECTDEFINES_H_

#include "defines.h"

#define UI64LIT(N) ACE_UINT64_LITERAL(N)
// used for creating values for respawn for example


enum GuidType
{
    GUID_TYPE_ITEM			= 0x01,
    GUID_TYPE_PLAYER        = 0x02,
    GUID_TYPE_GAMEOBJECT    = 0x03,
    GUID_TYPE_NPC          	= 0x04,
    GUID_TYPE_PET           = 0x05,
    GUID_TYPE_CORPSE        = 0x06,
    GUID_TYPE_PARTY			= 0x07,
    GUID_TYPE_MAP			= 0x08
};

enum GuidPart{
	GUIDPART_GUID			= 0x00,
	GUIDPART_SERVER			= 0x01,
	GUIDPART_TYPE			= 0x02,
	GUIDPART_STORAGE		= 0x03,
	GUIDPART_LOW			= 0x04,
};

#define IS_EMPTY_GUID(Guid)          ( Guid == 0 )

#define IS_CREATURE_GUID(Guid)       ( GUID_TYPE_PART(Guid) == GUID_TYPE_NPC )
#define IS_PET_GUID(Guid)            ( GUID_TYPE_PART(Guid) == GUID_TYPE_PET )
#define IS_PLAYER_GUID(Guid)         ( GUID_TYPE_PART(Guid) == GUID_TYPE_PLAYER && Guid!=0 )
#define IS_NPC_GUID(Guid)            ( IS_CRE_OR_VEH_OR_PET_GUID(Guid) || IS_PLAYER_GUID(Guid) )
                                                            // special case for empty guid need check
#define IS_ITEM_GUID(Guid)           ( GUID_TYPE_PART(Guid) == GUID_TYPE_ITEM )
#define IS_GAMEOBJECT_GUID(Guid)     ( GUID_TYPE_PART(Guid) == GUID_TYPE_GAMEOBJECT )
#define IS_CORPSE_GUID(Guid)         ( GUID_TYPE_PART(Guid) == GUID_TYPE_CORPSE )
#define IS_MAP_GUID(Guid)         	 ( GUID_TYPE_PART(Guid) == GUID_TYPE_MAP )

#define MAKE_PAIR64(l, h)  uint64( uint32(l) | ( uint64(h) << 32 ) )
#define PAIR64_HIPART(x)   (uint32)((uint64(x) >> 32) & UI64LIT(0x00000000FFFFFFFF))
#define PAIR64_LOPART(x)   (uint32)(uint64(x)         & UI64LIT(0x00000000FFFFFFFF))

#define MAKE_PAIR32(l, h)  uint32( uint16(l) | ( uint32(h) << 16 ) )
#define PAIR32_HIPART(x)   (uint16)((uint32(x) >> 16) & 0x0000FFFF)
#define PAIR32_LOPART(x)   (uint16)(uint32(x)         & 0x0000FFFF)

/* Old GUID system.
 *
 * #define IS_CREATURE_GUID(Guid)       ( GUID_HIPART(Guid) == GUID_TYPE_NPC )
 * #define IS_PET_GUID(Guid)            ( GUID_HIPART(Guid) == GUID_TYPE_PET )
 * #define IS_PLAYER_GUID(Guid)         ( GUID_HIPART(Guid) == GUID_TYPE_PLAYER && Guid!=0 )
 * #define IS_NPC_GUID(Guid)            ( IS_CRE_OR_VEH_OR_PET_GUID(Guid) || IS_PLAYER_GUID(Guid) )
 *                                                           // special case for empty guid need check
 * #define IS_ITEM_GUID(Guid)           ( GUID_HIPART(Guid) == GUID_TYPE_ITEM )
 * #define IS_GAMEOBJECT_GUID(Guid)     ( GUID_HIPART(Guid) == GUID_TYPE_GAMEOBJECT )
 * #define IS_CORPSE_GUID(Guid)         ( GUID_HIPART(Guid) == GUID_TYPE_CORPSE )
 * #define IS_MAP_GUID(Guid)         	 ( GUID_HIPART(Guid) == GUID_TYPE_MAP )
 *
 *
 * // l - OBJECT_FIELD_GUID
 * // e - OBJECT_FIELD_ENTRY for GO (except GAMEOBJECT_TYPE_MO_TRANSPORT) and creatures or UNIT_FIELD_PETNUMBER for pets
 * // h - OBJECT_FIELD_GUID + 1
 * #define MAKE_NEW_GUID(l, e, h)   uint64( uint64(l) | ( uint64(e) << 32 ) | ( uint64(h) << 56 ) )
 *
 * #define GUID_HIPART(x) (uint32)((uint64(x) >> 56) & 0x000000FF)
 * #define GUID_ENPART(x) (uint32)((uint64(x) >> 32) & UI64LIT(0x0000000000FFFFFF))
 */

#define MAKE_NEW_GUID(S, T, ST,  L)   uint64( uint64(L) | ( uint64(ST) << 40 ) | ( uint64(T) << 48 ) | uint64(S) << 56)

#define GUID_SERVER_PART(x)		(uint32)((uint64(x) >> 56) & 0x000000FF)
#define GUID_TYPE_PART(x)		(uint32)((uint64(x) >> 48) & 0x000000FF)
#define GUID_STORAGE_PART(x)	(uint32)((uint64(x) >> 40) & 0x0000FFFF)
#define GUID_LOW_PART(x) 		(uint32)(uint64(x) & UI64LIT(0x00000000FFFFFFFF))



inline char const* GetLogNameForGuid(uint64 guid)
{
    switch(GUID_TYPE_PART(guid))
    {
        case GUID_TYPE_ITEM:         return "item";
        case GUID_TYPE_PLAYER:       return guid ? "player" : "none";
        case GUID_TYPE_GAMEOBJECT:   return "gameobject";
        case GUID_TYPE_NPC:         return "creature";
        case GUID_TYPE_PET:          return "pet";
        case GUID_TYPE_CORPSE:       return "corpse";
        default:
            return "<unknown>";
    }
}

#endif /* OBJECTDEFINES_H_ */
