#ifndef _EXTERNALPROCEDUREMGR_H_
#define _EXTERNALPROCEDUREMGR_H_
#include "common.h"
#include "Timer.h"
#include "ModuleSystem/Yieldable.h"
#include "DatabaseEnv.h"

using namespace SkyNet::ModuleSystem;
class ExternalProcedure: public Yieldable{

public:
	ExternalProcedure(int arg1, int arg2, int arg3, int arg4, int arg5);

	int GetArg1() const { return m_arg1; };
	int GetArg2() const { return m_arg2; };
	int GetArg3() const { return m_arg3; };
	int GetArg4() const { return m_arg4; };
	int GetArg5() const { return m_arg5; };

private:
	int m_arg1;
	int m_arg2;
	int m_arg3;
	int m_arg4;
	int m_arg5;

};
class ExternalProcedureMgr
{
public:
	ExternalProcedureMgr(void);
	virtual ~ExternalProcedureMgr(void);
	void Update(uint32 diff);

private:
	SimpleTimer m_timer;
	PreparedQueryResultFuture m_queryResultFuture;
	PreparedStatement* m_stmt;
	bool m_awaitingResult;
};

#define sExternalProcedureMgr ACE_Singleton<ExternalProcedureMgr, ACE_Null_Mutex>::instance()
#endif
