#include "ExternalProcedureMgr.h"
#include "ModuleSystem/ModuleMgr.h"

ExternalProcedure::ExternalProcedure(int arg1, int arg2, int arg3, int arg4, int arg5):
m_arg1(arg1),m_arg2(arg2),m_arg3(arg3),m_arg4(arg4),m_arg5(arg5)
{

}

ExternalProcedureMgr::ExternalProcedureMgr(void)
{
	m_timer.SetTime(6000);
}


ExternalProcedureMgr::~ExternalProcedureMgr(void)
{
}

void ExternalProcedureMgr::Update(uint32 diff)
{
	m_timer.Update(diff);
	if(m_timer.isPassed())
	{
		m_timer.Reset();

		PreparedStatement* stmt = SkyNetDatabase.GetPreparedStatement(GET_EXTERNAL_PROCEDURES);
		m_queryResultFuture = SkyNetDatabase.AsyncQuery(stmt);
	}
	

	if(m_queryResultFuture.ready())
	{
		sLog->outDebug(ExternalProcedures,"Retrieving external procedures...");
		int i = 1;
		int last = 0;
		PreparedQueryResult result;
        m_queryResultFuture.get(result);
		if(result)
		{
			do
			{
			
				Field* fields = result->Fetch();
				last = fields[0].GetUInt32();
				int type = fields[1].GetUInt32();
				ExternalProcedure* instance = new ExternalProcedure(fields[2].GetUInt32(),fields[3].GetUInt32(),fields[4].GetUInt32(),fields[5].GetUInt32(),fields[6].GetUInt32());
				Dispatch(EXTERNAL_PROCEDURE,type,instance,NULL);
				i++;

			}while(result->NextRow());

			SkyNetDatabase.PExecute("DELETE FROM `external_procedures` WHERE `id` <= '%d'",last);
		}
		m_queryResultFuture.cancel();
	}

}