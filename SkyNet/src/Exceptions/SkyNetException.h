/*
 * SkyNetException.h
 *
 *  Created on: Oct 19, 2012
 *      Author: dimitar
 */

#ifndef SKYNETEXCEPTION_H_
#define SKYNETEXCEPTION_H_
#include <stdarg.h>
#include <string>

class SkyNetException {
public:
	SkyNetException(const char * err, std::string resolution="N/A", ...);

	virtual ~SkyNetException();
	std::string Resolution(){ return m_resolution; }
private:
	va_list ap;
	const char* m_err;
	std::string m_resolution;
};

#endif /* SKYNETEXCEPTION_H_ */
