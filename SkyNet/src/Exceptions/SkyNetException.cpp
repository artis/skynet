/*
 * SkyNetException.cpp
 *
 *  Created on: Oct 19, 2012
 *      Author: dimitar
 */

#include "SkyNetException.h"
#include "Logging/Log.h"
#include <stdio.h>
#include <iostream>
SkyNetException::SkyNetException(const char * err, string resolution,...) {
    va_start(ap, err);

	std::cout << "\r\033[1;" << 31 << "m";
	fprintf(stdout, "SkyNetException caught! Error:");
	std::cout << "\033[0m";

	printf("\n");

    std::cout << "\r\033[1;" << 31 << "m";
    vfprintf(stdout, err, ap);
    std::cout << "\033[0m";

    printf("\n");
    std::cout << "\rCommand> ";
    va_end(ap);
    m_resolution = resolution;
}

SkyNetException::~SkyNetException() {
}


