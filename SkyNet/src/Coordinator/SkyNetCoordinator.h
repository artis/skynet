/*
 * SkyNetCoordinator.h
 *
 *  Created on: Sep 25, 2012
 *      Author: dimitar
 */

#ifndef SKYNETCOORDINATOR_H_
#define SKYNETCOORDINATOR_H_

#include <string>
#include "defines.h"
#include "common.h"
#include "Connection/GameSocketMgr.h"

using namespace std;
class Room;
namespace SkyNet{
/*! \brief SkyNetCoordinator is a helper class, which is used to perform certain task easier.
 *
 *  This class makes certain tasks easier for the programmer.
 *  It can perform various of tasks such as, but not limited to:
 *  * Starting a thread.
 *  * Configuring Core settings.
 *  * Open listener.
 */

	class SkyNetCoordinator {
	public:
		SkyNetCoordinator();
		virtual ~SkyNetCoordinator();
		const string& getListenerIp() const;
		void SetListenerIp(const string& listenerIp);
		uint getListenerPort() const;
		void AddAcceptor(string listenerIp, uint listenerPort){
            m_acceptors.push_back(AcceptorData(listenerIp, listenerPort));
		}
		void SetListenerPort(uint listenerPort);
		void RegisterRoomGroup(uint8 id, uint8 workers);
		void RegisterRoomScript(uint16 type, uint8 pool, Room* instance);
		void CreateRoom(uint16 type);
		bool Initialize();
		bool Prime();
		void Wait();
	private:
		string listener_ip;
		uint listener_port;
		vector<AcceptorData> m_acceptors;
	};
}
#endif /* SKYNETCOORDINATOR_H_ */
