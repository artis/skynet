/*
 * SkyNetCoordinator.cpp
 *
 *  Created on: Sep 25, 2012
 *      Author: dimitar
 */

#include "SkyNetCoordinator.h"
#include "Connection/GameSocketMgr.h"
#include "Threading/ThreadSystem.h"
#include "GameCore/Room.h"
#include "Exceptions/SkyNetException.h"
#include "Config/Config.h"
#include "Database/DatabaseEnv.h"
#include "Security/SecurityPolicyThread.h"

using namespace SkyNet;

enum {
    INVALID_METHOD = -1,
/*    SSLv2_client = 1,
    SSLv2_server,
    SSLv2, */
    SSLv3_client = 4,
    SSLv3_server,
    SSLv3,
/*    SSLv23_client,
    SSLv23_server,
    SSLv23, */
    TLSv1_client = 10,
    TLSv1_server,
    TLSv1
  };

SkyNetCoordinator::SkyNetCoordinator() {

}

SkyNetCoordinator::~SkyNetCoordinator() {
}

const string& SkyNetCoordinator::getListenerIp() const {
	return listener_ip;
}

void SkyNetCoordinator::SetListenerIp(const string& listenerIp) {
	listener_ip = listenerIp;
}

uint SkyNetCoordinator::getListenerPort() const {
	return listener_port;
}

void SkyNetCoordinator::SetListenerPort(uint listenerPort) {
	listener_port = listenerPort;
}
void SkyNetCoordinator::RegisterRoomGroup(uint8 id, uint8 workers){
	sGameCore->RegisterRoomGroup(id, workers);
}
void SkyNetCoordinator::RegisterRoomScript(uint16 type, uint8 pool, Room* instance){
	instance->Initialize_();
	RoomTemplateHolder* roomScript = new RoomTemplateHolder(instance, pool);
	try{
		sGameCore->RegisterRoomScript(type, roomScript);
	}catch(SkyNetException& ex){

	}
}

void SkyNetCoordinator::CreateRoom(uint16 type){
	sGameCore->CreateRoom(type);
}
bool SkyNetCoordinator::Initialize(){
	sLog->outNormal("");
    sLog->outNormal("   SSSSS    K  K    Y   Y  NN    N  EEEEEE  TTTTTTT");
    sLog->outNormal("  S         K K      Y Y   N N   N  E          T   ");
    sLog->outNormal("   SSSSS    KK        Y    N  N  N  EEEE       T   ");
    sLog->outNormal("        S   K K       Y    N   N N  E          T   ");
    sLog->outNormal("   SSSSS    K  K      Y    N    NN  EEEEEE     T   ");
	sLog->outNormal("");
	sLog->outNormal("                                         Ver. 0.2    ");
	sLog->outNormal("");
	sLog->outNormal("");


	std::string dbstring;
    uint8 async_threads, synch_threads;
    sConfig->SetSource("./skynet.conf");
    //sLog->outDebug("Using config file %s",sConfig->GetFilename().c_str());
    ///- Get login database info from configuration file
    dbstring = sConfig->GetStringDefault("SkyNetDatabaseInfo", "127.0.0.1;3306;root;password1;SkyNet");


    if (dbstring.empty())
    {
        sLog->outError("Login database not specified in configuration file");
        return false;
    }

    async_threads = sConfig->GetIntDefault("SkyNetDatabase.WorkerThreads", 1);
    if (async_threads < 1 || async_threads > 32)
    {
        sLog->outError("Login database: invalid number of worker threads specified. "
            "Please pick a value between 1 and 32.");
        return false;
    }

    synch_threads = sConfig->GetIntDefault("SkyNetDatabase.SynchThreads", 1);
    ///- Initialise the login database

    if (!SkyNetDatabase.Open(dbstring, async_threads, synch_threads))
    {
        sLog->outNormalInLine("Connecting to SkyNet database - ");
        sLog->SetColor(RED);
        sLog->outNormal("Failed");
		sLog->ResetColor();
        sLog->outError("Error: Cannot connect to skynet database %s", dbstring.c_str());
        return false;
    }

    sLog->outNormalInLine("Connecting to SkyNet database - ");
    sLog->SetColor(LGREEN);
    sLog->outNormal("OK");
	sLog->ResetColor();
    SkyNetDatabase.DirectPExecute("TRUNCATE `console`");
    SkyNetDatabase.DirectPExecute("TRUNCATE `rooms`");
    sLog->setSqlActive(true);



    ///- Get login database info from configuration file
    dbstring = sConfig->GetStringDefault("StorageDatabaseInfo", "127.0.0.1;3306;user;password2;Storage");
    if (dbstring.empty())
    {
        sLog->outError("Login database not specified in configuration file");
        return false;
    }

    async_threads = sConfig->GetIntDefault("StorageDatabase.WorkerThreads", 1);
    if (async_threads < 1 || async_threads > 32)
    {
        sLog->outError("Login database: invalid number of worker threads specified. "
            "Please pick a value between 1 and 32.");
        return false;
    }

    synch_threads = sConfig->GetIntDefault("StorageDatabase.SynchThreads", 1);
    ///- Initialise the login database
    if (!StorageDatabase.Open(dbstring, async_threads, synch_threads))
    {
        sLog->outNormalInLine("Connecting to Storage database - ");
        sLog->SetColor(RED);
        sLog->outNormal("Failed");
		sLog->ResetColor();
        sLog->outError("Error: Cannot connect to storage database %s", dbstring.c_str());
        return false;
    }

    sLog->outNormalInLine("Connecting to Storage database - ");
    sLog->SetColor(LGREEN);
    sLog->outNormal("OK");
	sLog->ResetColor();

    return true;
}
bool SkyNetCoordinator::Prime(){

	ACE_SSL_Context *context = ACE_SSL_Context::instance ();
	context->set_mode(TLSv1_server);
	context->certificate ("./server.pem", SSL_FILETYPE_PEM);
	context->private_key ("./server.pem", SSL_FILETYPE_PEM);

	string cipher_list = "ALL";

	int status = SSL_CTX_set_cipher_list(context->context(),
			  	  	  	  	  	  	  	  	  cipher_list.c_str());

    sLog->outNormalInLine("Configuring SSL - ");
	if(status == 1){
        sLog->SetColor(LGREEN);
        sLog->outNormal("OK");
		sLog->ResetColor();
	}
	else{
        sLog->SetColor(RED);
        sLog->outNormal("Failed");
		sLog->ResetColor();
    }

	SkyNet::Threading::Thread security_policy_thread(new SecurityPolicyThread);
    security_policy_thread.setPriority(SkyNet::Threading::Lowest);

	SkyNet::Threading::Thread bg_thread(new MasterRunnable);
	bg_thread.setPriority(SkyNet::Threading::Highest);





	sSocketMgr->StartNetwork(m_acceptors);
    sLog->outNormal("\nAwaiting incoming connections...");
	return true;

}

void SkyNetCoordinator::Wait(){
	sSocketMgr->Wait();
}
