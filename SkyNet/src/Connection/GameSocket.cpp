#include "GameSocket.h"

#include "GameSocketMgr.h"
#include "common.h"
#include "ServiceOpcodes.h"
#include "GameCore/GameCore.h"
//#include "Database/DatabaseEnv.h"
#include "GameCore/Global.h"
#include <ace/Message_Block.h>
#include <ace/OS_NS_string.h>
#include <ace/OS_NS_unistd.h>
#include <ace/os_include/arpa/os_inet.h>
#include <ace/os_include/netinet/os_tcp.h>
#include <ace/os_include/sys/os_types.h>
#include <ace/os_include/sys/os_socket.h>
#include <ace/OS_NS_string.h>
#include <ace/Reactor.h>
#include <ace/Auto_Ptr.h>

#include "ModuleSystem/ModuleMgr.h"


struct ServerPktHeader
{
    /**
     * size is the length of the payload _plus_ the length of the opcode
     */
    ServerPktHeader(uint16 size, uint16 cmd, uint16 flags) : size(size)
    {
        uint8 headerIndex=0;

        header[headerIndex++] = 0xFF & size;
        header[headerIndex++] = 0xFF & (size>>8);

        header[headerIndex++] = 0xFF & cmd;
		header[headerIndex++] = 0xFF & (cmd>>8);

		header[headerIndex++] = 0xFF & flags;
		header[headerIndex++] = 0xFF & (flags>>8);


    }

    uint8 getHeaderLength()
    {
        // cmd = 2 bytes, size = 2, flags = 2
        return 6;
    }

    const uint16 size;
    uint8 header[6];
    uint16 flags;
};

struct ClientPktHeader
{
    uint16 size;
    uint16 cmd;
    uint16 flags;
};


SkyNet::Networking::GameSocket::GameSocket (void): LobbyHandler(),
m_LastPingTime(ACE_Time_Value::zero), m_OverSpeedPings(0), m_Session(NULL),
m_RecvWPct(0), m_RecvPct(),m_Header(sizeof(ClientPktHeader)),m_OutBuffer(0), m_OutBufferSize(65536), m_OutActive(false)
{
    reference_counting_policy().value (ACE_Event_Handler::Reference_Counting_Policy::ENABLED);

    msg_queue()->high_water_mark(8*1024*1024);
    msg_queue()->low_water_mark(8*1024*1024);
    incomingEncryption = false;
    outgoingEncryption = false;
	sLog->outDebug("SkyNet::Networking::GameSocket::GameSocket");
}
SkyNet::Networking::GameSocket::~GameSocket (void)
{
    sLog->outDebug("SkyNet::Networking::GameSocket::~GameSocket");

    delete m_RecvWPct;

    if (m_OutBuffer)
        m_OutBuffer->release();

    closing_ = true;

    peer().peer().close();
}
const std::string& SkyNet::Networking::GameSocket::GetRemoteAddress (void) const
{
    return m_Address;
}

int SkyNet::Networking::GameSocket::handle_input (ACE_HANDLE)
{
    if (closing_)
        return -1;

    switch (handle_input_missing_data())
    {
        case -1 :
        {
            if ((errno == EWOULDBLOCK) ||
                (errno == EAGAIN))
            {
                return Update();                           // interesting line ,isn't it ?
            }

            //sLog->outDebug("LobbySocket::handle_input: Peer error closing connection errno = %s", ACE_OS::strerror (errno));

            errno = ECONNRESET;
            return -1;
        }
        case 0:
        {
            //sLog->outDebug("LobbySocket::handle_input: Peer has closed connection");

            errno = ECONNRESET;
            return -1;
        }
        case 1:
            return 1;
        default:
            return Update();                               // another interesting line ;)
    }

    ACE_NOTREACHED(return -1);
}


int SkyNet::Networking::GameSocket::handle_output (ACE_HANDLE)
{
	//sLog->outDebug("SkyNet::Networking::GameSocket::handle_output trying to send");
    ACE_GUARD_RETURN (LockType, Guard, m_OutBufferLock, -1);

    if (closing_)
        return -1;

    size_t send_len = m_OutBuffer->length();

    if (send_len == 0)
        return handle_output_queue(Guard);

    ssize_t n = peer().peer().send (m_OutBuffer->rd_ptr(), send_len);
/*#ifdef MSG_NOSIGNAL
    ssize_t n = peer().peer().send (m_OutBuffer->rd_ptr(), send_len);
#else
    ssize_t n = peer().peer().send (m_OutBuffer->rd_ptr(), send_len);
#endif // MSG_NOSIGNAL*/

    if (n == 0)
        return -1;
    else if (n == -1)
    {
        if (errno == EWOULDBLOCK || errno == EAGAIN)
            return schedule_wakeup_output (Guard);

        return -1;
    }
    else if (n < (ssize_t)send_len) //now n > 0
    {
        m_OutBuffer->rd_ptr (static_cast<size_t> (n));

        // move the data to the base of the buffer
        m_OutBuffer->crunch();

        return schedule_wakeup_output (Guard);
    }
    else //now n == send_len
    {
        m_OutBuffer->reset();

        return handle_output_queue (Guard);
    }

    ACE_NOTREACHED (return 0);
}

int SkyNet::Networking::GameSocket::handle_output_queue (GuardType& g)
{
    if (msg_queue()->is_empty())
        return cancel_wakeup_output(g);

    ACE_Message_Block *blk;

    if (msg_queue()->dequeue_head(blk, (ACE_Time_Value*)&ACE_Time_Value::zero) == -1)
    {
        //sLog->outError("LobbySocket::handle_output_queue dequeue_head");
        return -1;
    }
    Trojan_Message_Block *mblk = reinterpret_cast<Trojan_Message_Block*>(blk);

    const size_t send_len = mblk->length();

    ssize_t _n;
    if(mblk->IsEncrypted())
    	_n = peer().send (mblk->rd_ptr(), send_len);
    else
    	_n = peer().peer().send (mblk->rd_ptr(), send_len);

    const ssize_t n = _n;
/*#ifdef MSG_NOSIGNAL
    ssize_t n = peer().peer().send (mblk->rd_ptr(), send_len);
#else
    ssize_t n = peer().peer().send (mblk->rd_ptr(), send_len);
#endif // MSG_NOSIGNAL*/

    if (n == 0)
    {
        mblk->release();

        return -1;
    }
    else if (n == -1)
    {
        if (errno == EWOULDBLOCK || errno == EAGAIN)
        {
            msg_queue()->enqueue_head(mblk, (ACE_Time_Value*) &ACE_Time_Value::zero);
            return schedule_wakeup_output (g);
        }

        mblk->release();
        return -1;
    }
    else if (n < (ssize_t)send_len) //now n > 0
    {
        mblk->rd_ptr(static_cast<size_t> (n));

        if (msg_queue()->enqueue_head(mblk, (ACE_Time_Value*) &ACE_Time_Value::zero) == -1)
        {
            //sLog->outError("LobbySocket::handle_output_queue enqueue_head");
            mblk->release();
            return -1;
        }

        return schedule_wakeup_output (g);
    }
    else //now n == send_len
    {
        mblk->release();

        return msg_queue()->is_empty() ? cancel_wakeup_output(g) : ACE_Event_Handler::WRITE_MASK;
    }

    ACE_NOTREACHED(return -1);
}

int SkyNet::Networking::GameSocket::handle_close (ACE_HANDLE h, ACE_Reactor_Mask)
{
	sLog->outDebug("SkyNet::Networking::GameSocket::handle_close()");
    // Critical section
    {
        ACE_GUARD_RETURN (LockType, Guard, m_OutBufferLock, -1);

        closing_ = true;

        if (h == ACE_INVALID_HANDLE)
            peer().peer().close_writer();
    }

    // Critical section
    {
        ACE_GUARD_RETURN (LockType, Guard, m_SessionLock, -1);
		m_Session->RemoveReference();
        m_Session = NULL;
		
    }
    reactor()->remove_handler(this, ACE_Event_Handler::DONT_CALL | ACE_Event_Handler::ALL_EVENTS_MASK);
    return 0;
}
uint64 power(uint64 a, uint64 b)
{
     uint64 c=a;
     for (uint64 n=b; n>1; n--) c*=a;
     return c;
}
int SkyNet::Networking::GameSocket::open (void *a)
{
    ACE_UNUSED_ARG (a);

    // Prevent double call to this func.
    if (m_OutBuffer)
        return -1;

    // This will also prevent the socket from being Updated
    // while we are initializing it.
    m_OutActive = true;
    isAuthed = false;
    // Hook for the manager.
    _timer = 1000;
    _tick = getMSTime();
    //_stats = new SocketStatistics();
    //_stats->reset();

    if (sSocketMgr->OnSocketOpenHook(this) == -1)
        return -1;

    // Allocate the buffer.
    ACE_NEW_RETURN (m_OutBuffer, ACE_Message_Block (m_OutBufferSize), -1);

    // Store peer address.
    ACE_INET_Addr remote_addr;

    if (peer().get_remote_addr(remote_addr) == -1)
    {
        //sLog->outError ("LobbySocket::open: peer().get_remote_addr errno = %s", ACE_OS::strerror (errno));
        return -1;
    }

    m_Address = remote_addr.get_host_addr();

    // Send startup packet.
    setSession(new GameSession(this));
	m_Session->AddReference();
    ComPacket packet(SSOP_AUTH_REQUEST,1,1);




/*
    int P = 11;
    int G = 7;
    Xs = (rand()%2000000000)+1;

    uint64 Ys = (power(G, Xs) % P);
//sLog->outDebug("[Encryption] Ys = %lu", Ys);
    packet << Ys;
*/
    //sLog->outDebug("::open trying to send packet");
    if (SendPacket(packet) == -1){
    	 sLog->outDebug("::open something went wrong");
        return -1;
    }
	sLog->outDebug("Sent packet");
    //sLog->outDebug("::open packet should be sent");
    // Register with ACE Reactor
    if (reactor()->register_handler(this, ACE_Event_Handler::READ_MASK | ACE_Event_Handler::WRITE_MASK) == -1)
    {
        //sLog->outError ("LobbySocket::open: unable to register client handler errno = %s", ACE_OS::strerror (errno));
        return -1;
    }

    // reactor takes care of the socket from now on
    remove_reference();

    return 0;
}

int SkyNet::Networking::GameSocket::SendPacket (const ComPacket& pct)
{
	ACE_GUARD_RETURN (LockType, Guard, m_OutBufferLock, -1);

	//sLog->outDebug("SkyNet::Networking::GameSocket::SendPacket(): Sending packet with opcode %u and flags %u", pct.GetOpcode(),pct.GetFlags());

	//_stats->incOutPps(1);
	//_stats->incOutBps(pct.size() + 6);

	if(pct.HasFlag(2)){
		ComPacket encryptPacket(SSOP_SET_ENCRYPTION,0,1);
			    	SendPacket(encryptPacket);
		//sLog->outDebug("SkyNet::Networking::GameSocket::SendPacket: Sending encrypted packet");
		outgoingEncryption = true;
	}else{
		//sLog->outDebug("SkyNet::Networking::GameSocket::SendPacket: Sending non-encrypted packet");
	}

    if (closing_)
        return -1;


//sLog->outDebug("Sending packet...");
    ////sLog->outDebug("Packet size: %u opcode: %u flags: %u",pct.size(), pct.GetOpcode(), pct.GetFlags());
    //		pct.hexlike();
    ServerPktHeader header(pct.size()+4, pct.GetOpcode(), pct.GetFlags());

    // If there is enough space in the buffer and there are no other
    // packets pending to be send, directly send the packet,
    // otherwise enqueue the packet
    if ((m_OutBuffer->space() >= pct.size() + header.getHeaderLength() && msg_queue()->is_empty()) && !pct.HasFlag((uint16)ENCRYPTED_PACKET))
    {
    	//sLog->outDebug("SkyNet::Networking::GameSocket::SendPacket(): Putting packet with opcode %u and flags %u in the stream", pct.GetOpcode(),pct.GetFlags());

    	//Put the header on the buffer
    	if (m_OutBuffer->copy((char*) header.header, header.getHeaderLength()) == -1)
    	            ACE_ASSERT (false);
    	SkyNet::Networking::ByteBuffer buf;
    	buf.append((char*)header.header, header.getHeaderLength());
    	//buf.hexlike();

    	//Put the packet on the buffer
        if (!pct.empty())
            if (m_OutBuffer->copy((char*) pct.contents(), pct.size()) == -1)
                ACE_ASSERT (false);
    }
    else
    {
		//sLog->outDebug("Puting packet into the queue!");
    	//sLog->outDebug("SkyNet::Networking::GameSocket::SendPacket(): Putting packet with opcode %u and flags %u in the queue", pct.GetOpcode(),pct.GetFlags());

        // Enqueue the packet.
        Trojan_Message_Block* mb;

        ACE_NEW_RETURN(mb, Trojan_Message_Block(pct.size() + header.getHeaderLength()), -1);

        if(pct.HasFlag((uint16)ENCRYPTED_PACKET)){
        	//sLog->outDebug("Setting encryption");
        	mb->Encrypt();
        }
        mb->copy((char*) header.header, header.getHeaderLength());

        if (!pct.empty())
            mb->copy((const char*)pct.contents(), pct.size());

        if (msg_queue()->enqueue_tail(mb,(ACE_Time_Value*)&ACE_Time_Value::zero) == -1)
        {
            sLog->outError("LobbySocket::SendPacket enqueue_tail failed");
            mb->release();
            return -1;
        }
    }

    return 0;
}

int SkyNet::Networking::GameSocket::close (int)
{
	sLog->outDebug("SkyNet::Networking::GameSocket::close()");
    closing_ = true;
    shutdown();



    remove_reference();

    return 0;
}

int SkyNet::Networking::GameSocket::Update (void)
{
    if (closing_)
        return -1;

	if(m_Session && m_Session->GetPlayer() && !m_Session->GetPlayer()->isAuthed())
		m_Session->GetPlayer()->_ProcessCallbacks();

    if (m_OutActive || (m_OutBuffer->length() == 0 && msg_queue()->is_empty()))
        return 0;

    int ret;
    do
        ret = handle_output (get_handle());
    while (ret > 0);

	//sLog->outDebug("SkyNet::Networking::GameSocket::Update() returning %d", ret);

    return ret;
}
int SkyNet::Networking::GameSocket::cancel_wakeup_output (GuardType& g)
{
    if (!m_OutActive)
        return 0;

    m_OutActive = false;

    g.release();

    if (reactor()->cancel_wakeup
        (this, ACE_Event_Handler::WRITE_MASK) == -1)
    {
        //sLog->outError ("LobbySocket::cancel_wakeup_output");
        return -1;
    }

    return 0;
}

int SkyNet::Networking::GameSocket::schedule_wakeup_output (GuardType& g)
{
    if (m_OutActive)
        return 0;

    m_OutActive = true;

    g.release();

    if (reactor()->schedule_wakeup
        (this, ACE_Event_Handler::WRITE_MASK) == -1)
    {
        //sLog->outError ("LobbySocket::schedule_wakeup_output");
        return -1;
    }

    return 0;
}

int SkyNet::Networking::GameSocket::handle_input_header (void)
{

    ACE_ASSERT (m_RecvWPct == NULL);

    ACE_ASSERT (m_Header.length() == sizeof(ClientPktHeader));

    ClientPktHeader& header = *((ClientPktHeader*) m_Header.rd_ptr());

    //sLog->outDebug("LobbySocket::handle_input_header: client sent packet size = %d , cmd = %d",
    //		header.size, header.cmd);

    if ((header.size < 3) || (header.size > 30096))
    {
        sLog->outError ("LobbySocket::handle_input_header: client sent malformed packet size = %d , cmd = %d",
                      header.size, header.cmd);

        errno = EINVAL;
        return -1;
    }
    //sLog->outDebug("Receiving packet size: %u", header.size);
    header.size -= 4;

    //sLog->outDebug("Creating ComPacket with size: %u", header.size);
    ACE_NEW_RETURN (m_RecvWPct, ComPacket ((uint16) header.cmd, header.size, header.flags), -1);

    if (header.size > 0)
    {
        m_RecvWPct->resize (header.size);
        //sLog->outDebug("Packet header info size: %u cmd: %u flags: %u", header.size, header.cmd, header.flags);
        m_RecvPct.base ((char*) m_RecvWPct->contents(), m_RecvWPct->size());

    }
    else
    {
        ACE_ASSERT(m_RecvPct.space() == 0);
    }


    return 0;
}


int SkyNet::Networking::GameSocket::handle_input_payload (void)
{
    // Asserts to track problems with received packets.
//sLog->outDebug("SkyNet::Networking::GameSocket::handle_input_payload");
    ACE_ASSERT (m_RecvPct.space() == 0);
    ACE_ASSERT (m_Header.space() == 0);
    ACE_ASSERT (m_RecvWPct != NULL);


    const int ret = ProcessIncoming (m_RecvWPct);
	m_RecvPct.base (NULL, 0);
    m_RecvPct.reset();
    m_RecvWPct = NULL;

    m_Header.reset();

    if (ret == -1)
        errno = EINVAL;

    return ret;
}

int SkyNet::Networking::GameSocket::handle_input_missing_data (void)
{
    char buf [4096];

    ACE_Data_Block db (sizeof (buf),
                        ACE_Message_Block::MB_DATA,
                        buf,
                        0,
                        0,
                        ACE_Message_Block::DONT_DELETE,
                        0);

    ACE_Message_Block message_block(&db,
                                    ACE_Message_Block::DONT_DELETE,
                                    0);

    const size_t recv_size = message_block.space();

    const ssize_t n = receive (message_block.wr_ptr(),
                                          recv_size);

    if (n <= 0)
        return int(n);

    message_block.wr_ptr (n);

    while (message_block.length() > 0)
    {
        if (m_Header.space() > 0)
        {
            //need to receive the header
            const size_t to_header = (message_block.length() > m_Header.space() ? m_Header.space() : message_block.length());
            m_Header.copy (message_block.rd_ptr(), to_header);
            message_block.rd_ptr (to_header);

            if (m_Header.space() > 0)
            {
                // Couldn't receive the whole header this time.
                ACE_ASSERT (message_block.length() == 0);
                errno = EWOULDBLOCK;
                return -1;
            }

            // We just received nice new header
            if (handle_input_header() == -1)
            {
                ACE_ASSERT ((errno != EWOULDBLOCK) && (errno != EAGAIN));
                return -1;
            }
        }

        // Its possible on some error situations that this happens
        // for example on closing when epoll receives more chunked data and stuff
        // hope this is not hack, as proper m_RecvWPct is asserted around
        if (!m_RecvWPct)
        {
            sLog->outError( "Forcing close on input m_RecvWPct = NULL");
            errno = EINVAL;
            return -1;
        }

        // We have full read header, now check the data payload
        if (m_RecvPct.space() > 0)
        {
            //need more data in the payload
            const size_t to_data = (message_block.length() > m_RecvPct.space() ? m_RecvPct.space() : message_block.length());
            m_RecvPct.copy (message_block.rd_ptr(), to_data);
            message_block.rd_ptr (to_data);

            if (m_RecvPct.space() > 0)
            {
                // Couldn't receive the whole data this time.
                ACE_ASSERT (message_block.length() == 0);
                errno = EWOULDBLOCK;
                return -1;
            }
        }

        //just received fresh new payload
        if (handle_input_payload() == -1)
        {
            ACE_ASSERT ((errno != EWOULDBLOCK) && (errno != EAGAIN));
            return -1;
        }
    }

    return size_t(n) == recv_size ? 1 : 2;
}

void SkyNet::Networking::GameSocket::HandleFileHeader (ComPacket& recvPacket)
{

	string name;
	string path;
	int size;
	recvPacket >> path;
	recvPacket >> name;
	recvPacket >> size;
	m_recvFile = new SkyNet_File(size);
	m_recvFile->SetFilename(name);
	//sLog->outDebug("Current rpos %d ", recvPacket.rpos());
	recvPacket.rpos(0);

	m_recvFileData.clear();
	m_recvFileData.push_back(&recvPacket);
}
void SkyNet::Networking::GameSocket::HandleFileData (ComPacket& recvPacket)
{
	//sLog->outDebug("Received file data");
	m_recvFileData.push_back(&recvPacket);
	if(m_recvFile->GetNumPackets()+1 == m_recvFileData.size())
	{	
		//sLog->outDebug("Received the %d packet. We are ready to build a file.",m_recvFileData.size());

		m_recvFile->LoadFromPackets(m_recvFileData);
		Dispatch(int(NETWORKING), int(RECIEVE_FILE), m_recvFile, m_Session->GetPlayer());
		m_recvFile = NULL;
	}
}
int SkyNet::Networking::GameSocket::ProcessIncoming (ComPacket* new_pct)
{
    ACE_ASSERT (new_pct);

	//new_pct->hexlike();
    if(incomingEncryption){
    	incomingEncryption = false;
    	//sLog->outDebug("Setting encryption to FALSE");
    }
    // Ace memory management procedure.
    // When aptr gets destroyed (at the end of this scope)
    // new_pct will be unallocated also.
    // Who let the dogs out?! Who ?! Who ?! Who?!
    ACE_Auto_Ptr<ComPacket> aptr (new_pct);

    const ACE_UINT16 opcode = new_pct->GetOpcode();

    if (closing_)
        return -1;

    //sLog->outDebug("Return -1 to simulate problem.");
    //return -1;
    try
    {

    	//sLog->outDebug("Received packet Size: %u Opcode: %u Flags: %u",new_pct->size(),opcode,new_pct->GetFlags());
		if(new_pct->HasFlag(ENCRYPTED_PACKET)){

			//sLog->outDebug("Decrypting Packet...");
		}

    	if(new_pct->HasFlag(SERVICE_LAYER_PACKET)){
    		//sLog->outDebug("True");

			switch(opcode)
			{
			case OP_FILE_HEADER:
				this->HandleFileHeader(*new_pct);
				aptr.release();
				return 0;
				break;
			case OP_FILE_DATA:
				HandleFileData(*new_pct);
				aptr.release();
				return 0;
				break;

				case SCOP_PING:
					HandlePing (*new_pct);
					return 0;
					break;
				case SCOP_SET_ENCRYPTION:
					HandleEncryption(*new_pct);
					break;
				case SCOP_SANDBOX_AUTH_SESSION:
					if (m_Session->GetPlayer())
					{
						sLog->outError ("GameSocket::ProcessIncoming: Player send CMSG_AUTH_SESSION again");
						return -1;
					}
					sLog->outDebug ("GameSocket::ProcessIncoming: Player send CMSG_AUTH_SESSION");

					Dispatch(int(YIELD_TYPE_SESSION), int(AUTHENTICATE), new_pct, m_Session);
					return 0;
				case SSOP_REGISTRATION:
					if (m_Session->GetPlayer())
					{

						sLog->outError ("GameSocket::ProcessIncoming: Player send SSOP_REGISTRATION, but he/she is already logged in with an account.");
						return -1;
					}
					sLog->outDebug ("GameSocket::ProcessIncoming: Player send SSOP_REGISTRATION");
                    Dispatch(int(YIELD_TYPE_SESSION), int(REGISTER), new_pct, m_Session);
					return 0;
					break;
				case SCOP_AUTH_SESSION:
					if (m_Session->GetPlayer())
					{

						sLog->outError ("GameSocket::ProcessIncoming: Player send SSOP_AUTH_SESSION again");
						return -1;
					}
					sLog->outDebug ("GameSocket::ProcessIncoming: Player send SSOP_AUTH_SESSION");

					//sSocketMgr->OnSocketOpen(this);
					Dispatch(int(YIELD_TYPE_SESSION), int(AUTHENTICATE), new_pct, m_Session);
					return 0;
			   /* case COP_CHAT_MESSAGE:
					if (m_Session)
					{
						return -1;
					}
					//sLog->outDebug ("LobbySocket::ProcessIncoming: Player send CMSG_CHAT_MESSAGE");

					 m_Session->HandleChat (*new_pct);
					 return 0;*/
				default:
				{
					// All rest service opcodes (create, list and join rooms)
					// should be processed by the GameCore in the Master thread,
					// So lets pass the packet to the session.
					ACE_GUARD_RETURN (LockType, Guard, m_SessionLock, -1);
					if (m_Session && m_Session->GetPlayer())
					{
						//sLog->outDebug("SkyNet::Networking::GameSocket::ProcessIncoming: Queuing packet opcode: %d", new_pct->GetOpcode());
						m_Session->QueuePacket (new_pct);
						//sLog->outDebug("SkyNet::Networking::GameSocket::ProcessIncoming: Done");
						aptr.release();
						return 0;
					}else{
						sLog->outDebug("m_Session && m_Session->GetPlayer() returned false.");
					}
					break;
				}
			}
    	}
    	else{
    		//sLog->outDebug("SkyNet::Networking::GameSocket::ProcessIncoming(): Requesting Session lock.");
    		ACE_GUARD_RETURN (LockType, Guard, m_SessionLock, -1);
    		//sLog->outDebug("SkyNet::Networking::GameSocket::ProcessIncoming(): Session Lock acquired.");
    		if (m_Session)
			{
    			//sLog->outDebug("SkyNet::Networking::GameSocket::ProcessIncoming(): Received packet with opcode %u", new_pct->GetOpcode());

				// OK ,give the packet to LobbySession
				//sLog->outDebug("SkyNet::Networking::GameSocket::ProcessIncoming(): Queing packet.");
				m_Session->QueuePacket (new_pct);
				aptr.release();
				return 0;
			}
			else
			{
				sLog->outError ("LobbySocket::ProcessIncoming: Client not authed opcode = %u", uint32(opcode));
				return -1;
			}
    	}
    }
    catch (SkyNet::Networking::ByteBufferException &)
    {

        sLog->outError("LobbySocket::ProcessIncoming ByteBufferException occured while parsing an instant handled packet (opcode: %u) from client %s. Disconnected client.",
                opcode, GetRemoteAddress().c_str());

        //sLog->outDebug("Dumping error causing packet:");
       // new_pct->hexlike();

		sLog->outDebug("Exception caught");
        return -1;
    }
	catch (...)
	{
		sLog->outDebug("Exception caught");
	}

    ACE_NOTREACHED (return 0);
}

bool SkyNet::Networking::GameSocket::IsClosed (void) const
{
    return closing_;
}

void SkyNet::Networking::GameSocket::CloseSocket (void)
{
	sLog->outDebug("SkyNet::Networking::GameSocket::CloseSocket()");
    {
        ACE_GUARD (LockType, Guard, m_OutBufferLock);

        if (closing_)
            return;

        closing_ = true;
        peer().peer().close_writer();
    }

    {
        ACE_GUARD (LockType, Guard, m_SessionLock);

        m_Session = NULL;
    }
}

long SkyNet::Networking::GameSocket::AddReference (void)
{
	//sLog->outDebug("SkyNet::Networking::GameSocket::AddReference");
    return static_cast<long> (add_reference());
}

long SkyNet::Networking::GameSocket::RemoveReference (void)
{
	//sLog->outDebug("SkyNet::Networking::GameSocket::RemoveReference");
    return static_cast<long> (remove_reference());
}
void SkyNet::Networking::GameSocket::HandlePing(ComPacket &_packet){
	ComPacket packet(SSOP_PONG,0,SERVICE_LAYER_PACKET);
	int64 latency;
	_packet >> latency;
	//sLog->outDebug("Player latency: %u", latency);
	SendPacket(packet);
}



int SkyNet::Networking::GameSocket::HandleEncryption(ComPacket &_packet){
	this->incomingEncryption = true;
	return 0;
}

void SkyNet::Networking::GameSocket::StatisticsCheck(){
	uint32 diff = getMSTime() - _tick;
	_timer -= diff;
	_tick = getMSTime();
	if(_timer <= 0){
		_timer = 1000;
		//_stats->calculate();
	}
}

int SocketStatistics::getInBps() const
{
    return __inBps;
}

int SocketStatistics::getInPps() const
{
    return __inPps;
}

int SocketStatistics::getOutBps() const
{
    return __outBps;
}

int SocketStatistics::getOutPps() const
{
    return __outPps;
}

void SocketStatistics::incInBps(int inBps)
{
    _inBps += inBps;
}

void SocketStatistics::incInPps(int inPps)
{
    _inPps += inPps;
}

void SocketStatistics::incOutBps(int outBps)
{
    _outBps += outBps;
}

void SocketStatistics::incOutPps(int outPps)
{
    _outPps += outPps;
}

void SocketStatistics::reset(){
	__inPps		= 0;
	__outPps	= 0;
	__inBps		= 0;
	__outBps	= 0;

	_inPps	= 0;
	_outPps	= 0;
	_inBps	= 0;
	_outBps	= 0;
}
void SocketStatistics::calculate(){
	__inPps		= _inPps;
	__outPps	= _outPps;
	__inBps		= _inBps;
	__outBps	= _outBps;

	_inPps	= 0;
	_outPps	= 0;
	_inBps	= 0;
	_outBps	= 0;
}


