/*
 * GameSession.cpp
 *
 *  Created on: Oct 12, 2011
 *      Author: dimitar
 */
#include "GameSession.h"
#include "GameCore/GameCore.h"
#include "ServiceOpcodes.h"
#include "Exceptions/SkyNetException.h"
#include "GameCore/RoomCreationCallback.h"

enum RoomCreationResponses{
	ROOM_CREATED_SUCESSFULLY	= 1,
	ROOM_CREATION_FAILED		= 2,
};
GameSession::GameSession(uint32 id,  GameSocket *sock): m_Socket(sock), m_player(NULL)
{
	sLog->outDebug("GameSession::GameSession %p",this);
	m_references = 0;
	if (sock)
	{
		m_Address = sock->GetRemoteAddress();
		sock->AddReference();
	}

	//sLog->outDebug("Creating %lu",(uint64)this);
};
GameSession::GameSession(GameSocket *sock): m_Socket(sock), m_player(NULL)
{
	sLog->outDebug("GameSession::GameSession %p",this);
	m_references = 0;
	if (sock)
	{
		m_Address = sock->GetRemoteAddress();
		sock->AddReference();
	}
	//sLog->outDebug("Creating %lu",(uint64)this);
};

bool GameSession::Update(uint32 diff){

    //sLog->outDebug("Updating %lu",(uint64)this);
    ComPacket* packet;
    while (m_Socket && !m_Socket->IsClosed())
    {
        if(!_recvQueue.next(packet))
            break;
    	//sLog->outDebug("Processing packet by thread %d",updater->getType());
    	try{

    		ProcessPacket(packet);
    	}catch(SkyNet::Networking::ByteBufferException &)
		{
			sLog->outDebug("An errer occured while parsing an incoming packet. Skipping the packet.");
		}
    	catch(SkyNetException& ex){
    		sLog->outError("Exception handled internally.");
    		sLog->outError("Resolution: %s", ex.Resolution().c_str());
    	}
        delete packet;
     }

    ///- Cleanup socket pointer if need

	if (m_Socket && m_Socket->IsClosed())
	{
		OnClose();
		m_Socket->RemoveReference();
		sLog->outDebug("GameSession::Update(): Cleaning up socket pointers. References: %u");

		m_Socket = NULL;
		//sLog->outDebug("GameSession::Update(): Done.");
	}

	if (!m_Socket){
		sLog->outDebug("GameSession::Update(): Socket is dead. Returning false.");
		return false;
	}
	return true;
}
/*
void LobbySession::HandleChat(ComPacket &packet){

	std::string message;
	packet >> message;
	ComPacket pack(SOP_PLAYER_CHAT_MESSAGE);
	pack << this->GetAccountId();
	pack << message;
	sLog->outDebug("HANDLE CHAT");
	sLobby.SendGlobalMessage(&packet);
	return;
}

*/


void GameSession::QueuePacket(ComPacket* new_packet)
{
	//sLog->outDebug("GameSession::QueuePacket");
    _recvQueue.add(new_packet);
}
void GameSession::RoomCreateCallback(uint64 roomID)
{
	ComPacket response(SSOP_CREATE_ROOM_RESPONSE);
	response << (uint8)ROOM_CREATED_SUCESSFULLY;
	response << roomID;
	SendPacket(response);
	//sLog->outDebug("Room %d created successfully.", roomID);
}
bool GameSession::ProcessPacket(ComPacket *packet)
    {
	//sLog->outDebug("GameSession::ProcessPacket");
		if(!packet->HasFlag(SERVICE_LAYER_PACKET)){
			sLog->outDebug("Dispatching packet received from player %d ptr %p", GetPlayer()->GetGUID(), GetPlayer());
			Dispatch(YIELD_TYPE_DATA_PACKET , packet->GetOpcode(),packet, GetPlayer(), GetPlayer());
			
		}else
			switch(packet->GetOpcode()){
			case SCOP_CREATE_ROOM:
				{
					//sLog->outDebug("Player sent Room Creation request");
					uint8 room_type;
					(*packet) >> room_type;

					sGameCore->CreateRoom(room_type, new RoomCreationCallback(this));
					
				}
				break;
			case SCOP_JOIN_ROOM:
				{
					uint64 room_id;
					(*packet) >> room_id;
					Room* room = sGameCore->FindRoom(room_id);
					//sLog->outDebug("GameSession::ProcessPacket Got room pointer %lu", (uint64)room);
					if(room){
					    //sLog->outDebug("GameSession::ProcessPacket got room %lu", (uint64)room);
						
						GetPlayer()->_ScheduleForMove(room);
						sGameCore->RemovePlayer(GetPlayer());
					}else{
                        sLog->outDebug("No room");
                    }
				}
				break;

			case SCOP_LIST_ROOMS:
			{
				RoomVector rooms = sGameCore->GetRooms();
				ComPacket response(SSOP_LIST_ROOMS,200,SERVICE_LAYER_PACKET);
				response << (uint32)rooms.size();
				for(uint i = 0; i < rooms.size(); i++){
					uint64 id = rooms[i]->getId();
					uint8 type = rooms[i]->GetType();
					response << id;
					response << type;
					//sLog->outDebug("Packing Room ID: %lu Type: %d", id, type);
				}
				SendPacket(response);
			}
			break;
		}
        return true;
    }
bool GameSession::IsSendQueueEmpty() { return m_Socket->IsSendQueueEmpty(); }
int GameSession::SendPacket(const ComPacket& packet){ 

	if(!m_Socket){
		sLog->outDebug("GameSession::SendPacket(): m_Socket is NULL");
		return -1;
	}
	//sLog->outDebug("YEY");
	m_Socket->SendPacket(packet);
	return 0;
}

void GameSession::Disconnect(){
	if(m_Socket)
		m_Socket->CloseSocket();
}

GameSocket *GameSession::getSocket() const
{
    return m_Socket;
}

void GameSession::setSocket(GameSocket *socket)
{
    m_Socket = socket;
}




