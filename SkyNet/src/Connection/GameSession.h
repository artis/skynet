/*
 * GameSession.h
 *
 *  Created on: Oct 12, 2011
 *      Author: dimitar
 */

#ifndef GAMESESSION_H_
#define GAMESESSION_H_

#include "defines.h"
#include "GameSocket.h"
#include "ModuleSystem/ModuleSystem.h"
#include "ace/Future.h"
#include "Database/DatabaseEnv.h"
#include "Referencable.h"

class RoomCreationCallback;
//class SkyNet::Networking::ComPacket;

namespace SkyNet{
	namespace Core{
		class BasePlayer;
	}
	namespace Networking{
		class GameSocket;
	}
}
using namespace SkyNet::Core;
using namespace SkyNet::Networking;
class GameSession: public YieldSubject, public Yieldable
{
public:
	GameSession(uint32 id,  GameSocket *sock);
    GameSession(GameSocket *sock);
    GameSession() {};
    virtual ~GameSession()
    {
        sLog->outDebug("GameSession::~GameSession");
    };

    void SetSocket(GameSocket *sock)
    {
        m_Socket = sock;
    };

    bool Update(uint32 diff);

    virtual bool HandleAuthentication(std::string username, std::string password, uint32 & userId)
    {
        return true;
    }
    virtual bool HandleSandboxAuthentication(std::string username, std::string password, uint32 & userId)
    {
        return true;
    }
    virtual void OnClose(){};
    
	bool IsSendQueueEmpty();
    int SendPacket(const ComPacket& packet);
    void QueuePacket(ComPacket *new_packet);
    bool ProcessPacket(ComPacket *packet);


    BasePlayer* GetPlayer(){ return m_player; }
    void SetPlayer(BasePlayer* plr){
		sLog->outDebug("GameSession::SetPlayer(%p)", plr);
    	m_player = plr;
    }
    virtual void OnRemoveFromUpdater(){};
    void Disconnect();
    GameSocket *getSocket() const;
    void setSocket(GameSocket *socket);
	
	void RoomCreateCallback(uint64 roomID);

	size_t AddReference()
	{ 
		m_references++; 
		sLog->outDebug("SkyNet_Reference_Counter::AddReference: %d references now", m_references);
		return m_references; 
	};
	size_t RemoveReference()
	{
		size_t result = --m_references;
		sLog->outDebug("SkyNet_Reference_Counter::RemoveReference: %d references left", m_references);
		if(result == 0){
			sLog->outDebug("SkyNet_Reference_Counter::RemoveReference: deleting object");
			delete this;
			return 0;
		
		}
		return result;
	};
protected:
	uint8 _sessionStatus;
	GameSocket* m_Socket;
	std::string m_Address;
	BasePlayer* m_player;
	size_t m_references;
	SkyNet::Threading::LockedQueue<ComPacket*, ACE_Thread_Mutex> _recvQueue;

	RoomCreationCallback* m_roomCreateCallback;
};



typedef UNORDERED_MAP<uint32, GameSession*> SessionMap;
#endif /* GAMESESSION_H_ */
