#ifndef _LOBBYSOCKET_H
#define _LOBBYSOCKET_H

#include <ace/Basic_Types.h>
#include <ace/Synch_Traits.h>
#include <ace/Svc_Handler.h>
#include <ace/SOCK_Stream.h>
#include <ace/SOCK_Acceptor.h>
#include <ace/Acceptor.h>
#include <ace/Thread_Mutex.h>
#include <ace/Guard_T.h>
#include <ace/Unbounded_Queue.h>
#include <ace/Message_Block.h>
#include "ace/SOCK_Connector.h"
#include "ace/Connector.h"
#include <ace/SSL/SSL_SOCK_Stream.h>
#include <ace/SSL/SSL_SOCK_Acceptor.h>

#if !defined (ACE_LACKS_PRAGMA_ONCE)
#pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */

#include "defines.h"
#include "ComPacket.h"
#include "GameSession.h"
#include "ModuleSystem/YieldSubject.h"
//#include "ClusterSocket.h"



class ACE_Message_Block;

class GameSession;

namespace SkyNet{
	namespace Networking{
		class ComPacket;
	}
}
using namespace SkyNet::Networking;
/// Handler that can communicate over stream sockets.
//static uint32 sessions;
//static uint32 users;
enum StatisticsAction{
	PACKET_SENT		=	1,
	PACKET_RECEIVED	=	2,
	DATA_SENT		=	3,
	DATA_RECEIVED	=	4,
};
struct SocketStatistics
{
    int _inPps;
    int _outPps;
    int _inBps;
    int _outBps;

    int __inPps;
	int __outPps;
	int __inBps;
	int __outBps;

    int getInBps() const;
    int getInPps() const;
    int getOutBps() const;
    int getOutPps() const;
    void incInBps(int inBps);
    void incInPps(int inPps);
    void incOutBps(int outBps);
    void incOutPps(int outPps);
    void calculate();
    void reset();
};
enum eAuthResult {
	AUTH_RESULT_SUCCESS = 0x01,
	AUTH_RESULT_FAILED_WRONG_INFO = 0x02,
	AUTH_RESULT_FAILED_BANNED = 0x03,

};
class Trojan_Message_Block: public ACE_Message_Block{
	bool _encrypt;
public:
	Trojan_Message_Block(size_t size):ACE_Message_Block(size),_encrypt(false){};
	bool IsEncrypted(){ return _encrypt;};
	void Encrypt(){ _encrypt = true;};
};
typedef ACE_Svc_Handler<ACE_SSL_SOCK_STREAM, ACE_NULL_SYNCH> LobbyHandler;

namespace SkyNet{
	namespace Networking{
		class GameSocket : protected LobbyHandler, public YieldSubject
		{
			public:
				/// Declare some friends
				friend class ACE_Acceptor< SkyNet::Networking::GameSocket, ACE_SSL_SOCK_ACCEPTOR >;
				friend class GameSocketMgr;
				friend class ReactorRunnable;

				/// Declare the acceptor for this class
				typedef ACE_Acceptor< GameSocket, ACE_SSL_SOCK_ACCEPTOR > Acceptor;
				//typedef ACE_Connector< ClusterSocket, ACE_SSL_SOCK_ACCEPTOR > Connector;

				/// Mutex type used for various synchronizations.
				typedef ACE_Thread_Mutex LockType;
				typedef ACE_Guard<LockType> GuardType;

				/// Check if socket is closed.
				bool IsClosed (void) const;

				/// Close the socket.
				void CloseSocket (void);

				/// Get address of connected peer.
				const std::string& GetRemoteAddress (void) const;

				/// Send A packet on the socket, this function is reentrant.
				/// @param pct packet to send
				/// @return -1 of failure
				int SendPacket (const ComPacket& pct);

				bool IsSendQueueEmpty() { return msg_queue()->is_empty(); }
				/// Add reference to this object.
				long AddReference (void);

				/// Remove reference to this object.
				long RemoveReference (void);
				void setSession(GameSession* ses){m_Session = ses;}

				SocketStatistics* GetStatistics(){ return _stats; }
			protected:
				/// things called by ACE framework.
				GameSocket (void);
				virtual ~GameSocket (void);

				/// Called on open ,the void* is the acceptor.
				virtual int open (void *);

				/// Called on failures inside of the acceptor, don't call from your code.
				virtual int close (int);

				/// Called when we can read from the socket.
				virtual int handle_input (ACE_HANDLE = ACE_INVALID_HANDLE);

				/// Called when the socket can write.
				virtual int handle_output (ACE_HANDLE = ACE_INVALID_HANDLE);

				/// Called when connection is closed or error happens.
				virtual int handle_close (ACE_HANDLE = ACE_INVALID_HANDLE,
					ACE_Reactor_Mask = ACE_Event_Handler::ALL_EVENTS_MASK);

				/// Called by WorldSocketMgr/ReactorRunnable.
				int Update (void);
				void StatisticsCheck();
				int receive(void *buf, size_t len){
        			if(incomingEncryption)
        				return peer().recv(buf, len);
        			else
        				return peer().peer().recv(buf,len);
				}
			private:
				/// Helper functions for processing incoming data.
				int handle_input_header (void);
				int handle_input_payload (void);
				int handle_input_missing_data (void);
				int handle_authchallenge(void);
				/// Help functions to mark/unmark the socket for output.
				/// @param g the guard is for m_OutBufferLock, the function will release it
				int cancel_wakeup_output (GuardType& g);
				int schedule_wakeup_output (GuardType& g);

				/// Drain the queue if its not empty.
				int handle_output_queue (GuardType& g);

				/// process one incoming packet.
				/// @param new_pct received packet ,note that you need to delete it.
				int ProcessIncoming (ComPacket* new_pct);

				/// Called by ProcessIncoming() on CLIENT_AUTH_SESSION.
				int HandleEncryption(ComPacket &_packet);
				int HandleChat (ComPacket& recvPacket);

				/// Called by ProcessIncoming() on CMSG_PING.
				void HandlePing (ComPacket& recvPacket);
				void HandleFileHeader (ComPacket& recvPacket);
				void HandleFileData (ComPacket& recvPacket);
			private:
				vector<ComPacket*> m_recvFileData;
				SkyNet_File* m_recvFile;
				/// Time in which the last ping was received. Pinging should be used to calculate
				/// client -> server latency. Not yet implemented.
				ACE_Time_Value m_LastPingTime;

				/// Keep track of over-speed pings ,to prevent ping flood. Not implemented yet.
				uint32 m_OverSpeedPings;

				/// Address of the remote peer
				std::string m_Address;

				/// Class used for managing encryption of the headers
				//AuthCrypt m_Crypt;

				/// Mutex lock to protect m_Session
				LockType m_SessionLock;

				/// Session to which received packets are routed
				GameSession* m_SessionTemplate;
				GameSession* m_Session;

				ComPacket* m_RecvWPct;

				/// This block actually refers to m_RecvWPct contents,
				/// which allows easy and safe writing to it.
				/// It wont free memory when its deleted. m_RecvWPct takes care of freeing.
				ACE_Message_Block m_RecvPct;

				/// Fragment of the received header.
				ACE_Message_Block m_Header;

				/// Mutex for protecting output related data.
				LockType m_OutBufferLock;

				/// Buffer used for writing output.
				ACE_Message_Block *m_OutBuffer;

				/// Size of the m_OutBuffer.
				size_t m_OutBufferSize;

				/// True if the socket is registered with the reactor for output
				bool m_OutActive;
				bool isAuthed;
				SkyNet::Networking::ByteBuffer EncryptionKey;
				uint64 Xs;

				bool incomingEncryption;
				bool outgoingEncryption;

				SocketStatistics* _stats;

				int _timer;
				uint32 _tick;
		};
	}
}
#endif  /* _WORLDSOCKET_H */

/// @}
