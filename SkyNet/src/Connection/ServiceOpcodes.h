/*
 * ServiceOpcodes.h
 *
 *  Created on: Oct 12, 2011
 *      Author: dimitar
 */

#ifndef SERVICEOPCODES_H_
#define SERVICEOPCODES_H_

enum ServiceOpcodes
{
    S_OP_NULL_ACTION                = 0x0000,
    SCOP_PING                       = 0x0001,
    SSOP_PONG                       = 0x0002,
    SSOP_AUTH_REQUEST               = 0x0003,
    SCOP_AUTH_SESSION               = 0x0004,
    SSOP_AUTH_RESPONSE              = 0x0005,
    SSOP_SET_ENCRYPTION     		= 0x0006,
    SCOP_SET_ENCRYPTION   			= 0x0007,
    SCOP_SANDBOX_AUTH_SESSION		= 0x0008,
    SSOP_DISCONNECT					= 0x0009,
    SCOP_LIST_ROOMS					= 0x000A,
    SSOP_LIST_ROOMS					= 0x000B,
    SCOP_CREATE_ROOM				= 0x000C,
    SSOP_CREATE_ROOM_RESPONSE 		= 0x000D,
    SCOP_JOIN_ROOM					= 0x000E,
    SSOP_JOIN_ROOM_RESPONSE 		= 0x000F,
    SCOP_BROADCAST_PACKET			= 0x0010, // TODO: Implement packet broadcast to players in a room.
    SSOP_BROADCAST_PACKET			= 0x0011,
	SSOP_REGISTRATION				= 0x0012,
	OP_FILE_HEADER					= 0x0013,
	OP_FILE_DATA					= 0x0014,

};


#endif /* SERVICEOPCODES_H_ */
