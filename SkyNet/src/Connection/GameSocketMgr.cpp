/*
 * GameSocketMgr.cpp
 *
 *  Created on: Oct 12, 2011
 *      Author: dimitar
 */


#include "GameSocketMgr.h"
#include "common.h"

#include <ace/ACE.h>
#include <ace/Log_Msg.h>
#include <ace/Reactor.h>
#include <ace/Reactor_Impl.h>
#include <ace/TP_Reactor.h>
#include <ace/Dev_Poll_Reactor.h>
#include <ace/Guard_T.h>
#include <ace/Atomic_Op.h>
#include <ace/os_include/arpa/os_inet.h>
#include <ace/os_include/netinet/os_tcp.h>
#include <ace/os_include/sys/os_types.h>
#include <ace/os_include/sys/os_socket.h>
#include "GameCore/Global.h"
#include <set>
#include "ModuleSystem/ModuleMgr.h"
#include "defines.h"
#include "Config/Config.h"
/**
* This is a helper class to LobbySocketMgr ,that manages
* network threads, and assigning connections from acceptor thread
* to other network threads
*/

namespace SkyNet{
	namespace Networking{
		class ReactorRunnable : protected ACE_Task_Base
		{
			public:

				ReactorRunnable() :
					m_Reactor(0),
					m_Connections(0),
					m_ThreadId(-1)
				{
					ACE_Reactor_Impl* imp = 0;

					#if defined (ACE_HAS_EVENT_POLL) || defined (ACE_HAS_DEV_POLL)

					imp = new ACE_Dev_Poll_Reactor();

					imp->max_notify_iterations (128);
					imp->restart (1);

					#else

					imp = new ACE_TP_Reactor();
					imp->max_notify_iterations (128);

					#endif

					m_Reactor = new ACE_Reactor (imp, 1);
				}

				virtual ~ReactorRunnable()
				{
					Stop();
					Wait();

					delete m_Reactor;
				}

				void Stop()
				{
					m_Reactor->end_reactor_event_loop();
				}

				int Start()
				{
					if (m_ThreadId != -1)
						return -1;

					return (m_ThreadId = activate());
				}

				void Wait() { ACE_Task_Base::wait(); }

				long Connections()
				{
					return static_cast<long> (m_Connections.value());
				}

				int AddSocket (GameSocket* sock)
				{
					ACE_GUARD_RETURN (ACE_Thread_Mutex, Guard, m_NewSockets_Lock, -1);

					++m_Connections;
					sock->AddReference();
					sock->reactor (m_Reactor);
					m_NewSockets.insert (sock);

					return 0;
				}

				ACE_Reactor* GetReactor()
				{
					return m_Reactor;
				}

			protected:

				void AddNewSockets()
				{
					ACE_GUARD (ACE_Thread_Mutex, Guard, m_NewSockets_Lock);

					if (m_NewSockets.empty())
						return;

					for (SocketSet::const_iterator i = m_NewSockets.begin(); i != m_NewSockets.end(); ++i)
					{
            			GameSocket* sock = (*i);

						if (sock->IsClosed())
						{
							sock->RemoveReference();
							--m_Connections;
						}
						else
							m_Sockets.insert (sock);
					}

					m_NewSockets.clear();
				}

				virtual int svc()
				{
					//sLog->outNormal ("Network Thread Starting");

					ACE_ASSERT (m_Reactor);

					SocketSet::iterator i, t;

					while (!m_Reactor->reactor_event_loop_done())
					{
						// dont be too smart to move this outside the loop
						// the run_reactor_event_loop will modify interval
						ACE_Time_Value interval (0, 10000);

						if (m_Reactor->run_reactor_event_loop (interval) == -1)
							break;

						AddNewSockets();

						for (i = m_Sockets.begin(); i != m_Sockets.end();)
						{

							if ((*i)->Update() == -1)
							{
								t = i;
								++i;
								sLog->outDebug("svc() is closing a socket");
								(*t)->CloseSocket();

								(*t)->RemoveReference();

								--m_Connections;
								m_Sockets.erase (t);
							}
							else
								++i;
						}
					}

					sLog->outDebug ("Network Thread Exitting");

					return 0;
				}

			private:
				typedef ACE_Atomic_Op<ACE_SYNCH_MUTEX, long> AtomicInt;
				typedef std::set<GameSocket*> SocketSet;

				ACE_Reactor* m_Reactor;
				AtomicInt m_Connections;
				int m_ThreadId;

				SocketSet m_Sockets;

				SocketSet m_NewSockets;
				ACE_Thread_Mutex m_NewSockets_Lock;
		};
	}
}
using namespace SkyNet::Networking;
SkyNet::Networking::GameSocketMgr::GameSocketMgr() :
    m_NetThreads(0),
    m_NetThreadsCount(0),
    m_SockOutKBuff(-1),
    m_SockOutUBuff(65536),
    m_UseNoDelay(true),
    m_Acceptor (0)
{
    ACE::set_handle_limit(-1);
}

SkyNet::Networking::GameSocketMgr::~GameSocketMgr()
{
    delete [] m_NetThreads;
    delete m_Acceptor;
}

int
SkyNet::Networking::GameSocketMgr::StartReactiveIO (vector<AcceptorData> acceptors)
{
    m_UseNoDelay = true;

    int num_threads = sConfig->GetIntDefault("Networking.WorkerThreads", 1);;
    if (num_threads <= 0)
    {
        sLog->outError ("Network.Threads is wrong in your config file");
        return -1;
    }

    m_acceptors = acceptors.size();
    m_NetThreadsCount = static_cast<size_t> (num_threads + m_acceptors ); // +1 acceptor +1 master cluster server

    m_NetThreads = new ReactorRunnable[m_NetThreadsCount];

    sLog->outNormalInLine ("\nInitializing Networking - ");

    // -1 means use default
    m_SockOutKBuff = -1;

    m_SockOutUBuff = 65536;

    if (m_SockOutUBuff <= 0)
    {
        sLog->outError ("Network.OutUBuff is wrong in your config file");
        return -1;
    }
    for(size_t i = 0; i < acceptors.size(); i++){
        const char* address = acceptors[i].listenerIp.c_str();
        ACE_UINT16 port = acceptors[i].listenerPort;
        GameSocket::Acceptor *acc = new GameSocket::Acceptor;
        m_Acceptor = acc;

        ACE_INET_Addr listen_addr (port, address);

        if (acc->open(listen_addr, m_NetThreads[i].GetReactor(), ACE_NONBLOCK) == -1)
        {
            sLog->SetColor (RED);
            sLog->outNormal ("Failed");
			sLog->ResetColor();
            sLog->outError ("Error: Failed to open acceptor, check if the port is free");
            return -1;
        }
    }

    sLog->SetColor (LGREEN);
    sLog->outNormal ("OK");
	sLog->ResetColor();
    sLog->outNormal ("Max allowed socket connections %d", ACE::max_handles());

    for (size_t i = 0; i < m_NetThreadsCount; ++i)
        m_NetThreads[i].Start();

    sLog->outNormal("Total network threads: %d", m_NetThreadsCount);
    sLog->outNormal("Acceptors: %d", m_acceptors);
    sLog->outNormal("Workers: %d", num_threads);

    return 0;
}

int
SkyNet::Networking::GameSocketMgr::StartNetwork (vector<AcceptorData> acceptors)
{

    if (StartReactiveIO(acceptors) == -1)
        return -1;

    return 0;
}

void
SkyNet::Networking::GameSocketMgr::StopNetwork()
{
    if (m_Acceptor)
    {
    	GameSocket::Acceptor* acc = dynamic_cast<GameSocket::Acceptor*> (m_Acceptor);

        if (acc)
            acc->close();
    }

    if (m_NetThreadsCount != 0)
    {
        for (size_t i = 0; i < m_NetThreadsCount; ++i)
            m_NetThreads[i].Stop();
    }

    Wait();

}

void
SkyNet::Networking::GameSocketMgr::Wait()
{
    if (m_NetThreadsCount != 0)
    {
        for (size_t i = 0; i < m_NetThreadsCount; ++i)
            m_NetThreads[i].Wait();
    }
}

int
SkyNet::Networking::GameSocketMgr::OnSocketOpenHook (GameSocket* sock)
{
    // set some options here
    if (m_SockOutKBuff >= 0)
    {
        if (sock->peer().set_option (SOL_SOCKET,
            SO_SNDBUF,
            (void*) & m_SockOutKBuff,
            sizeof (int)) == -1 && errno != ENOTSUP)
        {
            sLog->outError ("LobbySocketMgr::OnSocketOpen set_option SO_SNDBUF");
            return -1;
        }
    }

    static const int ndoption = 1;

    // Set TCP_NODELAY.
    if (m_UseNoDelay)
    {
        if (sock->peer().set_option (ACE_IPPROTO_TCP,
            TCP_NODELAY,
            (void*)&ndoption,
            sizeof (int)) == -1)
        {
            sLog->outError ("LobbySocketMgr::OnSocketOpen: peer().set_option TCP_NODELAY errno = %s", ACE_OS::strerror (errno));
            return -1;
        }
    }

    sock->m_OutBufferSize = static_cast<size_t> (m_SockOutUBuff);

    // we skip the Acceptor Thread
    size_t min = m_acceptors;

    ACE_ASSERT (m_NetThreadsCount >= 1);

    //OnSocketOpen(sock);
    //Yield(NETWORKING, SOCKET_OPEN, sock);
    for (size_t i = min; i < m_NetThreadsCount; ++i)
        if (m_NetThreads[i].Connections() < m_NetThreads[min].Connections())
            min = i;

    //sLog->outDebug("Assigning connection to thread %d", min);
    return m_NetThreads[min].AddSocket (sock);
}

void SkyNet::Networking::GameSocketMgr::Register(){

}

