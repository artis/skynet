/*
 * GameSocketMgr.h
 *
 *  Created on: Oct 12, 2011
 *      Author: dimitar
 */

#ifndef GAMESOCKETMGR_H_
#define GAMESOCKETMGR_H_

#include "GameSocket.h"
#include "GameCore/GameCore.h"
#include <ace/Basic_Types.h>
#include <ace/Singleton.h>
#include <ace/Thread_Mutex.h>


namespace SkyNet{
	namespace Networking{
		class ReactorRunnable;
	}
}
class ACE_Event_Handler;

struct AcceptorData{
    AcceptorData(string listener, uint port){
        listenerIp = listener;
        listenerPort = port;
    }
    string listenerIp;
    uint listenerPort;

};

namespace SkyNet{
	namespace Networking{
		class GameSocket;
/// Manages all sockets connected to peers and network threads
		class GameSocketMgr
		{
		public:
		  friend class GameSocket;
		  GameSocketMgr();
		  virtual ~GameSocketMgr();
		  /// Start network, listen at address:port .
		  int StartNetwork (vector<AcceptorData> acceptors);

		  /// Stops all network threads, It will wait for all running threads .
		  void StopNetwork();

		  /// Wait untill all network threads have "joined" .
		  void Wait();

		  /// Make this class singleton .

		  virtual void OnSocketOpen(GameSocket* sock){};
		protected:
		  int OnSocketOpenHook(GameSocket* sock);

		  int StartReactiveIO(vector<AcceptorData> acceptors);

		  virtual void Register();
		protected:


		  ReactorRunnable* m_NetThreads;
		  size_t m_NetThreadsCount;

		  int m_SockOutKBuff;
		  int m_SockOutUBuff;
		  bool m_UseNoDelay;

		  ACE_UINT16 m_acceptors;
		  ACE_Event_Handler* m_Acceptor;
		};
	}
}

#define sSocketMgr ACE_Singleton<GameSocketMgr, ACE_Null_Mutex>::instance()

#endif /* GAMESOCKETMGR_H_ */
