/*
 * Policy_Handler.cpp
 *
 *  Created on: Sep 6, 2011
 *      Author: dimitar
 */

#include "Policy_Handler.h"

Policy_Handler::Policy_Handler() {}

Policy_Handler::~Policy_Handler() {}

int Policy_Handler::open(void *){

	ACE_INET_Addr addr;

	//Checking if the connection is valid.
	if (this->peer ().get_remote_addr (addr) == -1)
	return -1;

	/*
	Acceptor<> won't register us in it's reactor, so we have to do it ourselves.
	Using READ_MASK will make the reactor call  handle_input()
	when the client perform some action with the socket.
	*/
	if (ACE_Reactor::instance()->register_handler (this, ACE_Event_Handler::READ_MASK) == -1)
		ACE_ERROR_RETURN ((LM_ERROR, "(%P|%t) can't register with reactor\n"),-1);

	else if (ACE_Reactor::instance()->schedule_timer (this,	0,ACE_Time_Value (5000),
			ACE_Time_Value (5000)) == -1)
		ACE_ERROR_RETURN ((LM_ERROR, "can'(%P|%t) t register with reactor\n"),-1);

	//sLog->outDebug("SecurityPolicyHandler: Incomming connection from %s", addr.get_host_addr());


	return 0;
}

void Policy_Handler::destroy(void){
    /* Remove ourselves from the reactor */
	  ACE_Reactor::instance()->remove_handler
      (this,
       ACE_Event_Handler::READ_MASK | ACE_Event_Handler::DONT_CALL);

    /* Cancel that timer we scheduled in open() */
	  ACE_Reactor::instance()->cancel_timer (this);

    /* Shut down the connection to the client.  */
    this->peer ().close ();

    /* Free our memory.  */
    delete this;
}

int Policy_Handler::close (u_long flags)
  {
    //Marking the flags as unused.
    ACE_UNUSED_ARG (flags);

    /*
      Clean up and go away.
      */
    this->destroy ();
    return 0;
  }
int Policy_Handler::handle_input (ACE_HANDLE)
	{
		char buf[128];
		ACE_OS::memset (buf, 0, sizeof (buf));

		ssize_t retval;
		switch (retval = this->peer().recv ((char *)&buf[0], BUFSIZ)){
			case -1:
			case 0:
				return -1;

			default:

				std::string policy="<?xml version='1.0'?><cross-domain-policy><allow-access-from domain='*' to-ports='1024-12200'/></cross-domain-policy>";

				ACE_INET_Addr addr;
				peer().get_remote_addr(addr);

				std::string policy_request("<policy-file-request/>");
				if(buf == policy_request){
					SkyNet::Networking::ByteBuffer packets;
					packets << policy;
					peer().send((char const*)packets.contents(), packets.size());
				}else{
					cout << "Invalid input" << endl;
				}

		}

    return 0;
  }

int Policy_Handler::handle_timeout (const ACE_Time_Value &tv,
                              const void *arg)
  {
	ACE_UNUSED_ARG(tv);
    ACE_UNUSED_ARG(arg);

   // this->peer().close();
   // this->close();

    return 0;
  }

int Policy_Handler::handle_close (ACE_HANDLE, ACE_Reactor_Mask)
  {
    this->destroy ();
    return 0;
  }
