/*
 * SecurityPolicyThread.h
 *
 *  Created on: Sep 6, 2011
 *      Author: dimitar
 */

#ifndef SECURITYPOLICYTHREAD_H_
#define SECURITYPOLICYTHREAD_H_

#include "Policy_Handler.h"
#include "Threading/Threading.h"

/// Main Lobby thread. Updates everything ( sessions, chat channels etc.)
class SecurityPolicyThread : public SkyNet::Threading::Runnable
{
    public:
        void run(){
        	std::string bind_ip = "0.0.0.0";
        	Policy_Acceptor policy_acceptor;
        	ACE_INET_Addr policy_addr(policy_port, bind_ip.c_str());
        	if (policy_acceptor.open(policy_addr, ACE_Reactor::instance(), ACE_NONBLOCK)
        				== -1) {
        		sLog->outError("Unable to start Security Policy thread. Looks like another server on this host has Security Policy handler running.");
        	}
        	sLog->outNormal("Started security policy");

        	while (true) {
				ACE_Time_Value interval(0, 100000);
				if (ACE_Reactor::instance()->run_reactor_event_loop(interval) == -1)
					break;
			}
        }
};

#endif /* SECURITYPOLICYTHREAD_H_ */
