/*
 * Policy_Handler.h
 *
 *  Created on: Sep 6, 2011
 *      Author: dimitar
 */

#ifndef POLICY_HANDLER_H_
#define POLICY_HANDLER_H_
#include <ace/INET_Addr.h>
#include <ace/Dev_Poll_Reactor.h>
#include <ace/TP_Reactor.h>
#include <ace/ACE.h>
#include <ace/Sig_Handler.h>

#include "ace/Acceptor.h"
#include "ace/SOCK_Acceptor.h"
#include "ace/Reactor.h"
#include <ace/Signal.h>
#include "common.h"
#include "ByteBuffer.h"
#include <ace/SOCK_Stream.h>
#include <ace/Reactor.h>
#include <iostream>
#include <string>
#include "common.h"

class Policy_Handler: public ACE_Svc_Handler <ACE_SOCK_STREAM, ACE_NULL_SYNCH> {
public:
	Policy_Handler();
	virtual ~Policy_Handler();
	virtual int open (void *);
	virtual void destroy (void);
	virtual int close (u_long flags = 0);

protected:
	virtual int handle_input (ACE_HANDLE);
	virtual int handle_timeout (const ACE_Time_Value &tv, const void *arg);
	virtual int handle_close (ACE_HANDLE, ACE_Reactor_Mask);
};
typedef ACE_Acceptor<Policy_Handler, ACE_SOCK_ACCEPTOR> Policy_Acceptor;
#define policy_port  2710
#endif /* POLICY_HANDLER_H_ */
