#ifndef COMPACKET_H
#define COMPACKET_H
//#include <boost/filesystem.hpp>
#include "ByteBuffer.h"
#include "ModuleSystem/Yieldable.h"
#include <fstream>
#include "Connection/ServiceOpcodes.h"
#include <openssl/md5.h>
#include "Exceptions/SkyNetException.h"


enum PacketFlags{
	SERVICE_LAYER_PACKET	=	1,
	ENCRYPTED_PACKET		=	2,
	COMPRESSED_PACKET		=	4, // TODO: Implement Packet Compression
	PRIORITY_PACKET			=	8, // TODO: Implement Priority Packets
	CLUSTERED_PACKET		=	16 // TODO: Implement Clusterization
};
namespace SkyNet{
	namespace Networking{
		//class ComPacket;
		using namespace ModuleSystem;
		
		class ComPacket : public SkyNet::Networking::ByteBuffer, public Yieldable
		{
			public:
																	// just container for later use
			ComPacket()                                       : ByteBuffer(0), m_opcode(0)
				{
					setTypeId(YIELD_OBJECT_DATA_PACKET);
				}
				explicit ComPacket(uint16 opcode, size_t res=200, uint16 flags=0) : ByteBuffer(res), m_opcode(opcode), m_flags(flags) {
        			setTypeId(YIELD_OBJECT_DATA_PACKET);
				}
																	// copy constructor
				ComPacket(const ComPacket &packet)              : ByteBuffer(packet), m_opcode(packet.m_opcode)
				{
        			setTypeId(YIELD_OBJECT_DATA_PACKET);
				}

				void Initialize(uint16 opcode, size_t newres=200)
				{
					clear();
					_storage.reserve(newres);
					m_opcode = opcode;
				}
				 void RC4(ByteBuffer key)
					{
					 if((this->size() == 0) || key.size() == 0)
						 return;
					 std::vector<uint8>* key_storage = key.GetStorage();

					 uint8* s = new  uint8[256];
					 uint8* k = new uint8[256];

						uint8 temp;
						int i, j;

						for (i = 0; i < 256; i++)
						{
							s[i] = (uint8)i;

							k[i] = (*key_storage)[i % key.size()];
						}

						j = 0;
						for (i = 0; i < 256; i++)
						{
							j = (j + s[i] + k[i]) % 256;
							temp = s[i];
							s[i] = s[j];
							s[j] = temp;
						}

						i = j = 0;
						for (int x = 0; x < (int)this->size(); x++)
						{
							i = (i + 1) % 256;
							j = (j + s[i]) % 256;
							temp = s[i];
							s[i] = s[j];
							s[j] = temp;
							int t = (s[i] + s[j]) % 256;
							_storage[x] ^= s[t];
						}
					}
				uint16 GetOpcode() const { return m_opcode; }
				void SetOpcode(uint16 opcode) { m_opcode = opcode; }

				uint16 GetFlags() const { return m_flags; }
				void SetFlags(uint16 flags) { m_flags = flags; }
				void SetFlag(uint16 flag){
					m_flags = m_flags | flag;

				}
				bool HasFlag(uint16 flag) const{
					return (m_flags & flag) != 0;
				}
				/*void append(const ComPacket& buffer)
				{
					if(buffer.wpos())
						ByteBuffer::append(buffer.contents(), buffer.wpos());
				}*/

			protected:
				uint16 m_opcode;
				uint16 m_flags;
		};
		class SkyNet_File: public Yieldable
		{
			unsigned long int r_ptr;
			unsigned long int w_ptr;
			unsigned int m_size;
			char* m_buff;
			string m_fileName;
			string m_filePath;
			unsigned int m_numPackets;
			string m_md5;
		public: 
			~SkyNet_File(){
				delete [] m_buff;
			}
			SkyNet_File(unsigned long int size = 1)
			{
				m_numPackets = 0;
				r_ptr = 0;
				w_ptr = 0;
				m_size = size;
				//sLog->outDebug("Alocating %d",size);
				m_buff = new char[size];
				m_numPackets = (int)ceil( ( (double)m_size) /4096);
			}
			SkyNet_File(std::string load_path, string d_filename = "", string d_path = "")
			{
				
				//sLog->outDebug("File %s",d_filename.c_str());
				m_numPackets = 0;

				m_fileName = d_filename;
				m_filePath = d_path;
				r_ptr = 0;
				w_ptr = 0;
				ifstream file (load_path.c_str(), ios::in|ios::binary);
				if (!file) { sLog->outDebug("Error opening file %s",load_path.c_str());throw SkyNetException("Unable to open file.", "File send skipped"); return; }
				
				file.seekg (0, ios::end);
				m_size = file.tellg();
				file.seekg(0,ios::beg);
				const long int buf_size = (const long int)m_size;
				if(buf_size == -1){
					sLog->outDebug("Bad file %s",m_fileName.c_str());
					int a;
					cin >> a;
				}
				//sLog->outDebug("Alocating %d",buf_size);
				m_buff = new char[buf_size];
				file.read(m_buff, buf_size); 
				file.close();
				m_md5 = GetMD5(load_path);
			}
			char* GetContents()
			{
				return m_buff;
			}
			string GetFilename(){ return m_fileName; }
			void SetFilename(string name){ m_fileName = name; }


			int GetSize(){
				return m_size;
			}

			void PutContents(char* contents, int count)
			{
				for(int i = 0; i < (count); i++){
					m_buff[w_ptr] = contents[i];
					w_ptr++;
				}
			}

			void Save(string dir, string filename)
			{
				//sLog->outDebug("File Save");
				if(m_md5 == GetMD5()){
					//sLog->outDebug("MD5 matches %s",GetMD5().c_str());
					//ComPacket test(1,1);
					//test << GetMD5();
					//test.hexlike();
				}
				else{
					sLog->outDebug("MD5 DOES NOT matches %s",GetMD5().c_str());
					return;
				}
				
				//sLog->outDebug("SAVING FILE!!!");
				//sLog->outDebug("Creating folder %s",GetDestinationPath().c_str());
				//
				//boost::filesystem::create_directories(GetDestinationPath().c_str());
				#ifdef WIN32
				CreateDirectory(dir.c_str(), NULL);
				#else
				mkdir(dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
				#endif
				string del = "/";
				string path = dir + del + filename;
				sLog->outDebug("Saving to %s",path.c_str());
				ofstream file_out (path.c_str(), ios::out|ios::binary);

				file_out.write(GetContents(), GetSize());
				file_out.close();


			}
			string GetMD5()
			{
				unsigned char c[MD5_DIGEST_LENGTH];
				
				int i;
				
				MD5_CTX mdContext;
				//int bytes;
				//unsigned char data[1024];

				MD5_Init (&mdContext);
				int step = 1024;
				unsigned int ptr = 0;
				while (ptr != m_size){
					if(m_size - ptr < 1024)
						step = m_size - ptr;

					MD5_Update (&mdContext, &m_buff[ptr], step);
					ptr += step;
				}
				MD5_Final (c,&mdContext);

				char md5[MD5_DIGEST_LENGTH+1];
				for(i = 0; i < MD5_DIGEST_LENGTH; i++){/*printf("%02x", c[i]);*/ md5[i] = c[i];}
				md5[MD5_DIGEST_LENGTH] = 0;
				string md5_str(md5);
				return md5_str;
			}
			string GetMD5(string path)
			{
				unsigned char c[MD5_DIGEST_LENGTH];
				
				int i;
				FILE *inFile = fopen (path.c_str(), "rb");
				MD5_CTX mdContext;
				int bytes;
				unsigned char data[1024];

				if (inFile == NULL) {
					printf ("%s can't be opened.\n", path.c_str());
					return 0;
				}

				MD5_Init (&mdContext);
				while ((bytes = fread (data, 1, 1024, inFile)) != 0)
					MD5_Update (&mdContext, data, bytes);
				MD5_Final (c,&mdContext);

				char md5[MD5_DIGEST_LENGTH+1];
				for(i = 0; i < MD5_DIGEST_LENGTH; i++){/*printf("%02x", c[i]);*/ md5[i] = c[i];}
				md5[MD5_DIGEST_LENGTH] = 0;
				string md5_str(md5);
				fclose(inFile);
				return md5_str;
			}
			void LoadFromPackets(vector<ComPacket> packets){
				//return;
				ComPacket header = packets[0];
				string save_name;
				w_ptr = 0;
				r_ptr = 0;
				//(*header).hexlike();
				//sLog->outDebug("Reading the file header");
				//sLog->outDebug("Reading the file name");
				header >> m_filePath;
				header >> save_name;
				//sLog->outDebug("Reading the file size");
				header >> m_size;
				header >> m_md5;

				delete [] m_buff;
				m_buff = new char[(const int)m_size];
				//int transfer_ptr = 0;
				//sLog->outDebug("There are %d packets to build a file", packets.size());
				for(int i = 1; i <= (int)ceil( ( (double)m_size) /4096); i++)
				{
					//sLog->outDebug("Reading file data");
					ComPacket payload = packets[i];
					//cout << "Reading packet " << i << " Size: " << payload.size() << endl;
					this->PutContents((char*)(&(*payload.GetStorage())[0]), payload.size());
			
				}
			}
			void LoadFromPackets(vector<ComPacket*> packets){
				
				//return;
				ComPacket* header = packets[0];
				string save_name;
				w_ptr = 0;
				r_ptr = 0;
				//(*header).hexlike();
				//sLog->outDebug("Reading the file header");
				//sLog->outDebug("Reading the file name");
				(*header) >> m_filePath;
				(*header) >> save_name;
				//sLog->outDebug("Reading the file size");
				(*header) >> m_size;
				(*header) >> m_md5;

				delete [] m_buff;
				m_buff = new char[(const int)m_size];
				//int transfer_ptr = 0;
				//sLog->outDebug("There are %d packets to build a file", packets.size());
				for(int i = 1; i <= (int)ceil( ( (double)m_size) /4096); i++)
				{
					//sLog->outDebug("Reading file data");
					ComPacket* payload = packets[i];
					//cout << "Reading packet " << i << " Size: " << payload->size() << endl;
					this->PutContents((char*)(&(*payload->GetStorage())[0]), payload->size());
			
				}
			}
			string GetDestinationPath() { return m_filePath; }
			unsigned int GetNumPackets(){ return m_numPackets; }
			vector<ComPacket*> SplitIntoPackets()
			{
				vector<ComPacket*> packets;

				ComPacket* init_packet = new ComPacket((uint16)OP_FILE_HEADER,50,1);
				(*init_packet) << m_filePath;
				(*init_packet) << m_fileName;
				(*init_packet) << (int)m_size;
				(*init_packet) << m_md5;

				packets.push_back(init_packet);

				unsigned int transfer_ptr = 0;
				m_numPackets = (unsigned int)ceil( ( (double)m_size) /4096);
				for(unsigned int i = 0; i < m_numPackets; i++)
				{
					//cout << "Building packet " << i << endl;
					ComPacket* payload = new ComPacket((uint16)OP_FILE_DATA,4096,1);
					
					int length = 0;
					if(m_size-transfer_ptr >= 4096)
						length = 4096;
					else
						length = m_size-transfer_ptr;
					payload->resize(length);
					for(int j = 0; j < length; j++)
					{
						//payload->append(&m_buff[transfer_ptr],length);
						(*payload->GetStorage())[j] = m_buff[transfer_ptr];
						transfer_ptr++;
						assert(transfer_ptr <= m_size);
					}
					packets.push_back(payload);

				}
				return packets;
			}

		};

	}
}
#endif
