#ifndef LOG_H
#define LOG_H


//#include "config.h"
#include <ace/Singleton.h>
#include "defines.h"
#include "common.h"
#include <stdarg.h>
#include <iostream>
#include <stdio.h>
#include <string>

enum LogLevel{
	LogLevelMinimum = 1, //Just Normal messages
	LogLevelNormal = 2,	 //Normal + Error messages
	LogLevelDebug = 3	 //Normal + Error + Debug messages

};
/*enum LogColor{
    NORMAL_COLOR  = 0,
    RED     = 31,
    GREEN   = 32,
};*/

enum LogColor
{
    BLACK,
    RED,
    GREEN,
    BROWN,
    BLUE,
    MAGENTA,
    CYAN,
    GREY,
    YELLOW,
    LRED,
    LGREEN,
    LBLUE,
    LMAGENTA,
    LCYAN,
    WHITE,
	MaxColors
};
enum SystemLogFilter{
	FilterModuleSystem		= 1,
	RoomSystem				= 2,
	ExternalProcedures		= 4,


};
class Log {
	friend class ACE_Singleton<Log, ACE_Thread_Mutex>;
	Log();
	~Log();

	public:
	void outProgress(int percent);
	void outProgress(int done, int total);
	void outDebug(const char * err, ...);
	void outDebug(uint32 filter, const char * err, ...);
	void outDebug(SystemLogFilter filter, const char * err, ...);
	void outDebugInLine(const char * err, ...);
	void outError(const char * err, ...);
	void outNormal(const char * err, ...);
	void outNormalInLine(const char * err, ...);
	void outCMD(const char * err, ...);
	void outSQLDriver(const char * err, ...);
	void outException(const char * err, ...);
	void setLogLevel(uint8 level){m_loglevel = level;};
	void outDB(const char * err,va_list ap_db);
	bool GetSQLDriverQueryLogging(){return false;}

    LogColor GetColor() const{ return m_color; }
	bool isSqlActive() const {
		return m_sqlActive;
	}

	void setSqlActive(bool sqlActive) {
		m_sqlActive = sqlActive;
	}

	void AddFilter(uint32 filter);
	void RemoveFilter(uint32 filter);
	bool HasFilter(uint32 filter);

	void AddSystemFilter(uint32 filter);
	void SetSystemFilter(uint32 filter);
	void RemoveSystemFilter(uint32 filter);
	bool HasSystemFilter(uint32 filter);

	static Log* GetInstance();
	void SetColor(LogColor color);
	void ResetColor();
private:
	uint8 m_loglevel;
	bool m_sqlActive;
	LogColor m_color;
	uint32 m_filter;
	uint32 m_systemFilter;
};

#define sLog Log::GetInstance()

#endif
