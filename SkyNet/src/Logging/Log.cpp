#include "Log.h"
#include <sstream>
#include "Database/DatabaseEnv.h"
#include <cmath>
#include <math.h>

Log::Log():m_sqlActive(false), m_color(WHITE), m_filter(0), m_systemFilter(0){
	m_loglevel = LogLevelDebug;
};
Log::~Log(){};
void Log::outDB(const char * err,va_list ap_db){
	if(!this->isSqlActive())
		return;

    char buffer[1512];
    vsprintf(buffer, err, ap_db);
    std::string log(buffer);
    SkyNetDatabase.PExecute("INSERT INTO `console` (`string`) VALUES ('%s')", log.c_str());

}
void Log::outProgress(int done, int total){

	outProgress(floor(((float)done/(float)total)*100));
}
void Log::outProgress(int percent){
	if(percent >= 100){
        cout << "\r";
        cout << "                                                                    ";
        cout << "\r";
        return;
	}

	cout << "\r Loading " << percent << "% \t[";

	float total_stars = 50;
	int stars = floor(percent/100.0f*total_stars);
	//cout << endl << endl << "percent " << percent << "stars " << stars << " total " << total_stars << endl<< endl;
	//sleep(5);
	for(int i = 1; i <= stars; i++)
		cout << "*";

	for(int o = 1; o <= total_stars-stars; o++)
		cout << " ";
	cout << "]";

	cout.flush();
}
void Log::outDebug(const char * err, ...){

	va_list ap_db;
	va_start(ap_db, err);
	outDB(err,ap_db);
    va_end(ap_db);

    va_list ap;
    /*for (int i = 31; i <= 37; i++)
    {
    std::cout << "\033[0;" << i << "mHello!\033[0m" << std::endl;
    std::cout << "\033[1;" << i << "mHello!\033[0m" << std::endl;
    }*/
    va_start(ap, err);


	SetColor(LGREEN);
    vfprintf(stdout, err, ap);
	ResetColor();
    printf("\n");
    cout << "\r";
    va_end(ap);
};
void Log::outDebug(SystemLogFilter filter, const char * err, ...)
{
	if(!HasSystemFilter(filter))
		return;

	va_list ap_db;
	va_start(ap_db, err);
	outDB(err,ap_db);
    va_end(ap_db);

    va_list ap;

    va_start(ap, err);


	SetColor(LGREEN);
    vfprintf(stdout, err, ap);

    printf("\n");
    cout << "\r";
    va_end(ap);
};
void Log::outException(const char * err, ...){

	va_list ap_db;
	va_start(ap_db, err);
	outDB(err,ap_db);
    va_end(ap_db);

    va_list ap;
    /*for (int i = 31; i <= 37; i++)
    {
    std::cout << "\033[0;" << i << "mHello!\033[0m" << std::endl;
    std::cout << "\033[1;" << i << "mHello!\033[0m" << std::endl;
    }*/
    va_start(ap, err);


#ifdef CONFIG_IS_ECLIPSE_BUILD
    printf("\r[DEBUG] ");
    vfprintf(stdout, err, ap);
#else
    std::cout << "\r\033[1;" << 32 << "m";

    vfprintf(stdout, err, ap);
    std::cout << "\033[0m";
#endif
    printf("\n");
    cout << "\rCommand> ";
    va_end(ap);
};
void Log::outDebugInLine(const char * err, ...){
	if(m_loglevel < LogLevelDebug)
		return;

    va_list ap;

    va_start(ap, err);

    vfprintf(stdout, err, ap);

    va_end(ap);
}
void Log::outNormal(const char * err, ...){

	va_list ap_db;
	va_start(ap_db, err);
	outDB(err,ap_db);
    va_end(ap_db);

	va_list ap;

    va_start(ap, err);

    if(GetColor() != WHITE){
        std::cout << "\033[1;" << GetColor() << "m";

        vfprintf(stdout, err, ap);
        std::cout << "\033[0m";
    }else
        vfprintf(stdout, err, ap);


    printf("\n\r");
    va_end(ap);
}

void Log::outNormalInLine(const char * err, ...){
	if(m_loglevel < LogLevelDebug)
		return;

    va_list ap;

    va_start(ap, err);

    if(GetColor() != WHITE){
        std::cout << "\033[1;" << GetColor() << "m";

        vfprintf(stdout, err, ap);
        std::cout << "\033[0m";
    }else
        vfprintf(stdout, err, ap);

    va_end(ap);
}



void Log::outError(const char * err, ...){
	if(m_loglevel < LogLevelNormal)
		return;

	va_list ap_db;
	va_start(ap_db, err);
	outDB(err,ap_db);
    va_end(ap_db);

    va_list ap;

    va_start(ap, err);
    SetColor(LGREEN);
    vfprintf(stdout, err, ap);
    
    printf("\n\r");

    va_end(ap);
}

void Log::outCMD(const char * err, ...){
	if(m_loglevel < LogLevelNormal)
		return;


    va_list ap;

    va_start(ap, err);
    vfprintf(stdout, err, ap);
    printf("\n\r");
    va_end(ap);
}
void Log::outSQLDriver(const char * err, ...)
{
	//return;
	if(m_loglevel < LogLevelNormal)
		return;

    va_list ap;

    va_start(ap, err);

    SetColor(LBLUE);
    vfprintf(stdout, err, ap);
    ResetColor();


    printf("\n");
    //cout << "\rCommand> ";
    va_end(ap);
}

Log* Log::GetInstance(){
	return ACE_Singleton<Log, ACE_Thread_Mutex>::instance();
}

void Log::AddFilter(uint32 filter)
{
	m_filter |= filter;
}

void Log::RemoveFilter(uint32 filter)
{
	m_filter &= ~filter;
}

bool Log::HasFilter(uint32 filter)
{
	return (m_filter | filter) == m_filter;
}


void Log::AddSystemFilter(uint32 filter)
{
	m_systemFilter |= filter;
}
void Log::SetSystemFilter(uint32 filter)
{
	m_systemFilter = filter;
}

void Log::RemoveSystemFilter(uint32 filter)
{
	m_systemFilter &= ~filter;
}

bool Log::HasSystemFilter(uint32 filter)
{
	return (m_systemFilter | filter) == m_systemFilter;
}

void Log::SetColor(LogColor color)
{
	bool stdout_stream = true;
    #if PLATFORM == PLATFORM_WINDOWS
    static WORD WinColorFG[MaxColors] =
    {
        0,                                                  // BLACK
        FOREGROUND_RED,                                     // RED
        FOREGROUND_GREEN,                                   // GREEN
        FOREGROUND_RED | FOREGROUND_GREEN,                  // BROWN
        FOREGROUND_BLUE,                                    // BLUE
        FOREGROUND_RED |                    FOREGROUND_BLUE, // MAGENTA
        FOREGROUND_GREEN | FOREGROUND_BLUE,                 // CYAN
        FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE, // WHITE
                                                            // YELLOW
        FOREGROUND_RED | FOREGROUND_GREEN |                   FOREGROUND_INTENSITY,
                                                            // RED_BOLD
        FOREGROUND_RED |                                      FOREGROUND_INTENSITY,
                                                            // GREEN_BOLD
        FOREGROUND_GREEN |                   FOREGROUND_INTENSITY,
        FOREGROUND_BLUE | FOREGROUND_INTENSITY,             // BLUE_BOLD
                                                            // MAGENTA_BOLD
        FOREGROUND_RED |                    FOREGROUND_BLUE | FOREGROUND_INTENSITY,
                                                            // CYAN_BOLD
        FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
                                                            // WHITE_BOLD
        FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY
    };

    HANDLE hConsole = GetStdHandle(stdout_stream ? STD_OUTPUT_HANDLE : STD_ERROR_HANDLE);
    SetConsoleTextAttribute(hConsole, WinColorFG[color]);
    #else
    enum ANSITextAttr
    {
        TA_NORMAL                                = 0,
        TA_BOLD                                  = 1,
        TA_BLINK                                 = 5,
        TA_REVERSE                               = 7
    };

    enum ANSIFgTextAttr
    {
        FG_BLACK                                 = 30,
        FG_RED,
        FG_GREEN,
        FG_BROWN,
        FG_BLUE,
        FG_MAGENTA,
        FG_CYAN,
        FG_WHITE,
        FG_YELLOW
    };

    enum ANSIBgTextAttr
    {
        BG_BLACK                                 = 40,
        BG_RED,
        BG_GREEN,
        BG_BROWN,
        BG_BLUE,
        BG_MAGENTA,
        BG_CYAN,
        BG_WHITE
    };

    static uint8 UnixColorFG[MaxColors] =
    {
        FG_BLACK,                                          // BLACK
        FG_RED,                                            // RED
        FG_GREEN,                                          // GREEN
        FG_BROWN,                                          // BROWN
        FG_BLUE,                                           // BLUE
        FG_MAGENTA,                                        // MAGENTA
        FG_CYAN,                                           // CYAN
        FG_WHITE,                                          // WHITE
        FG_YELLOW,                                         // YELLOW
        FG_RED,                                            // LRED
        FG_GREEN,                                          // LGREEN
        FG_BLUE,                                           // LBLUE
        FG_MAGENTA,                                        // LMAGENTA
        FG_CYAN,                                           // LCYAN
        FG_WHITE                                           // LWHITE
    };

    fprintf((stdout_stream? stdout : stderr), "\x1b[%d%sm", UnixColorFG[color], (color >= YELLOW && color < MaxColors ? ";1" : ""));
    #endif
}

void Log::ResetColor()
{
	bool stdout_stream = true;
    #if PLATFORM == PLATFORM_WINDOWS
    HANDLE hConsole = GetStdHandle(stdout_stream ? STD_OUTPUT_HANDLE : STD_ERROR_HANDLE);
    SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
    #else
    fprintf((stdout_stream ? stdout : stderr), "\x1b[0m");
    #endif
}