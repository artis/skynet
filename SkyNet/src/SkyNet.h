/*
 * SkyNet.h
 *
 *  Created on: Jun 13, 2012
 *      Author: dimitar
 */

#ifndef SKYNET_H_
#define SKYNET_H_
#pragma GCC diagnostic ignored "-Wunused-function"

#include "Threading/Threading.h"
#include "ModuleSystem/ModuleSystem.h"
#include "Coordinator/SkyNetCoordinator.h"
#include "Connection/GameSocket.h"
#include "GameCore/GameCore.h"
#include "Exceptions/SkyNetException.h"
#include "GameCore/Room.h"
#include "Config/Config.h"
#include "Container/LSDStorage.h"
#include "Container/TemplateHolder.h"
#include "ExternalProcedures/ExternalProcedureMgr.h"
#include "GameCore/PlayerRegistry.h"
#include <vector>
#include "Pooling/Pooling.h"

using namespace SkyNet::ModuleSystem;
namespace SkyNet{
	static void SpawnThread(Threading::Runnable* instace, Threading::Priority p){
		Threading::Thread worker(instace);
		worker.setPriority(p);
	}
	static void ScheduleModule(Module* module){
		sModuleMgr->Schedule(module);
	}

	

}
#endif /* SKYNET_H_ */
