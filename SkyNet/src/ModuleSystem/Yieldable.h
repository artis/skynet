/*
 * Yiedable.h
 *
 *  Created on: Jun 10, 2012
 *      Author: dimitar
 */

#ifndef YIEDABLE_H_
#define YIEDABLE_H_

#include <boost/signal.hpp>
#include <boost/bind.hpp>

namespace SkyNet{
	namespace ModuleSystem{
		enum YieldableObjectType{
			YIELD_OBJECT_DATA_PACKET	=	0x01,
			YIELD_OBJECT_GAME_SOCKET	=	0x02,

		};
		enum NetworkYield{
			SOCKET_OPEN			=	0x01,
			RECIEVE_FILE,
		};
		enum SessionYield{
			AUTHENTICATE	=	0x01,
			REGISTER		=	0x02,
		};
		enum YieldsTypes{
			YIELD_TYPE_DATA_PACKET			=	0x01,
			NETWORKING			=	0x02,
			OBJECT_VALUES_SYNCH	=	0x03,
			YIELD_TYPE_SESSION,
			EXTERNAL_PROCEDURE,
			CUSTOM_YIELD,
		};
		class Yieldable: public boost::signals::trackable {
		public:
			Yieldable();
			virtual ~Yieldable();
			int getTypeId() const;
			void setTypeId(int m_typeId);

			template <class T>
			T* ConvertTo(){
				return static_cast<T*>(this);
			}
		private:
			int m_typeId;

		};
	}
}
#endif /* YIEDABLE_H_ */
