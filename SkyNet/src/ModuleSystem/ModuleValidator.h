/*
 * ModuleValidator.h
 *
 *  Created on: Oct 25, 2012
 *      Author: dimitar
 */

#ifndef MODULEVALIDATOR_H_
#define MODULEVALIDATOR_H_

#include "common.h"
#include "defines.h"
#include <boost/signal.hpp>
#include <boost/bind.hpp>

class GameSession;

namespace SkyNet{
	namespace ModuleSystem{
		class Yieldable;
		class YieldSubject;

		class ModuleValidator: public boost::signals::trackable {
		public:
			ModuleValidator();
			virtual ~ModuleValidator();
		};
	}
}

#endif /* MODULEVALIDATOR_H_ */
