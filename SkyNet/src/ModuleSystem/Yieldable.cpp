/*
 * Yiedable.cpp
 *
 *  Created on: Jun 10, 2012
 *      Author: dimitar
 */

#include "Yieldable.h"

using namespace SkyNet::ModuleSystem;
Yieldable::Yieldable() { }

Yieldable::~Yieldable() {}

int Yieldable::getTypeId() const
{
    return m_typeId;
}

void Yieldable::setTypeId(int m_typeId)
{
    this->m_typeId = m_typeId;
}


