/*
 * Module.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: dimitar
 */

#include "Module.h"
#include "ModuleMgr.h"
#include "GameCore/BasePlayer.h"

using namespace SkyNet::ModuleSystem;

Module::Module(): m_player(NULL) {

}

Module::~Module() {
}
void Module::ReceiveYield(int YieldId, Yieldable* data, YieldSubject* subject){

}

bool Module::Update(uint32 diff){ return true;};

void Module::RequestAccessOpcodeHandle(uint32 opcode){

}

void Module::SetPlayerMember(BasePlayer* player){
    m_player = player;
}
BasePlayer* Module::GetPlayer(){
    return m_player;
}
