/*
 * Module.h
 *
 *  Created on: Apr 10, 2012
 *      Author: dimitar
 */

#ifndef MODULE_H_
#define MODULE_H_

#include "common.h"
#include "defines.h"
#include <boost/signal.hpp>
#include <boost/bind.hpp>

class GameSession;


namespace SkyNet{
	namespace Core{
		class BasePlayer;
	}
	namespace Networking{
		class ComPacket;
	}
	namespace ModuleSystem{
		using namespace Networking;
		using namespace Core;
	    enum ModuleOptions{
            MODULE_OPTION_PLAYER_MEMBER     = 1
	    };
		class Yieldable;
		class YieldSubject;
		class Module: public boost::signals::trackable{
		public:
			Module();

			virtual ~Module();
			//!Called every update tick (default: 50ms)
			/**
			  * \param diff indicates the real time elapsed since last update in ms.
			 @note This method will be called only if the module has been scheduled for update.
			*/
			virtual bool Update(uint32 diff);
			virtual void ProcessPacket(ComPacket* packet, GameSession* session){};

			virtual void ReceiveYield(int YieldId, Yieldable* data, YieldSubject* subject);
			//template <class T>
			//virtual void ReceiveYield(int YieldId, int data, T& subject){}

			virtual void ReceiveSignal( int YieldId, Yieldable* data, YieldSubject* subject) {};
			virtual void ReceiveYield(int YieldId, int data, YieldSubject* subject){};
			virtual void ReceiveYield(int YieldId, uint8 data, YieldSubject* subject){};
			virtual void ReceiveYield(int YieldId, uint16 data, YieldSubject* subject){};
			virtual void ReceiveYield(int YieldId, uint32 data, YieldSubject* subject){};
			virtual void ReceiveYield(int YieldId, uint64 data, YieldSubject* subject){};
			virtual void ReceiveYield(int YieldId, int8 data, YieldSubject* subject){};
			virtual void ReceiveYield(int YieldId, int16 data, YieldSubject* subject){};
			virtual void ReceiveYield(int YieldId, int64 data, YieldSubject* subject){};


            virtual Module* Clone() = 0;

			// Module Access
			void RequestAccessOpcodeHandle(uint32 opcode);

			bool isActive() const{ return m_active;}
			void Disable(){ m_active = false; }
			void Enable(){ m_active = true; }

			void Lock(){ /* TODO Lock function for Module class. */ };
			void UnLock(){ /* TODO UnLock function for Module class. */ };
			/*bool HasOption(uint8 value){
                return (m_options & value) == value;
			}
			void SetOption(uint8 value){
                m_options |= value;
			}
			void RemoveOption(uint8 value){
                m_options = m_options & ~value;
			}*/
			void SetID(uint id){
                m_id = id;
			}
			uint GetID() const{
                return m_id;
			}
			void SetPlayerMember(BasePlayer* player);
			BasePlayer* GetPlayer();
		private:

			bool m_active;
			uint8 m_options;
            uint m_id;
            BasePlayer* m_player;

		};
	}
}

#endif /* MODULE_H_ */
