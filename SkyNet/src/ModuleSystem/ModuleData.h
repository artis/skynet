/*
 * ModuleData.h
 *
 *  Created on: Apr 12, 2012
 *      Author: dimitar
 */

#ifndef MODULEDATA_H_
#define MODULEDATA_H_
#include "defines.h"
#include "common.h"

enum ModuleDataTypeID{
	COM_PACKET		= 0x00,

};

class ModuleData {
public:
	ModuleData();
	virtual ~ModuleData();

	uint8 GetTypeId(){ return m_typeID; }

	template <class T>
	T* ConvertTo (){ return reinterpret_cast<T*>(this);	}

private:
	uint8 m_typeID;
};

#endif /* MODULEDATA_H_ */
