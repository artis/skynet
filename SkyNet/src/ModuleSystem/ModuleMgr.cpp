/*
 * ModuleMgr.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: dimitar
 */

#include "ModuleMgr.h"
#include "ModuleData.h"
#include "ComPacket.h"
#include "GameCore/BasePlayer.h"

using namespace SkyNet::ModuleSystem;
ModuleMgr::ModuleMgr() 
{ 
}
ModuleMgr::~ModuleMgr() {
    for (uint i = 0; i < m_modules.size(); i++)
		delete m_modules[i];

    for (uint i = 0; i < m_validators.size(); i++)
		delete m_validators[i];

    for(uint i = 0; i < m_callback.size(); i++){
        if(m_callback[i]){
            for(uint j = 0; j < (*m_callback[i]).size(); j++){
                if((*m_callback[i])[j]){
                    for(uint k = 0; k < (*m_callback[i])[j]->size(); k++){
                        delete (*(*m_callback[i])[j])[k];
                    }
                    delete (*m_callback[i])[j];
                }
            }
            delete m_callback[i];
        }
    }

    for (uint i = 0; i < m_validatorCallback.size(); i++)
        for(uint j = 0; j < m_validatorCallback[i].size(); j++){
            if(m_validatorCallback[i][j])
                delete m_validatorCallback[i][j];
        }

}


void ModuleMgr::Schedule(Module* module)
{
	m_scheduledModules.push_back(module);
}
void ModuleMgr::Subscribe(uint id,int YieldType, int YieldId, Module* handler, ModuleCallBack* c )
{

    if(handler->GetPlayer() && !is_player){
		sLog->outDebug(FilterModuleSystem, "Module ptr %p subscribing player %d as member. YieldType = %d YieldId = %d", handler, handler->GetPlayer()->GetID(), YieldType, YieldId);
        handler->GetPlayer()->Subscribe(id,YieldType, YieldId, handler, c);
        return;
    }

    m_modules.push_back(handler);
    
	uint64 index = (uint64)YieldType | ((uint64)YieldId << 32);

	std::map<uint64, vector<ModuleCallBack*>* >::iterator loc = m_callbackMap.find(index);
	if(loc != m_callbackMap.end())
		loc->second->push_back(c);
	else{
		 vector<ModuleCallBack*>* vec = new  vector<ModuleCallBack*>();
		 vec->push_back(c);
		 m_callbackMap[index] = vec;
	}

	/*if(m_callback.capacity() < (uint)YieldType)
        m_callback.resize(YieldType+1, NULL);

    if(!m_callback[YieldType])
        m_callback[YieldType] = new vector<vector<ModuleCallBack*>* >;

    if(m_callback[YieldType]->capacity() < (uint)YieldId)
        m_callback[YieldType]->resize(YieldId+1, NULL);

    if((*m_callback[YieldType])[YieldId] == NULL)
        (*m_callback[YieldType])[YieldId] = new vector<ModuleCallBack*>;

    (*m_callback[YieldType])[YieldId]->push_back(c);
	*/


}
void ModuleMgr::Dispatch(unsigned int YieldType, unsigned int YieldId, Yieldable* data, YieldSubject* subject, BasePlayer* player)
{
	uint64 index = (uint64)YieldType | ((uint64)YieldId << 32);

	sLog->outDebug(FilterModuleSystem, "ModuleMgr::Dispatch()");

	if(player)
		player->Dispatch(YieldType, YieldId, data, subject);

	bool validated = true;
	if(m_validatorCallback.size() > (unsigned)YieldId){
		//sLog->outDebug("Something.");
		int num = m_validatorCallback[YieldId].size();

		for(int i = 0; i < num; i++){
			ModuleValidatorCallBack* f = m_validatorCallback[YieldId][i];

			//sLog->outDebug("Calling validator.");
			if(!(*f)(data, subject)){
				validated=false;
				//sLog->outDebug("The validator returned false.");
				break;
			}
			//sLog->outDebug("The validator returned true.");
		}

	}
	if(!validated)
		return;


	std::map<uint64, vector<ModuleCallBack*>* >::iterator loc = m_callbackMap.find(index);
	if(loc != m_callbackMap.end()){
		vector<ModuleCallBack*>* vec = loc->second;
		uint num = vec->size();
		for(uint i = 0; i < num; i++)
			(*(*vec)[i])(data, subject);
	
	}

	sLog->outDebug(FilterModuleSystem,"ModuleMgr::Dispatch(): A total of %d callbacks handled dispatch Type %d ID %d", YieldType, YieldId);

	return;

	/*
	if(m_callback.size() <= (unsigned)YieldType || m_callback[YieldType] == NULL){
		sLog->outDebug(FilterModuleSystem,"ModuleMgr::Dispatch(): There are no callbacks registered with any ID for this dispatch type (%d)", YieldType);
		return;
	}
	if(m_callback[YieldType]->size() <= (unsigned)YieldId || (*m_callback[YieldType])[YieldId] == NULL){
		sLog->outDebug(FilterModuleSystem,"ModuleMgr::Dispatch(): There are no callbacks for dispatch ID %d", YieldId);
		return;
	}


	int num = (*(*m_callback[YieldType])[YieldId]).size();
	for(int i = 0; i < num; i++)
		(*(*(*m_callback[YieldType])[YieldId])[i])(data, subject);
*/

}

void Dispatch(unsigned int YieldType, unsigned int YieldId, Yieldable* data, YieldSubject* subject, BasePlayer* player){
	sModuleMgr->Dispatch(YieldType, YieldId, data, subject, player);
}


void ModuleMgr::Update(uint32 diff)
{
	for (uint i = 0; i < m_scheduledModules.size(); i++)
		m_scheduledModules[i]->Update(diff);
}