/*
 * YieldSubject.h
 *
 *  Created on: Jun 10, 2012
 *      Author: dimitar
 */

#ifndef YIELDSUBJECT_H_
#define YIELDSUBJECT_H_
#include <boost/signal.hpp>
#include <boost/bind.hpp>
namespace SkyNet{
	namespace ModuleSystem{

		enum YieldSubjectID{
			Session			=	0x01,
		};
		class YieldSubject: public boost::signals::trackable {
		public:
			YieldSubject();
			virtual ~YieldSubject();
			int getTypeId() const;
			void setTypeId(int m_typeId);

			template <class T>
			T* ConvertTo(){
				return static_cast<T*>(this);
			}
		private:
			int m_typeId;

		};
	}
}
#endif /* YIELDSUBJECT_H_ */
