/*
 * ModulePtr.h
 *
 *  Created on: Jun 17, 2012
 *      Author: dimitar
 */

#ifndef MODULEPTR_H_
#define MODULEPTR_H_


namespace SkyNet{
	namespace ModuleSystem{
		class Module;
/*! \brief A chess piece
 * The abstract parent of all chess pieces. */
		class ModulePtr {
		public:
			ModulePtr(Module* instance);
			virtual ~ModulePtr();
			//! Overloads operator-> to make the object behave as pointer
			Module* operator->();
		private:
			Module* m_module;
		};
	}
}
#endif /* MODULEPTR_H_ */
