/*
 * ModuleMgr.h
 *
 *  Created on: Apr 10, 2012
 *      Author: dimitar
 */

#ifndef MODULEMGR_H_
#define MODULEMGR_H_

#include "Module.h"
#include "ModuleValidator.h"

#include "Yieldable.h"
#include "ComPacket.h"
#include <vector>
#include <map>
#include <set>
#include <boost/signal.hpp>
#include <boost/bind.hpp>


using namespace boost;
using namespace std;


namespace SkyNet{
	namespace Core{
		class BasePlayer;
	}
	namespace ModuleSystem{
	//Macro for easier ModuleCallBack creation


	typedef boost::function< void ( Yieldable*, YieldSubject*)  > ModuleCallBack;
	typedef boost::function< bool ( Yieldable*, YieldSubject*)  > ModuleValidatorCallBack;

#define CREATE_CALLBACK(callback)   new ModuleCallBack(boost::bind(callback,this,_1,_2))
#define CREATE_VALIDATOR_CALLBACK(callback)   new ModuleValidatorCallBack(boost::bind(callback,this,_1,_2))
	class BaseModule
	{
	public:
		virtual ~BaseModule(){};

		void SetStatus(int val){
			status = val;
		}
		virtual void ReceiveYield(){};

	    int status;
	};

		typedef multimap<uint64, Module*> ModuleMap;



		class ModuleData;

		class ModuleMgr {
			
		public:
			ModuleMgr();
			virtual ~ModuleMgr();
			
			void Schedule(Module* module);

			inline void Dispatch(unsigned int YieldType, unsigned int YieldId, Yieldable* data, YieldSubject* subject, BasePlayer* player = NULL);
			/// Subscribe for a Module for a given event./usr/bin/make -f "/home/dimitar/git/skynet/CodeBlox/SkyNet/Makefile"  VERBOSE=1 clean
			/// \param YieldType - Event type.
			/// \param YieldId - Event ID. (Unique in the Event type scope).
			void Subscribe(uint id,int YieldType, int YieldId, Module* handler, ModuleCallBack* c);
			/// Subscribe for a ModuleValidator for a given event.
			/// \param YieldType - Event type.
			/// \param YieldId - Event ID. (Unique in the Event type scope).
			void Subscribe(int YieldType, int YieldId, ModuleValidator* handler, ModuleValidatorCallBack* c )
			{
                m_validators.push_back(handler);
				sLog->outDebug("Validator subscribing.");
				if(m_validatorCallback.capacity() < (uint)YieldId)
					m_validatorCallback.resize(YieldId+1);

				m_validatorCallback[YieldId].push_back(c);
			}
			void Update(uint32 diff);
		protected:
			
			ModuleMap m_yieldHandlers;
			vector<Module*> m_scheduledModules;
			vector<Module*> m_modules;
			vector<ModuleValidator*> m_validators;
			UNORDERED_MAP<uint64, vector<vector<vector<ModuleCallBack> > > > m_playerCallbacks;
			boost::signal<void (int, Yieldable* , YieldSubject*)> SigA;
			vector<vector<Module*> > m_handles;
			vector<vector<vector<ModuleCallBack*>* >* > m_callback;

			std::map<uint64, vector<ModuleCallBack*>* > m_callbackMap;
			vector<vector<ModuleValidatorCallBack*> > m_validatorCallback;
			bool is_player;
		};

		class GlobalModuleMgr: public ModuleMgr{
			friend class ACE_Singleton<ModuleMgr, ACE_Null_Mutex>;
		public:
			GlobalModuleMgr(){ is_player = false; }
			
		};
	}
}
		#define sModuleMgr ACE_Singleton<SkyNet::ModuleSystem::GlobalModuleMgr, ACE_Null_Mutex>::instance()

/*      TODO: Delete these shit.

		void YieldSignal(int YieldType, int YieldId, Yieldable* data, YieldSubject* subject = NULL);
		void YieldVector(int YieldType, int YieldId, Yieldable* data, YieldSubject* subject);
		void YieldSmart(int YieldType, int YieldId, Yieldable* data, YieldSubject* subject);
*/
using namespace SkyNet::ModuleSystem;
using namespace SkyNet::Core;
void Dispatch(unsigned int YieldType, unsigned int YieldId, Yieldable* data, YieldSubject* subject, BasePlayer* player = NULL);

#endif /* MODULEMGR_H_ */
