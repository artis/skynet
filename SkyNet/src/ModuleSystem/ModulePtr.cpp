/*
 * ModulePtr.cpp
 *
 *  Created on: Jun 17, 2012
 *      Author: dimitar
 */

#include "ModulePtr.h"
#include "Module.h"

using namespace SkyNet::ModuleSystem;

ModulePtr::ModulePtr(Module* instance): m_module(instance) {
	m_module->Lock();
}

ModulePtr::~ModulePtr() {
	m_module->UnLock();
}

Module* ModulePtr::operator->(){
	return m_module;
}
