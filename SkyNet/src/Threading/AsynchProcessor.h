/*
 * AsynchProcessor.h
 *
 *  Created on: May 31, 2012
 *      Author: dimitar
 */

#ifndef ASYNCHPROCESSOR_H_
#define ASYNCHPROCESSOR_H_
#include "LockedQueue.h"

class __AsynchProcedure{

};
template <typename Handler>
class AsynchProcessor: public __AsynchProcedure {
public:
	class AsynchProcedure{
		AsynchProcedure(){	};
		virtual ~AsynchProcedure(){	};
		virtual void Run(){	};
		void (Handler::*callback)(AsynchProcessor<Handler>::AsynchProcedure* processor);
	};
	AsynchProcessor(){	};
	virtual ~AsynchProcessor(){	};
	void Queue(AsynchProcedure* procedure);
	void Update();

private:
	SkyNet::Threading::LockedQueue<AsynchProcedure*, ACE_Recursive_Thread_Mutex> m_queue;
};
//#define sAsyncMgr ACE_Singleton<AsynchProcessor, ACE_Null_Mutex>::instance()
#endif /* ASYNCHPROCESSOR_H_ */
