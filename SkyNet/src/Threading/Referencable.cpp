/*
 * Referencable.cpp
 *
 *  Created on: Nov 20, 2011
 *      Author: dimitar
 */

#include "Referencable.h"


Referencable::Referencable() {}

Referencable::~Referencable() {
	//sLog->outDebug("Invoking Referencable::~Referencable");

	ReferenceList::const_iterator itr;

	{	//critical section
		ACE_Write_Guard<ACE_RW_Mutex> guard(_refByMutex);

		for (itr = _refBy.begin(); itr != _refBy.end(); ++itr)
		{
			//sLog->outDebug("Notify reference loss");
			(*itr)->LooseReferenceTo(this);
			_refBy.erase((*itr));
		}
	}

	{	//critical section
		ACE_Write_Guard<ACE_RW_Mutex> guard(_refToMutex);
		for (itr = _refTo.begin(); itr != _refTo.end(); ++itr)
		{
			(*itr)->RemoveReferenceBy(this);
			_refTo.erase((*itr));
		}
	}


}

void Referencable::RegisterReferenceTo(Referencable* target){
	if(this->IsReferencingTo(target))
		return;

	ACE_Write_Guard<ACE_RW_Mutex> guard(_refToMutex);
	_refTo.insert(target);

	target->RegisterReferenceBy(this);
}

void Referencable::RegisterReferenceBy(Referencable* target){
	if(this->IsReferencedBy(target))
		return;

	ACE_Write_Guard<ACE_RW_Mutex> guard(_refByMutex);
	_refBy.insert(target);

}

bool Referencable::IsReferencingTo(Referencable* target){
	ACE_Read_Guard<ACE_RW_Mutex> guard(_refToMutex);
	ReferenceList::const_iterator itr;
	itr = _refTo.find(target);
	if(itr == _refTo.end())
		return false;

	return true;
}

bool Referencable::IsReferencedBy(Referencable* target){
	ACE_Read_Guard<ACE_RW_Mutex> guard(_refByMutex);
	ReferenceList::const_iterator itr;
	itr = _refBy.find(target);
	if(itr == _refBy.end())
		return false;

	return true;
}

void Referencable::RemoveReferenceTo(Referencable* target){
	if(!this->IsReferencingTo(target))
		return;
	ACE_Write_Guard<ACE_RW_Mutex> guard(_refToMutex);
	_refTo.erase(target);
	target->RemoveReferenceBy(this);
}
void Referencable::RemoveReferenceBy(Referencable* target){
	if(!this->IsReferencedBy(target))
		return;

	ACE_Write_Guard<ACE_RW_Mutex> guard(_refToMutex);
	_refBy.erase(target);
}
void Referencable::LooseReferenceTo(Referencable* target){
	if(!this->IsReferencingTo(target))
		return;

	{ //critical section
		ACE_Write_Guard<ACE_RW_Mutex> guard(_refToMutex);
		_refTo.erase(target);
	}


	this->OnLostReference(target);
}
