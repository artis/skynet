/*
 * Referencable.h
 *
 *  Created on: Nov 20, 2011
 *      Author: dimitar
 */

#ifndef REFERENCABLE_H_
#define REFERENCABLE_H_
#include <set>
#include "ace/RW_Mutex.h"
#include "Logging/Log.h"

using namespace std;
/*
template <class PoolType, class T>
class SkyNet_Reference_Counter
{
public:
	SkyNet_Reference_Counter():m_references(0)
	{
		
	};
	virtual ~SkyNet_Reference_Counter() {};
	int RemoveReference()const
	{
		int result = --m_references;
		sLog->outDebug("SkyNet_Reference_Counter::RemoveReference: %d references left", m_references);
		if(result == 0){
			sLog->outDebug("SkyNet_Reference_Counter::RemoveReference: deleting object");
			ACE_Singleton<PoolType, ACE_Null_Mutex>::instance()->Recycle((T*)this);
			
			//delete (SkyNet_Reference_Counter*)this;
			return 0;
		
		}
		return result;
	}
	int AddReference() const
	{ 
		m_references++; 
		return m_references; 
	}

	mutable int m_references;
};
*/
class Referencable;
typedef std::set<Referencable*> ReferenceList;
class Referencable {
public:
	Referencable();
	virtual ~Referencable();
	void RegisterReferenceTo(Referencable* target);
	void RegisterReferenceBy(Referencable* target);
	void RemoveReferenceTo(Referencable* target);
	void RemoveReferenceBy(Referencable* target);

	void LooseReferenceTo(Referencable* target);
	bool IsReferencingTo(Referencable* target);
	bool IsReferencedBy(Referencable* target);

	virtual void OnLostReference(Referencable* target){};
private:
	ReferenceList _refBy;
	ACE_RW_Mutex _refByMutex;
	ReferenceList _refTo;
	ACE_RW_Mutex _refToMutex;
};

#endif /* REFERENCABLE_H_ */
