/*
 * LSDStorage.h
 *
 *  Created on: Sep 3, 2011
 *      Author: dimitar
 */

#ifndef LSDSTORAGE_H_
#define LSDSTORAGE_H_
#include "defines.h"
#include "Database/DatabaseEnv.h"


template<class T>
class LSDStorage {
public:

        typedef UNORDERED_MAP<uint32, T*> P;

        T* LookupEntry(uint32 key){
                //sLog->outDebug("Accessing key %u, total elements: %u", key, eCount);
                return (key >= eCount) ? NULL : row[key];
        }
        LSDStorage(){
        }
        void UnLoad(){
            for(int i = 0; i <= eCount; i++){
                    T* lsd_row = row[i];
                    if(lsd_row)
                            delete lsd_row;
            }
        }
        void Load(std::string table){
            QueryResult result = StorageDatabase.PQuery("SELECT MAX(entry) as elements FROM `%s`",table.c_str());
            if(!result){
                sLog->outNormalInLine("Loading store \"%s\" - ",table.c_str());
                sLog->SetColor(RED);
                sLog->outNormal("Failed");
                return;
            }
            Field* fields = result->Fetch();
            eCount = fields[0].GetUInt32();

            if(eCount == 0) // Incase of empty LSD table
                    return;

            eCount++;
            result = StorageDatabase.PQuery("SELECT * FROM `%s`",table.c_str());
            sLog->outDebug("\nLoading %s entries.", table.c_str());
            int total = result->GetRowCount();
            row = new T * [eCount];
            int i = 1;
            do{

                Field* fields = result->Fetch();
                uint32 entry    = fields[0].GetUInt32();
                T* lsd_row              = new T(fields);
                row[entry]              = lsd_row;
                sLog->outProgress(i,total);
                i++;

            }while(result->NextRow());
            sLog->outDebug(">> Loaded %u %s entries.",total,table.c_str());
            sLog->outNormalInLine("Loading store \"%s\" - ",table.c_str());
            sLog->SetColor(LGREEN);
            sLog->outNormal("OK\n");
			sLog->ResetColor();
        }
         ~LSDStorage(){};
        uint32 eCount;
        T** row;
};


#endif /* LSDSTORAGE_H_ */
