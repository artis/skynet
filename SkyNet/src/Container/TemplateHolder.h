#ifndef TEMPLATEHOLDER_H
#define TEMPLATEHOLDER_H

#include <vector>
#include "common.h"
template <class T>
class TemplateHolder
{
    public:
        void RegisterTemplate(uint32 template_id, T* instance){
            if(m_templates.size() <= template_id)
                m_templates.resize(template_id+1, NULL);

            m_templates[template_id] = instance;

        }
        T* GenerateObject(uint32 template_id){
            if( m_templates.size() <= template_id)
                return NULL;
            return m_templates[template_id]->Clone();
        }
    private:
        vector<T*> m_templates;
};

#endif // TEMPLATEHOLDER_H
