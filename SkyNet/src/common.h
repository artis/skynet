#ifndef __COMMON_H_
#define __COMMON_H_
#include "Logging/Log.h"
#include <assert.h>
#include <ace/Basic_Types.h>
#include <ace/Guard_T.h>
#include <ace/RW_Thread_Mutex.h>
#include <ace/Thread_Mutex.h>
#include <ace/Singleton.h>
#include <ace/Null_Mutex.h>
#include "Container/unordered_map.h"
#include "Threading/LockedQueue.h"
#include "config.h"
#include <cmath>


#endif