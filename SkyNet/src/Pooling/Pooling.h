#ifndef _POOLING_H
#define _POOLING_H
#include <vector>

class Poolable{
public:
	virtual void Construct()=0;
	virtual void Recycle()=0;
};
template <class T>
class NullObjectPool{
public:
	T* Build() { return new T(); }
	void Recycle(T* obj) { delete obj; }
};
template <class T>
class ObjectPool{
public:
	ObjectPool()
	{
		m_objects.reserve(4000);
	}
	T* Build()
	{
		if(m_objects.empty())
			return new T();
		else
		{
			m_lock.acquire();
			T* obj = static_cast<T*>(m_objects.back());
			m_objects.pop_back();
			m_lock.release();
			return obj;
		}
	};

	void Recycle(Poolable* obj)
	{
		sLog->outDebug("Recycling");
        m_lock.acquire();
		m_objects.push_back(obj);
        m_lock.release();
	}

private:
	std::vector<Poolable*> m_objects;
	ACE_Thread_Mutex m_lock;
};
#endif