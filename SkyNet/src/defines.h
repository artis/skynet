/*
 * defines.h
 *
 *  Created on: Oct 17, 2010
 *      Author: trojaneca
 */
#ifndef DEFINE_H
#define DEFINE_H


#include <sys/types.h>

#include <ace/Basic_Types.h>

#include <string>

#define ASSERT ACE_ASSERT

#define UI64FMTD ACE_UINT64_FORMAT_SPECIFIER

#ifndef PI
#define PI 3.14159265358979323846f
#endif

typedef ACE_INT64 int64;
typedef ACE_INT32 int32;
typedef ACE_INT16 int16;
typedef ACE_INT8 int8;
typedef ACE_UINT64 uint64;
typedef ACE_UINT32 uint32;
typedef ACE_UINT16 uint16;
typedef ACE_UINT8 uint8;
typedef ACE_UINT32 uint;

#ifndef ACE_SSL_SOCK_ACCEPTOR
#define ACE_SSL_SOCK_ACCEPTOR ACE_SSL_SOCK_Acceptor
#endif

#ifndef ACE_SSL_SOCK_STREAM
#define ACE_SSL_SOCK_STREAM ACE_SSL_SOCK_Stream
#endif

#define SERVER_BUFFER_SIZE 4096
#define MIN_PACKET_SIZE 4
#define MAX_PACKET_SIZE 2048


#define MAX_QUERY_LEN 32*1024
#define WPFatal( assertion, errmsg ) if( ! (assertion) ) { sLog->outError( "%s:%i FATAL ERROR:\n  %s\n", __FILE__, __LINE__, (char *)errmsg ); assert( #assertion &&0 ); abort(); }


enum TimeConstants
{
    MINUTE = 60,
    HOUR   = MINUTE*60,
    DAY    = HOUR*24,
    WEEK   = DAY*7,
    MONTH  = DAY*30,
    YEAR   = MONTH*12,
    IN_MILLISECONDS = 1000
};

inline void UnusedVariable(void*){
        return;
}


#define ECLIPSE_BUILD
using namespace std;
#endif //DEFINE_H