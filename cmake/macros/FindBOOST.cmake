#
# Find the ACE client includes and library
#

# This module defines
# ACE_INCLUDE_DIR, where to find ace.h
# ACE_LIBRARIES, the libraries to link against
# ACE_FOUND, if false, you cannot build anything that requires ACE

# also defined, but not for general use are
# ACE_LIBRARY, where to find the ACE library.

set( BOOST_FOUND 0 )

if ( UNIX )
FIND_PATH( BOOST_INCLUDE_DIR
NAMES
boost/signal.hpp
PATHS
/usr/include
/usr/local/include
$ENV{ACE_ROOT}
$ENV{ACE_ROOT}/include
${CMAKE_SOURCE_DIR}/externals/ace
~/.sys/include
DOC
"Specify include-directories that might contain signal.hpp here."
)
FIND_LIBRARY( BOOST_LIBRARY
NAMES
boost_signals
PATHS
/usr/lib
/usr/local/lib
/usr/local/ace/lib
$ENV{ACE_ROOT}/lib
$ENV{ACE_ROOT}
~/.sys/lib
DOC "Specify library-locations that might contain the ACE library here."
)

# FIND_LIBRARY( ACE_EXTRA_LIBRARIES
# NAMES
# z zlib
# PATHS
# /usr/lib
# /usr/local/lib
# DOC
# "if more libraries are necessary to link into ACE, specify them here."
# )

if ( BOOST_LIBRARY )
if ( BOOST_INCLUDE_DIR )
set( BOOST_FOUND 1 )
message( STATUS "Found BOOST library: ${BOOST_LIBRARY}")
message( STATUS "Found BOOST headers: ${BOOST_INCLUDE_DIR}")
else ( BOOST_INCLUDE_DIR )
message(FATAL_ERROR "Could not find BOOST headers! Please install BOOST libraries and headers")
endif ( BOOST_INCLUDE_DIR )
endif ( BOOST_LIBRARY )

mark_as_advanced( BOOST_FOUND BOOST_LIBRARY BOOST_EXTRA_LIBRARIES BOOST_INCLUDE_DIR )
endif (UNIX)

find_path(BOOST_INCLUDE_DIR
  NAMES
    boost/signal.hpp
  PATHS
    ${BOOST_ADD_INCLUDE_PATH}
	"C:/boost"
    "C:/Program Files (x86)/SkyNet Essentials/boost/include"
  DOC
    "Specify the directory containing mysql.h."
)


if( WIN32 )
  find_library( BOOST_LIBRARY 
    NAMES
      libboost_signals-vc100-mt-1_51.lib
    PATHS
      "C:/Program Files (x86)/SkyNet Essentials/boost/lib"
    DOC "Specify the location of the BOOST library here."
  )
endif( WIN32 )
