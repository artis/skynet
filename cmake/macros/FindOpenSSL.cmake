#
# Find the OPENSSL client includes and library
#

# This module defines
# OPENSSL_INCLUDE_DIR, where to find ace.h
# OPENSSL_LIBRARIES, the libraries to link against
# OPENSSL_FOUND, if false, you cannot build anything that requires OPENSSL

# also defined, but not for general use are
# OPENSSL_LIBRARY, where to find the OPENSSL library.

set( OPENSSL_FOUND 0 )

if ( UNIX )
FIND_PATH( OPENSSL_INCLUDE_DIR
NAMES
openssl/ssl.h
PATHS
"C:/openssl/include"
DOC
"Specify include-directories that might contain ace.h here."
)
FIND_LIBRARY( OPENSSL_LIBRARY
NAMES
ssleay32MT.lib
PATHS
"C:/Program Files (x86)/SkyNet Essentials/OpenSSL-Win32/out32/VC"
DOC "Specify library-locations that might contain the OPENSSL library here."
)

# FIND_LIBRARY( OPENSSL_EXTRA_LIBRARIES
# NAMES
# z zlib
# PATHS
# /usr/lib
# /usr/local/lib
# DOC
# "if more libraries are necessary to link into OPENSSL, specify them here."
# )

if ( OPENSSL_LIBRARY )
if ( OPENSSL_INCLUDE_DIR )
set( OPENSSL_FOUND 1 )
message( STATUS "Found OPENSSL library: ${OPENSSL_LIBRARY}")
message( STATUS "Found OPENSSL headers: ${OPENSSL_INCLUDE_DIR}")
else ( OPENSSL_INCLUDE_DIR )
message(FATAL_ERROR "Could not find OPENSSL headers! Please install OPENSSL libraries and headers")
endif ( OPENSSL_INCLUDE_DIR )
endif ( OPENSSL_LIBRARY )

mark_as_advanced( OPENSSL_FOUND OPENSSL_LIBRARY OPENSSL_EXTRA_LIBRARIES OPENSSL_INCLUDE_DIR )
endif (UNIX)

find_path(OPENSSL_INCLUDE_DIR
  NAMES
    openssl/ssl.h
  PATHS
	"C:/Program Files (x86)/SkyNet Essentials/OpenSSL-Win32/inc32/"
  DOC
    "Specify the directory containing mysql.h."
)


if( WIN32 )
  find_library( OPENSSL_LIBRARY 
    NAMES
      ssleay32MT.lib
    PATHS
      "C:/Program Files (x86)/SkyNet Essentials/OpenSSL-Win32/out32/VC"
    DOC "Specify the location of the mysql library here."
  )
endif( WIN32 )

if ( OPENSSL_LIBRARY )
if ( OPENSSL_INCLUDE_DIR )
set( OPENSSL_FOUND 1 )
message( STATUS "Found OPENSSL library: ${OPENSSL_LIBRARY}")
message( STATUS "Found OPENSSL headers: ${OPENSSL_INCLUDE_DIR}")
else ( OPENSSL_INCLUDE_DIR )
message(FATAL_ERROR "Could not find OPENSSL headers! Please install OPENSSL libraries and headers")
endif ( OPENSSL_INCLUDE_DIR )
endif ( OPENSSL_LIBRARY )

mark_as_advanced( OPENSSL_FOUND OPENSSL_LIBRARY OPENSSL_EXTRA_LIBRARIES OPENSSL_INCLUDE_DIR )