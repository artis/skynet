#
# Find the ACE client includes and library
#

# This module defines
# ACE_INCLUDE_DIR, where to find ace.h
# ACE_LIBRARIES, the libraries to link against
# ACE_FOUND, if false, you cannot build anything that requires ACE

# also defined, but not for general use are
# ACE_LIBRARY, where to find the ACE library.

set( BOOST_EXCEPTIONS_FOUND 0 )

if ( UNIX )
FIND_PATH( BOOST_EXCEPTIONS_INCLUDE_DIR
NAMES
boost/signal.hpp
PATHS
/usr/include
/usr/local/include
$ENV{ACE_ROOT}
$ENV{ACE_ROOT}/include
${CMAKE_SOURCE_DIR}/externals/ace
~/.sys/include
DOC
"Specify include-directories that might contain signal.hpp here."
)

FIND_LIBRARY( BOOST_EXCEPTIONS_LIBRARY
NAMES
BOOST_EXCEPTIONS_signals
PATHS
/usr/lib
/usr/local/lib
/usr/local/ace/lib
$ENV{ACE_ROOT}/lib
$ENV{ACE_ROOT}
~/.sys/lib
DOC "Specify library-locations that might contain the ACE library here."
)

# FIND_LIBRARY( ACE_EXTRA_LIBRARIES
# NAMES
# z zlib
# PATHS
# /usr/lib
# /usr/local/lib
# DOC
# "if more libraries are necessary to link into ACE, specify them here."
# )

if ( BOOST_EXCEPTIONS_LIBRARY )

set( BOOST_EXCEPTIONS_FOUND 1 )
message( STATUS "Found BOOST Exceptions library: ${BOOST_EXCEPTIONS_LIBRARY}")

endif ( BOOST_EXCEPTIONS_LIBRARY )

mark_as_advanced( BOOST_EXCEPTIONS_FOUND BOOST_EXCEPTIONS_LIBRARY BOOST_EXCEPTIONS_EXTRA_LIBRARIES BOOST_EXCEPTIONS_INCLUDE_DIR )
endif (UNIX)


if( WIN32 )
  find_library( BOOST_EXCEPTIONS_LIBRARY 
    NAMES
      libboost_exception-vc100-mt-1_51.lib
    PATHS
      "C:/Program Files (x86)/SkyNet Essentials/boost/lib"
    DOC "Specify the location of the BOOST library here."
  )
endif( WIN32 )
